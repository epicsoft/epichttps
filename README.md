<!-- vscode-markdown-toc -->
* 1. [Versions](#Versions)
	* 1.1. [Details](#Details)
* 2. [Requirements](#Requirements)
* 3. [Examples](#Examples)
	* 3.1. [Simple run](#Simplerun)
* 4. [Environments](#Environments)
* 5. [Limitations](#Limitations)
* 6. [Links](#Links)
* 7. [License](#License)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


# epicHTTPS

epicHTTPS is a graphical interface for the administration and creation of Let's Encrypt certificates as well as web server configuration files.

epicHTTPS supports rotating keys, HTTP Strict Transport Security (HSTS), HTTP Public Key Pinning (HPKP), and other features.


##  1. <a name='Versions'></a>Versions

`latest` [Dockerfile](https://gitlab.com/epicsoft/epichttps/blob/master/docker/Dockerfile)

`2.3.1` [Dockerfile](https://gitlab.com/epicsoft/epichttps/blob/release-2.3.1/docker/Dockerfile)

`2.3.0` [Dockerfile](https://gitlab.com/epicsoft/epichttps/blob/release-2.3.0/docker/Dockerfile)

`2.2.1` [Dockerfile](https://gitlab.com/epicsoft/epichttps/blob/release-2.2.1/docker/Dockerfile)

`2.2.0` [Dockerfile](https://gitlab.com/epicsoft/epichttps/blob/release-2.2.0/docker/Dockerfile)


###  1.1. <a name='Details'></a>Details

`2.3.1`

- End of support
- Development of version 3 begins

`2.3.0`

- Docker image based on the official repository [openjdk](https://hub.docker.com/_/openjdk/) in version `8u171-jre-alpine3.8`.
- It will install the latest versions of `certbot`,` openssl` and `curl`.

`2.2.1` `latest`

- Fix for [CVE-2018-6003](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-6003)

`2.2.0`

- Docker image based on the official repository [openjdk](https://hub.docker.com/_/openjdk/) in version `8u151-jre-alpine3.7` with Alpine Linux in version `3.7`.
- It will install the latest versions of `certbot`,` openssl` and `curl` for Alpine Linux version` 3.7`.


##  2. <a name='Requirements'></a>Requirements

- Docker installed - https://www.docker.com/get-docker
- Optional: Docker Swarm initialized - https://docs.docker.com/engine/swarm/ - Some examples use Docker Swarm, but are not required.


##  3. <a name='Examples'></a>Examples


###  3.1. <a name='Simplerun'></a>Simple run 

    docker run --rm -it -p "8080:8080" --name epichttps registry.gitlab.com/epicsoft/epichttps:dev

With this command we start epicHTTPS in standalone mode for testing. Once the application has booted, it can be accessed via browser via `http://localhost:8080/`. The username is `admin` and the password is displayed in the log output.


##  4. <a name='Environments'></a>Environments

`SPRING_PROFILES_ACTIVE`

*default:* prod,nginx

Sets the current Spring runtime environment (profile)

`EPICHTTPS_USERNAME`

*default:* admin

Username for logging in epicHTTPS.

`EPICHTTPS_PASSWORD`

*default:* _empty_

Password for logging in epicHTTPS. If the password is empty, a new one is generated at startup and displayed in the log.

`EPICHTTPS_ENCRYPTED`

*default:* false

Indicates whether the password is encrypted in the `EPICHTTPS_PASSWORD` field. You can use a BCrypt hash with 16 rounds.

`EPICHTTPS_HOSTPATH`

*default:* /etc/letsencrypt

The absolute path to the certificate directories. Used to create absolute paths for systems (e.g., hosts) outside the Docker container.

`EPICHTTPS_DOMAIN`

*default:* epichttps.example.com

The domain name under the epicHTTPS is reachable

`EPICHTTPS_PROXY_PASS`

*default:* http://127.0.0.1:8080/

This value is used for the initial webserver configuration file to set the proxy address. This is necessary primarily for the initial configuration, since the encryption uses self-signed certificates.

`JAVA_OPTS`

*default:* -server -XX:+UseG1GC

Additional Java VM parameters


##  5. <a name='Limitations'></a>Limitations

- epicHTTPS can not directly reload (or restart) the web server, so the file `.../custom/reload` with a current timestamp will be described when a reload is required. (One solution is the [nginx](https://gitlab.com/epicdocker/nginx) Docker Image by epicsoft with integrated inotify)
- epicHTTPS currently offers no possibility of notification. The sending of emails is planned.
- Created domains can not be deleted.
- Download (including deletion) and upload of backup keys is not yet possible, only affects HTTP Public Key Pinning (HPKP). 
- Certificates can not be revoked.
- There is no automatic cleanup of old keys and certificates.


##  6. <a name='Links'></a>Links 

- https://gitlab.com/epicsoft/epichttps/tree/master
- https://hub.docker.com/r/epicsoft/epichttps/


##  7. <a name='License'></a>License 

MIT License see [LICENSE](https://gitlab.com/epicsoft/epichttps/blob/master/LICENSE)

Please note the licenses of third parties and libraries, these may differ.