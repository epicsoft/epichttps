#
# Copyright 2017 epicsoft.de / Alexander Schwarz
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#
# epicHTTPS - Redirect one Domain from HTTP to HTTPS  
#
# !!! DON'T MODIFY THIS FILE - File is generated and will be overwritten !!!
#
# Example: 
# include /etc/letsencrypt/custom/subdomain.example.com/nginx/redirect.inc
#

server {
  {{#ipv4}}
  listen {{{this}}};
  {{/ipv4}}
  {{#ipv6}}
  listen {{{this}}};
  {{/ipv6}}

  server_tokens  off;
  server_name    {{#domains}}{{this}} {{/domains}};
  include        {{hostauthincludefile}};
  
  location / {
    return 302 https://$host:{{httpsport}}$request_uri;
  }
}