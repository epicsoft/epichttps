#
# Copyright 2017 epicsoft.de / Alexander Schwarz
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#
# epicHTTPS - TLS include file. 
# 
# !!! DON'T MODIFY THIS FILE - File is generated and will be overwritten !!!
# 
# Usage:
# Create your server configuration for https as usual and include it in this configuration.
# 
# Example: 
# server {
#   listen 443 ssl http2;
#   listen [::]:443 ssl http2;
#
#   server_name example.com;
#   root /srv/www/example.com;
#   index index.html;
#
#   include /etc/letsencrypt/custom/example.com/nginx/tls.inc;
# }

# security server informations
{{#server_tokens_enable}}
server_tokens {{{server_tokens}}}; 
{{/server_tokens_enable}}

# security header
{{#header_content_type_option_enable}}
add_header X-Content-Type-Options "{{{header_content_type_option}}}";
{{/header_content_type_option_enable}}
{{#header_xss_protection_enable}}
add_header X-XSS-Protection "{{{header_xss_protection}}}";
{{/header_xss_protection_enable}}
{{#header_frame_option_enable}}
add_header X-Frame-Options {{{header_frame_option}}};
{{/header_frame_option_enable}}

# HSTS and HPKP
{{#header_hsts_enable}}
add_header Strict-Transport-Security '{{{hsts_content}}}';
{{/header_hsts_enable}}
{{#header_hpkp_enable}}
add_header Public-Key-Pins '{{{hpkp_content}}}';
{{/header_hpkp_enable}}
{{#header_hpkp_report_enable}}
add_header Public-Key-Pins-Report-Only '{{{hpkp_report_content}}}';
{{/header_hpkp_report_enable}}

# certs configuration
ssl_certificate {{{cert_dir}}}/{{{cert_file}}};
ssl_certificate_key {{{key_dir}}}/{{{cert_key}}};
{{#trusted_certificate_enable}}
ssl_trusted_certificate {{{cert_dir}}}/{{{cert_file}}};
{{/trusted_certificate_enable}}
{{#dhparam_enable}}
ssl_dhparam {{{key_dir}}}/{{{dhparam}}};
{{/dhparam_enable}}
{{#session_timeout_enable}}
ssl_session_timeout {{{session_timeout}}};
{{/session_timeout_enable}}
{{#session_cache_enable}}
ssl_session_cache {{{session_cache}}};
{{/session_cache_enable}}
{{#session_tickets_enable}}
ssl_session_tickets {{{session_tickets}}};
{{/session_tickets_enable}}
    
# protocols and ciphers
ssl_protocols {{{protocols}}};
ssl_ciphers '{{{ciphers}}}';
{{#prefer_server_ciphers_enable}}
ssl_prefer_server_ciphers {{{prefer_server_ciphers}}};
{{/prefer_server_ciphers_enable}}

# OCSP stapling
{{#stapling_enable}}
ssl_stapling {{{stapling}}};
{{/stapling_enable}}
{{#stapling_verify_enable}}
ssl_stapling_verify {{{stapling_verify}}};
{{/stapling_verify_enable}}

# Resolver
{{#resolver_timeout_enable}}
resolver_timeout {{{resolver_timeout}}};
{{/resolver_timeout_enable}}
{{#resolver_enable}}
resolver {{{resolver_ip}}};
{{/resolver_enable}}
