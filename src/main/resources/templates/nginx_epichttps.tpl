#
# Copyright 2016 epicsoft.de / Alexander Schwarz
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#
# Epic LE Builder - Template for an encrypted subdomain.
# 
# This file may be modified, it will not be overwritten or modified. The file will be re-created the next time it is deleted.
# The configuration can be used to establish a secure connection to the server as long as no valid certificates are available. 
# This configuration uses self-signed certificates to enable a TSL connection.
# This domain is used for initial setup, uses self-signed certificates and is not designed for permanent operation.
#
# Usage:
# Include this file in any nginx configuration.
# 
# Example: 
# include {{host_global_nginx}}/{{filename}};
#

server {
  listen {{ipv4}}80;	
  listen [{{ipv6}}]:80;

  server_tokens off;
  server_name {{domain}};
  
  # Authentication
  location ~ /.well-known/acme-challenge/(.*) {  	
    root {{host_webroot}};
    expires off;
    add_header Cache-Control no-cache;
    add_header Content-Type text/plain;
  }  

  # redirect to https
  location / {
    return 302 https://$host$request_uri;
  }
}

server {
  listen {{ipv4}}443 ssl http2;
  listen [{{ipv6}}]:443 ssl http2;

  server_tokens off;
  server_name {{domain}};

  # redirect to HTTP for authentication
  location ~ /.well-known/acme-challenge/(.*) {
    return 302 http://{{domain}}:80$request_uri;
  }
  
  # proxy to application 
  location / {
    proxy_pass {{{proxy_pass}}};
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Host $server_name;
    proxy_set_header X-Forwarded-Proto $scheme;
        
    # WebSocket support
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $http_connection;
  }

  # certs configuration
  ssl_certificate {{host_global_cert}}/self_signed.crt;
  ssl_certificate_key {{host_global_cert}}/self_signed.key;
  ssl_session_tickets off;

  # modern configuration. tweak to your needs.
  ssl_protocols TLSv1.2;
  ssl_ciphers 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256';
  ssl_prefer_server_ciphers on;

  # OCSP stapling
  ssl_stapling off;
  ssl_stapling_verify off;
}