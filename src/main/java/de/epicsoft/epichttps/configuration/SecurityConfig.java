/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.configuration;

import java.util.UUID;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.util.StringUtils;

import de.epicsoft.epichttps.common.EpicWatch;
import de.epicsoft.epichttps.ui.common.Resource;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  public static final String ROLE_PREFIX = "ROLE_";

  public static final String ROLE_ADMIN = "ADMIN";

  public static final String ANONYMOUS_USER = "anonymousUser";

  private static final String CONF_KEY_USERNAME = "epichttps.username";

  private static final String CONF_KEY_PASSWORD = "epichttps.password";

  private static final String CONF_KEY_BCRYPT_STRENGTH = "epicsoft.security.bcrypt.strength";

  private static final String BCRYPT_REGEX = "^\\$2[ayb]\\$.{56}$";

  @Autowired
  private Environment env;

  @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    final EpicWatch watch = new EpicWatch();
    final String login = Resource.LOGIN;
    // @formatter:off
    http
      .csrf().disable()
      .rememberMe().disable()
      .authorizeRequests()
      	// erlaube Vaadin und Login
        .antMatchers("/VAADIN/**", "/PUSH/**", "/UIDL/**","/vaadinServlet/UIDL/**",login, login + "/**").permitAll()
        .antMatchers("/**").fullyAuthenticated()
    .and()
      .logout().logoutUrl(Resource.LOGOUT).logoutSuccessUrl(login).permitAll()
    .and()
      .sessionManagement().sessionFixation().newSession()
    .and()
      .exceptionHandling()
        .accessDeniedPage(login)
        .authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint(login));
    // @formatter:on
    log.info("SecurityConfig::configure in {}", watch);
  }

  @Bean("passwordEncoder")
  public PasswordEncoder passwordEncoder() {
    final EpicWatch watch = new EpicWatch();
    final Integer strength = this.env.getProperty(CONF_KEY_BCRYPT_STRENGTH, Integer.class);
    final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(strength);
    log.info("Created::{} in {}", encoder, watch);
    return encoder;
  }

  /**
   * Uebernimmt Benutzername und Passwort aus der Konfiguration. Prueft dabei moegliche Probleme bei der Konfiguration und erstellt ggf. Meldungen. <br>
   * Passwoerter duerfen nicht an den Logger uebergeben und gespeichert werden, deswegen erfolgt die Ausgabe von Warnungen und Hinweisen direkt an sysout.
   *
   * @param auth
   * @param encoder
   * @throws Exception
   */
  @Autowired
  @DependsOn("passwordEncoder")
  public void configureGlobal(final AuthenticationManagerBuilder auth, final PasswordEncoder encoder) throws Exception {
    final EpicWatch watch = new EpicWatch();
    final String username = this.env.getProperty(CONF_KEY_USERNAME);
    final String envPass = this.env.getProperty(CONF_KEY_PASSWORD);
    String encryptedPass;

    if (StringUtils.hasText(envPass)) {
      if (this.isPasswordBcrypted(envPass)) {
        // verwende uebergebened bcrypt passwort
        encryptedPass = envPass;
      } else {
        // Passwort ist im Klartext uebergeben
        encryptedPass = encoder.encode(envPass);
        log.warn("Environment property contains clear password.");
        System.out.println("###############################################################################");
        System.out.println("#");
        System.out.println(String.format("# Change 'EPICHTTPS_PASSWORD' to '%s'", encryptedPass));
        System.out.println("#");
        System.out.println("###############################################################################");
      }
    } else {
      // Kein Passwort uebergeben, ein temporaeres Passwort wird erzeugt
      final String tmpPass = UUID.randomUUID().toString();
      encryptedPass = encoder.encode(tmpPass);
      log.warn("Admin passwort is empty. Set property 'EPICHTTPS_PASSWORD' with your password.");
      System.out.println("###############################################################################");
      System.out.println("#");
      System.out.println(String.format("# Temporary admin password '%s'", tmpPass));
      System.out.println("#");
      System.out.println("###############################################################################");
    }

    auth.inMemoryAuthentication().withUser(username).password(encryptedPass).roles(ROLE_ADMIN).and().passwordEncoder(encoder);
    log.info("SecurityConfig::configureGlobal in {}", watch);
  }

  private boolean isPasswordBcrypted(final String envPass) {
    return Pattern.compile(BCRYPT_REGEX).matcher(envPass).find();
  }
}