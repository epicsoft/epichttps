/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.configuration;

import java.nio.file.FileSystem;
import java.nio.file.FileSystems;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.github.marschall.memoryfilesystem.MemoryFileSystemFactoryBean;

import de.epicsoft.epichttps.Profiles;
import de.epicsoft.epichttps.common.EpicWatch;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Configuration
public class FileSystemConfiguration {

	@Profile({ Profiles.PROD, Profiles.DEV })
	@Bean(destroyMethod = "close")
	public FileSystem defaultFileSystem() {
		final EpicWatch watch = new EpicWatch();
		final FileSystem fs = FileSystems.getDefault();
		log.info("Created::{} in {}", fs, watch);
		return fs;
	}

	@Profile({ Profiles.TEST })
	@Bean(destroyMethod = "close")
	public FileSystem inMemoryFileSystem() {
		final EpicWatch watch = new EpicWatch();

		final MemoryFileSystemFactoryBean factory = new MemoryFileSystemFactoryBean();
		factory.setType(MemoryFileSystemFactoryBean.LINUX);

		final FileSystem fs = factory.getObject();
		log.info("Created::{} in {}", fs, watch);
		return fs;
	}
}