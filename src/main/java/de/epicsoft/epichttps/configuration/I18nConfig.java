/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.configuration;

import java.nio.charset.StandardCharsets;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.common.EpicWatch;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Configuration
public class I18nConfig {

	@Value("${epicsoft.datetime.format}")
	private String dateTimeFormat;

	@Value("${epicsoft.datetime.format.detailed}")
	private String dateTimeFormatDetailed;

	@Bean
	public MessageSource messageSource() {
		final EpicWatch watch = new EpicWatch();
		final EpicMessageSource messageSource = new EpicMessageSource();
		messageSource.setBasename("i18n/messages");
		messageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());
		messageSource.setUseCodeAsDefaultMessage(false);
		messageSource.setFallbackToSystemLocale(false);
		messageSource.setLocale(Locale.GERMANY);
		log.info("Created::{} in {}", messageSource, watch);
		return messageSource;
	}

	@Bean(name = { "dateTimeFormatter", "defaultDateTimeFormatter" })
	public DateTimeFormatter defaultDateTimeFormatter() {
		final EpicWatch watch = new EpicWatch();
		final DateTimeFormatter defaultDateTimeFormatter = DateTimeFormatter.ofPattern(this.dateTimeFormat);
		log.info("Created::{} with format '{}' in {}", defaultDateTimeFormatter, this.dateTimeFormat, watch);
		return defaultDateTimeFormatter;
	}

	@Bean(name = "detailedDateTimeFormatter")
	public DateTimeFormatter detailedDateTimeFormatter() {
		final EpicWatch watch = new EpicWatch();
		final DateTimeFormatter detailedDateTimeFormatter = DateTimeFormatter.ofPattern(this.dateTimeFormatDetailed);
		log.info("Created::{} with format '{}' in {}", detailedDateTimeFormatter, this.dateTimeFormatDetailed, watch);
		return detailedDateTimeFormatter;
	}
}