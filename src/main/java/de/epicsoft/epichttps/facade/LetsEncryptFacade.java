/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission
 * notice shall be included in all copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.epicsoft.epichttps.facade;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import de.epicsoft.epichttps.certificate.LetsEncryptService;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import lombok.AllArgsConstructor;
import lombok.NonNull;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Service
@AllArgsConstructor
public class LetsEncryptFacade {

	@NonNull
	private final LetsEncryptService letsEncryptService;
	@NonNull
	private final SettingsService settingsService;

	public boolean isAccoutExists() {
		return this.letsEncryptService.isAccoutExists();
	}

	public void createAccount(final Settings settings) {
		Assert.isTrue(settings.isGlobal(), "Settings may be global");

		// Erstellt ein neues Let's Encrypt Konto
		final Boolean agreeTos = settings.getBool(SettingsKey.LE_ACCOUNT_AGREE_TOS);
		if (!settings.getBool(SettingsKey.LE_ACCOUNT_WITHOUT_EMAIL)) {
			final String email = settings.get(SettingsKey.LE_ACCOUNT_EMAIL);
			this.letsEncryptService.createAccount(email, agreeTos);
		} else {
			this.letsEncryptService.createAccount(agreeTos);
		}
		// Speichert die Einstellungen ab
		this.settingsService.save(settings);
	}
}
