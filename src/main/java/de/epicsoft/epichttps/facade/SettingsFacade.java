/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.facade;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsService;
import lombok.AllArgsConstructor;
import lombok.NonNull;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Service
@AllArgsConstructor
public class SettingsFacade {

	@NonNull
	private final SettingsService settingsService;

	public Settings getEnv() {
		return this.settingsService.getEnv();
	}

	public Settings getGlobal() {
		return this.settingsService.getGlobal();
	}

	public Settings getDomain(final DomainInfo domainInfo) {
		Assert.notNull(domainInfo, "Domain-Info may bo not null");

		return this.settingsService.getDomain(domainInfo);
	}

	public void save(final Settings settings) {
		if (settings.isGlobal()) {
			this.saveGlobalSettings(settings);
		} else if (settings.isDomain()) {
			this.saveDomainSettings(settings);
		} else {
			throw new IllegalArgumentException("Settings save not allow");
		}
	}

	public void resetAuth(final Settings domainSettings) {
		Assert.notNull(domainSettings, "Domain Settings may be not null");
		Assert.isTrue(domainSettings.isDomain(), "Settings are not domain settings");

		this.settingsService.resetAuth(domainSettings);
	}

	public void resetCert(final Settings domainSettings) {
		Assert.notNull(domainSettings, "Domain Settings may be not null");
		Assert.isTrue(domainSettings.isDomain(), "Settings are not domain settings");

		this.settingsService.resetCert(domainSettings);
	}

	public void resetCsr(final Settings domainSettings) {
		Assert.notNull(domainSettings, "Domain Settings may be not null");
		Assert.isTrue(domainSettings.isDomain(), "Settings are not domain settings");

		this.settingsService.resetCsr(domainSettings);
	}

	public void resetHstsHpkp(final Settings domainSettings) {
		Assert.notNull(domainSettings, "Domain Settings may be not null");
		Assert.isTrue(domainSettings.isDomain(), "Settings are not domain settings");

		this.settingsService.resetHstsHpkp(domainSettings);
	}

	public void resetWebserver(final Settings domainSettings) {
		Assert.notNull(domainSettings, "Domain Settings may be not null");
		Assert.isTrue(domainSettings.isDomain(), "Settings are not domain settings");

		this.settingsService.resetWebserver(domainSettings);
	}

	private void saveGlobalSettings(final Settings settings) {
		this.settingsService.save(settings);
	}

	private void saveDomainSettings(final Settings settings) {
		this.settingsService.save(settings);
	}
}
