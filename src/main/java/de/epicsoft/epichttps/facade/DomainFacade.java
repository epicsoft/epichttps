/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.facade;

import java.util.Set;

import org.apache.commons.validator.routines.DomainValidator;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import de.epicsoft.epichttps.certificate.CertificateService;
import de.epicsoft.epichttps.domain.Domain;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.domain.DomainInfoService;
import de.epicsoft.epichttps.domain.DomainService;
import de.epicsoft.epichttps.key.DhParamService;
import de.epicsoft.epichttps.key.KeyService;
import lombok.AllArgsConstructor;
import lombok.NonNull;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Service
@AllArgsConstructor
public class DomainFacade {

	@NonNull
	private final DomainInfoService domainInfoService;
	@NonNull
	private final DomainService domainService;
	@NonNull
	private final DhParamService dhParamService;
	@NonNull
	private final KeyService keyService;
	@NonNull
	private final CertificateService certificateService;

	public DomainInfo getDomainInfoById(final String id) {
		Assert.hasText(id, "ID may be not empty");

		return this.domainInfoService.getById(id);
	}

	public Set<DomainInfo> getAllDomainInfos() {
		return this.domainInfoService.getAll();
	}

	public Domain getDomainByDomainInfo(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		final Domain domain = this.domainService.get(info);
		domain.setDhParam(this.dhParamService.get(info));
		domain.setPrivateKeys(this.keyService.getValidPrivateKeys(info));
		domain.setBackupKeys(this.keyService.getValidBackupKeys(info));
		domain.setCertificates(this.certificateService.getAllValid(info));
		domain.setCurrentCertificate(this.certificateService.getCurrent(info));
		domain.setCurrentPrivateKey(this.keyService.getCurrentPrivateKey(info));
		return domain;
	}

	public DomainInfo createDomain(final String name) {
		Assert.hasText(name, "Name must be not empty");
		Assert.isTrue(DomainValidator.getInstance().isValid(name), "Name must be valid Domainname");

		final DomainInfo info = this.domainInfoService.create(name);
		this.domainService.create(info);
		return info;
	}
}