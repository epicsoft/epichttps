/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.facade;

import java.nio.file.Path;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import de.epicsoft.epichttps.certificate.CertificateService;
import de.epicsoft.epichttps.certificate.CsrService;
import de.epicsoft.epichttps.common.EpicWatch;
import de.epicsoft.epichttps.common.exception.EpicIllegalArgumentException;
import de.epicsoft.epichttps.domain.Domain;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.domain.DomainService;
import de.epicsoft.epichttps.facade.BuildInfo.BuildInfoBuilder;
import de.epicsoft.epichttps.key.DhParamService;
import de.epicsoft.epichttps.key.KeyService;
import de.epicsoft.epichttps.webserver.WebserverService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class BuildFacade {

	@NonNull
	private final KeyService keyService;
	@NonNull
	private final DhParamService dhParamService;
	@NonNull
	private final CsrService csrService;
	@NonNull
	private final CertificateService certificateService;
	@NonNull
	private final WebserverService webserverService;
	@NonNull
	private final DomainService domainService;

	public BuildInfo isBuildNeeded(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		final BuildInfoBuilder build = BuildInfo.builder();
		build.key(this.keyService.isBuildNeeded(info));
		build.dhParam(this.dhParamService.isBuildNeeded(info));
		build.csr(this.csrService.isBuildNeeded(info));
		build.certificate(this.certificateService.isBuildNeeded(info));
		build.webserver(this.webserverService.isBuildNeeded(info));
		return build.build();
	}

	public BuildInfo build(final DomainInfo info) throws EpicIllegalArgumentException {
		Assert.notNull(info, "Domain Info must be not null");

		log.debug("Starting build for '{}'", info.getName());
		final EpicWatch watch = new EpicWatch();
		final BuildInfo build = this.isBuildNeeded(info);

		if (build.isKeyOrCertificateBuildNeeded()) {
			this.keyService.createOrUpdate(info);
		}
		this.webserverService.createOrUpdateAuth(info);

		if (this.webserverService.isAuthInstalled(info)) {
			final Domain domain = this.domainService.get(info);
			domain.setCurrentPrivateKey(this.keyService.getCurrentPrivateKey(info));
			domain.setPrivateKeys(this.keyService.getValidPrivateKeys(info));
			domain.setAllPrivateKeys(this.keyService.getAllPrivateKeys(info));
			domain.setBackupKeys(this.keyService.getValidBackupKeys(info));
			domain.setDhParam(this.dhParamService.create(info));
			domain.setCsr(this.csrService.createOrUpdate(info));

			if (build.isKeyOrCertificateBuildNeeded()) {
				final Path authDir = this.webserverService.getAuthDir(info);
				this.certificateService.createOrUpdate(domain, authDir);
			}
			domain.setCurrentCertificate(this.certificateService.getCurrent(info));

			this.webserverService.createOrUpdateHttps(domain);
		}

		log.debug("Finish build for '{}' in {}", info.getName(), watch);
		return build;
	}
}
