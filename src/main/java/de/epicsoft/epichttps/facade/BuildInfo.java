/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.facade;

import lombok.Builder;
import lombok.Value;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Builder
@Value
public class BuildInfo {

	@Builder.Default
	private final boolean key = false;

	@Builder.Default
	private final boolean dhParam = false;

	@Builder.Default
	private final boolean csr = false;

	@Builder.Default
	private final boolean certificate = false;

	@Builder.Default
	private final boolean webserver = false;

	public boolean isBuildNeeded() {
		return this.key || this.dhParam || this.csr || this.certificate || this.webserver;
	}

	public boolean isKeyOrCertificateBuildNeeded() {
		return this.key || this.csr || this.certificate;
	}
}
