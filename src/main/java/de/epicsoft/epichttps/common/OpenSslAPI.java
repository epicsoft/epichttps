/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.common;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Path;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import org.bouncycastle.openssl.PEMWriter;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import de.epicsoft.epichttps.key.DhParam;
import de.epicsoft.epichttps.key.Key;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import de.epicsoft.epichttps.terminal.Process;
import de.epicsoft.epichttps.terminal.ProcessManager;
import de.epicsoft.epichttps.terminal.ProcessStatus;
import de.epicsoft.epichttps.terminal.Terminal;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class OpenSslAPI {

	@NonNull
	private final SettingsService settingsService;
	@NonNull
	private final Terminal terminal;
	@NonNull
	private final FileSystemAPI fs;
	@NonNull
	private final ProcessManager processManager;

	/**
	 * Erstellt ein Schluessel-Paar, speichert den Private-Key ab und liefert den PIN zurueck.
	 *
	 * @param key {@link Key}
	 * @param message {@link String}
	 * @return {@link String} Key-PIN
	 */
	public String createKey(final Key key, final String message) {
		Assert.notNull(key, "Key may be not null");
		Assert.hasText(message, "Message may be not empty");

		String pin = null;
		final Process process = new Process("java native", message);
		this.processManager.start(process);
		log.info("{} - create key '{}'", process.getId(), key);

		try {
			final KeyPair keyPair = this.getKeyGenerator().generateKeyPair();
			final String privateKeyPem = this.createPemForPrivateKey(keyPair.getPrivate());
			pin = this.createPinForPublicKey(keyPair.getPublic());

			// speichert Key und PIN
			this.fs.saveFile(key.getKeyFile(), privateKeyPem);
			this.processManager.stop(process, ProcessStatus.SUCCESS, "");
		} catch (final Exception e) {
			log.error("{} - {}", process.getId(), e.getMessage(), e);
			this.processManager.stop(process, ProcessStatus.FAILED, e.getMessage());
			throw new RuntimeException(e);
		}
		return pin;
	}

	public String createPinForPublicKey(final PublicKey publicKey) {
		Assert.notNull(publicKey, "Public key may be not null");

		final byte[] digest = this.getMessageDigestInstance().digest(publicKey.getEncoded());
		return java.util.Base64.getEncoder().encodeToString(digest);
	}

	public DhParam createDhParam(final DhParam dhParam, final String message) {
		Assert.notNull(dhParam, "DH Param may be not null");
		Assert.hasText(message, "Message may be not empty");
		log.info("Create DH param '{}'", dhParam);

		final String cmd = this.terminal.build("openssl dhparam -out", dhParam.getPath().toString(), dhParam.getBit().toString());
		this.terminal.execute(new Process(cmd, message));
		return dhParam;
	}

	public Path createCsr(final Path csr, final Path csrConfig, final Key privateKey, final String message) {
		Assert.notNull(csr, "CSR may be not empty");
		Assert.notNull(csrConfig, "CSR Config may be not empty");
		Assert.notNull(privateKey, "Private Key may be not empty");
		Assert.hasText(message, "Message may be not empty");
		log.info("Create CSR '{}' from config '{}' with key '{}'", csr.toString(), csrConfig.toString(), privateKey.getKeyFile().getFileName().toString());

		final String cmd = this.terminal.build("openssl req -config", csrConfig.toString(), "-new -key", privateKey.getKeyFile().toString(), "-out", csr.toString(), "-outform der");
		this.terminal.execute(new Process(cmd, message));
		return csr;
	}

	public void createSelfSignedCertificate(final Path key, final Path cert, final Path csr, final Path csrConfig) {
		Assert.notNull(cert, "Certificate File may be not null");
		Assert.notNull(key, "Key File may be not null");
		Assert.notNull(csr, "CSR File may be not null");
		Assert.notNull(csrConfig, "CSR Config File may be not null");
		log.info("Create self signed Certificate '{}' with key '{}'", cert.toString(), key.toString());

		final Settings global = this.settingsService.getGlobal();
		final Integer size = global.getInt(SettingsKey.KEYS_SIZE);

		// create private key without passphrase
		this.terminal.execute(new Process(this.terminal.build("openssl genrsa -out", key.toString(), String.valueOf(size)), "Create self signed Certificate - Private Key"));
		// create CSR from CSR-Config
		this.terminal.execute(
				new Process(this.terminal.build("openssl req -config", csrConfig.toString(), "-new -key", key.toString(), "-out", csr.toString()), "Create self signed Certificate - CSR"));
		// create self signed certificate
		this.terminal.execute(new Process(this.terminal.build("openssl req -x509 -sha512 -days 3650 -key", key.toString(), "-in", csr.toString(), "-out", cert.toString()),
				"Create self signed Certificate - Certificate"));
	}

	private MessageDigest getMessageDigestInstance() {
		final Settings global = this.settingsService.getGlobal();
		final String algorithm = global.get(SettingsKey.MESSAGE_DIGEST_ALGORITHM);

		try {
			return MessageDigest.getInstance(algorithm);
		} catch (final NoSuchAlgorithmException e) {
			throw new UnsupportedOperationException(e);
		}
	}

	private KeyPairGenerator getKeyGenerator() {
		final Settings global = this.settingsService.getGlobal();
		final String algorithm = global.get(SettingsKey.KEYS_ALGORITHM);
		final Integer size = global.getInt(SettingsKey.KEYS_SIZE);

		try {
			final KeyPairGenerator keyGen = KeyPairGenerator.getInstance(algorithm);
			keyGen.initialize(size);
			return keyGen;
		} catch (final NoSuchAlgorithmException e) {
			throw new UnsupportedOperationException(e);
		}
	}

	private String createPemForPrivateKey(final PrivateKey privateKey) throws IOException {
		final StringWriter writer = new StringWriter();
		try (PEMWriter pemWriter = new PEMWriter(writer)) {
			pemWriter.writeObject(privateKey);
			pemWriter.flush();
			return writer.toString();
		}
	}

}