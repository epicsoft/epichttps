/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.common;

import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSet.Builder;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public class ImmutableSetCollector<T> implements Collector<T, Builder<T>, ImmutableSet<T>> {

	@Override
	public Supplier<Builder<T>> supplier() {
		return Builder::new;
	}

	@Override
	public BiConsumer<Builder<T>, T> accumulator() {
		return (b, e) -> b.add(e);
	}

	@Override
	public BinaryOperator<Builder<T>> combiner() {
		return (b1, b2) -> b1.addAll(b2.build());
	}

	@Override
	public Function<Builder<T>, ImmutableSet<T>> finisher() {
		return Builder::build;
	}

	@Override
	public Set<Characteristics> characteristics() {
		return ImmutableSet.of();
	}
}
