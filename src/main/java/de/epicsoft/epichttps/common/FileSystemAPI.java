/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class FileSystemAPI {

	private static final String DELIMITER = "\\A";

	@NonNull
	private final FileSystem fs;

	public Path getPath(final String first, final String... more) {
		Assert.notNull(first, "First part of path may be not null");

		return this.fs.getPath(first, more);
	}

	public List<Path> list(final Path directory) {
		Assert.notNull(directory, "Directory may be not null");

		List<Path> paths = new ArrayList<>();
		if (this.isDirectory(directory)) {
			try {
				paths = Files.list(directory).collect(Collectors.toList());
			} catch (final IOException e) {
				log.error(e.getMessage(), e);
			}
		} else {
			log.warn("Directory for list does not exist: '{}'", directory.toString());
		}
		return paths;
	}

	public Boolean isDirectory(final Path directory) {
		Assert.notNull(directory, "Directory may be not null");

		return Files.exists(directory) && Files.isDirectory(directory);
	}

	public void createDirectory(final Path directory) {
		Assert.notNull(directory, "Directory may be not null");

		if (!this.isDirectory(directory)) {
			log.debug("Create Directory '{}'", directory.toString());
			try {
				Files.createDirectories(directory);
			} catch (final IOException e) {
				log.error(String.format("Cannot create directory '%s'", directory.toString()), e);
			}
		}
	}

	/**
	 * Loescht ein Verzeichnis aus dem {@link FileSystem}. Wenn das Verz. nicht vorhnanden ist, gibt es keine Fehlermeldung.
	 *
	 * @param directory {@link Path}
	 */
	public void removeDirectory(final Path directory) {
		Assert.notNull(directory, "Directory may be not null");

		if (this.isDirectory(directory)) {
			log.debug("Remove Directory '{}'", directory.toString());
			try {
				Files.delete(directory);
			} catch (final IOException e) {
				log.error(String.format("Cannot remove directory '%s'", directory.toString()), e);
			}
		}
	}

	public synchronized Properties loadProperties(final Path file) throws IOException {
		Assert.notNull(file, "File may be not null");
		log.trace("Read Properties '{}'", file.toString());

		final Properties props = new Properties();
		try (InputStream in = Files.newInputStream(file)) {
			props.load(in);
		}
		return props;
	}

	public synchronized void saveProperties(final Properties props, final Path file) throws IOException {
		Assert.notNull(props, "Properties may be not null");
		Assert.notNull(file, "File may be not null");
		log.trace("Write Properties '{}'", file.toString());

		final Path parent = file.getParent();
		if (!this.isDirectory(parent)) {
			this.createDirectory(parent);
		}

		try (final OutputStream out = Files.newOutputStream(file)) {
			props.store(out, "");
		}
	}

	/**
	 * Speichert den Inhalt in eine Datei. <br>
	 * - Wenn der Verzeichnis-Pfad nicht vorhanden ist, wird dieser erstellt. <br>
	 * - Wenn die Datei nicht vorhanden ist, wird diese erstellt. <br>
	 * - Wenn die Datei bereits ein Inhalt hat, wird dieser ueberschrieben.
	 *
	 * @param file {@link Path}
	 * @param content {@link String}
	 */
	public void saveFile(final Path file, final String content) {
		Assert.notNull(file, "File may be not null");
		Assert.notNull(content, "Content may be not null");
		log.trace("Write Content to File '{}'", file.toString());

		this.createDirectory(file.getParent());

		try {
			Files.write(file, content.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	public Boolean saveFileIfChanged(final Path file, final String content) {
		Assert.notNull(file, "File may be not null");
		Assert.notNull(content, "Content may be not null");

		if (this.isFileChanged(file, content)) {
			this.saveFile(file, content);
			return true;
		}

		log.trace("File does not need update '{}'", file.toString());
		return false;
	}

	public Boolean isFileChanged(final Path file, final String content) {
		Assert.notNull(file, "File may be not null");
		Assert.notNull(content, "Content may be not null");

		return !this.isFile(file) || !content.equals(this.getContent(file));
	}

	/**
	 * Prueft ob eine Datei auf den Dateisystem bereits existiert.
	 *
	 * @param file {@link Path}
	 * @return {@link Boolean}
	 */
	public Boolean isFile(final Path file) {
		Assert.notNull(file, "File may be not null");

		return Files.exists(file, LinkOption.NOFOLLOW_LINKS);
	}

	/**
	 * Liefert den Inhalt einer Datei als {@link String} zurueck, verwendet dabei UTF-8 als Ecoding. <br>
	 * Datei muss existieren, sonst wird eine {@link IllegalArgumentException} geworfen.
	 *
	 * @param file {@link Path}
	 * @return {@link String}
	 */
	public String getContent(final Path file) {
		Assert.notNull(file, "File may be not null");
		Assert.isTrue(this.isFile(file), "File may be exists");

		try (Scanner scanner = new Scanner(file, StandardCharsets.UTF_8.name())) {
			return scanner.useDelimiter(DELIMITER).next();
		} catch (final IOException | NoSuchElementException e) {
			throw new RuntimeException(e);
		}
	}

	public void deleteFile(final Path file) {
		Assert.notNull(file, "File may be not null");

		if (this.isFile(file)) {
			log.debug("Delete File::{}", file.toString());
			try {
				Files.deleteIfExists(file);
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
}