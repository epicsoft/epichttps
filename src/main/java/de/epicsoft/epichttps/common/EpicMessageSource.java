/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.common;

import java.util.Arrays;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.ResourceBundleMessageSource;

import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@CacheConfig(cacheNames = "messages")
public class EpicMessageSource extends ResourceBundleMessageSource {

	private static final Pattern PLACEHOLDER = Pattern.compile("\\{\\d+\\}");

	private Locale locale = Locale.getDefault();

	@Cacheable(key = "#code")
	public String getMessage(final String code) {
		try {
			return this.getMessage(code, null, this.locale);
		} catch (final NoSuchMessageException e) {
			log.error("message not found for code:{}", code, e);
			return code;
		}
	}

	@Cacheable(key = "{ #code, #args }")
	public String getMessage(final String code, final Integer... args) {
		return this.getMessage(code, Arrays.stream(args).map(String::valueOf).collect(Collectors.toList()).stream().toArray(String[]::new));
	}

	@Cacheable(key = "{ #code, #args }")
	public String getMessage(final String code, final String... args) {
		String msg = this.getMessage(code, null, this.locale);
		if (args != null && args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				msg = msg.replaceAll("\\{" + i + "\\}", args[i]);
			}
		}

		final Matcher m = EpicMessageSource.PLACEHOLDER.matcher(msg);
		if (m.find()) {
			throw new IllegalArgumentException(String.format("Placeholder not resolved '%s'", msg));
		}
		return msg;
	}

	public Locale getLocale() {
		return this.locale;
	}

	@CacheEvict(allEntries = true)
	public void setLocale(final Locale locale) {
		this.locale = locale;
	}
}