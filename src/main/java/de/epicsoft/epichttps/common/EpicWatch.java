/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.common;

import org.springframework.util.StopWatch;

import com.google.common.base.Stopwatch;

/**
 * Vereinfachte {@link StopWatch} <br>
 * <br>
 * <code>
 * Stopwatch stopwatch = new Stopwatch(); // Startet den Timer <br>
 * log.info(stopwatch.toString()); // Stoppt den Timer und erstellt eine Ausgabe
 * </code> <br>
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public class EpicWatch {

	private final Stopwatch stopwatch;

	public EpicWatch() {
		this.stopwatch = Stopwatch.createStarted();
	}

	public String current() {
		return this.stopwatch.toString();
	}

	@Override
	public String toString() {
		this.stopwatch.stop();
		return this.stopwatch.toString();
	}
}
