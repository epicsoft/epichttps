/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.common;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Map;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TemplateService {

	private static final String TPL_SUBDIR = "templates/";
	private static final String TPL_SUFFIX = ".tpl";

	@NonNull
	private final Mustache.Compiler compiler;

	/**
	 * Erstellt ein Template ({@link String}) aus dem uebergebenen Namen (Template-Name) und den Werten ({@link Map}).
	 *
	 * @param name {@link String} Name wird ohne Pfad und ohne Endung ".tpl" angegeben.
	 * @param data {@link Map} with {@link String} as key and {@link Object} as value
	 * @return {@link String}
	 */
	public String render(final String name, final Map<String, Object> data) {
		return this.render(this.getTpl(name), data);
	}

	/**
	 * Erstellt einen {@link Reader} fuer das uebergebene Template. Dabei wird im Unterverzeichnis "templates" nach einer Datei mit dem selben Namen und der Endung ".tpl" gesucht. Im
	 * Fehlerfall wird eine {@link RuntimeException} geworfen. <br>
	 *
	 * @param name {@link String} Name wird ohne Pfad und ohne Endung ".tpl" angegeben.
	 * @return {@link Reader}
	 */
	private Reader getTpl(final String name) {
		try {
			final String path = new StringBuilder(TPL_SUBDIR).append(name).append(TPL_SUFFIX).toString();
			return new InputStreamReader(new ClassPathResource(path).getInputStream());
		} catch (final IOException e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	private String render(final Reader in, final Map<String, Object> data) {
		final Template tpl = this.compiler.compile(in);
		return tpl.execute(data);
	}
}