/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.vaadin.spring.annotation.EnableVaadin;

/**
 * Voraussetzung / Precondition <br>
 * <br>
 * Links: <br>
 * - https://bitbucket.org/epicsoft/epichttps <br>
 * - https://bitbucket.org/epicsoft/epichttps_docker <br>
 * <br>
 * Programmcode inkl. Fehlermeldungen und Logs in englisch / Program code incl. error and logs messages in english <br>
 * Grafische Oberflaeche zuerst in deutsch / Graphical interface first in German <br>
 * Kommentare in deutsch / Comments in german <br>
 * Dokumentation in deutsch / Documentation in german <br>
 * Bug Meldungen in englisch oder deutsch / Bug reports in enlish or german <br>
 * - https://bitbucket.org/epicsoft/epichttps/issues <br>
 * - https://bitbucket.org/epicsoft/epichttps_docker/issues <br>
 * <br>
 * - Use wrappers, make your life simple. Autoboxing allow, see https://docs.oracle.com/javase/tutorial/java/data/autoboxing.html <br>
 * <br>
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@SpringBootApplication
@ComponentScan
@EnableVaadin
@EnableScheduling
@EnableAsync
@EnableCaching(proxyTargetClass = true)
public class EpicHttpsApplication {

  public static final String NAME = "epicHTTPS";

  public static void main(final String[] args) {
    SpringApplication.run(EpicHttpsApplication.class, args);
  }
}
