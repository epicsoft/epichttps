/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.domain;

import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.validator.routines.DomainValidator;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.common.exception.DomainInfoNotFoundException;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Service
@RequiredArgsConstructor
public class DomainInfoService {

	@NonNull
	private final SettingsService settingsService;
	@NonNull
	private final FileSystemAPI fs;

	public DomainInfo getById(final String id) {
		Assert.hasText(id, "ID may be not empty");

		return this.getAll().stream().filter(info -> id.equals(info.getId())).findFirst().orElseThrow(() -> new DomainInfoNotFoundException());
	}

	public Set<DomainInfo> getAll() {
		final Settings globalSettings = this.settingsService.getGlobal();
		final String leRootDir = globalSettings.get(SettingsKey.LE_ROOT_DIR);
		final String customDir = globalSettings.get(SettingsKey.CUSTOM_SUBDIR);

		final Path customFullPath = this.fs.getPath(leRootDir, customDir);
		final List<Path> customContent = this.fs.list(customFullPath);
		/* @formatter:off */
		final Set<DomainInfo> infos =	customContent.parallelStream()
			.filter(content -> this.fs.isDirectory(content))
			.filter(dir -> DomainValidator.getInstance().isValid(dir.getFileName().toString()))
			.map(dir -> new  DomainInfo(this.buildId(dir.getFileName().toString()), dir.getFileName().toString(), dir))
			.collect(Collectors.toSet());
		/* @formatter:on */
		return infos;
	}

	public DomainInfo create(final String name) {
		Assert.hasText(name, "Name may be not empty");
		Assert.isTrue(DomainValidator.getInstance().isValid(name), "Name may be valid Domainname");

		final String id = this.buildId(name);
		final Path domainPath = this.buildDomainPath(name);
		return new DomainInfo(id, name, domainPath);
	}

	private String buildId(final String name) {
		return name.toLowerCase().replaceAll("\\.", "_");
	}

	private Path buildDomainPath(final String name) {
		final Settings globalSettings = this.settingsService.getGlobal();
		final String leRootDir = globalSettings.get(SettingsKey.LE_ROOT_DIR);
		final String customDir = globalSettings.get(SettingsKey.CUSTOM_SUBDIR);
		return this.fs.getPath(leRootDir, customDir, name);
	}
}
