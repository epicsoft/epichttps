/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.domain;

import com.google.common.collect.ImmutableSet;

import de.epicsoft.epichttps.certificate.Certificate;
import de.epicsoft.epichttps.certificate.Csr;
import de.epicsoft.epichttps.key.DhParam;
import de.epicsoft.epichttps.key.Key;
import de.epicsoft.epichttps.settings.Settings;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class Domain {

	@Getter
	private final boolean active;

	@NonNull
	@Getter
	private final DomainInfo info;

	@NonNull
	@Getter
	private final Settings settings;

	@NonNull
	@Getter
	private final ImmutableSet<String> alternativeNames;

	@Getter
	@Setter
	private Key currentPrivateKey;

	@Getter
	@Setter
	private ImmutableSet<Key> privateKeys;

	@Getter
	@Setter
	private ImmutableSet<Key> backupKeys;

	@Getter
	@Setter
	private DhParam dhParam;

	@Getter
	@Setter
	private Csr csr;

	@Getter
	@Setter
	private Certificate currentCertificate;

	@Getter
	@Setter
	private ImmutableSet<Certificate> certificates;

	@Getter
	@Setter
	private ImmutableSet<Key> allPrivateKeys;

}