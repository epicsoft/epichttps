/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.domain;

import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.google.common.collect.ImmutableSet;

import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.common.event.DomainCreatedEvent;
import de.epicsoft.epichttps.common.exception.DomainAlreadyExistsException;
import de.epicsoft.epichttps.common.exception.DomainNotFoundException;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Service
@RequiredArgsConstructor
public class DomainService {

	@NonNull
	private final SettingsService settingsService;
	@NonNull
	private final FileSystemAPI fs;
	@NonNull
	private final ApplicationEventPublisher publisher;

	public Domain get(final DomainInfo info) {
		Assert.notNull(info, "Domaininfo must be not null");
		if (!this.fs.isDirectory(info.getDirectory())) {
			throw new DomainNotFoundException(String.format("Domain not found '%s'", info.getName()));
		}

		final DomainProps props = new DomainProps(info);
		return new Domain(props.isActive(), info, this.settingsService.getDomain(info), ImmutableSet.copyOf(props.getAlternativeNames()));
	}

	public Domain create(final DomainInfo info) {
		Assert.notNull(info, "Domaininfo must ba not null");
		if (this.fs.isDirectory(info.getDirectory())) {
			throw new DomainAlreadyExistsException(String.format("Domain alredy exists '%s'", info.getName()));
		}

		final DomainProps domainProps = new DomainProps(info);
		this.fs.createDirectory(info.getDirectory());
		this.fs.createDirectory(domainProps.getKeySubDirPath());
		this.fs.createDirectory(domainProps.getCertSubDirPath());

		final HashSet<String> altNames = new HashSet<>();
		altNames.add(info.getName());

		final Settings domainSettings = this.settingsService.getDomain(info);
		domainSettings.set(SettingsKey.DOMAIN_ALTERNATIVE_NAMES, altNames);
		this.settingsService.save(domainSettings);

		final Domain domain = this.get(info);
		this.publisher.publishEvent(new DomainCreatedEvent(domain));
		return domain;
	}

	@Getter
	private class DomainProps {

		private final boolean active;
		private final Set<String> alternativeNames;
		private final Path keySubDirPath;
		private final Path certSubDirPath;

		public DomainProps(final DomainInfo info) {
			final Settings d = DomainService.this.settingsService.getDomain(info);
			final Settings g = DomainService.this.settingsService.getGlobal();

			this.active = d.getBool(SettingsKey.DOMAIN_ACTIVATED);
			this.alternativeNames = d.getSet(SettingsKey.DOMAIN_ALTERNATIVE_NAMES);
			this.keySubDirPath = DomainService.this.fs.getPath(info.getDirectory().toString(), g.get(SettingsKey.DOMAIN_KEY_SUBDIR));
			this.certSubDirPath = DomainService.this.fs.getPath(info.getDirectory().toString(), g.get(SettingsKey.DOMAIN_CERT_SUBDIR));
		}
	}
}