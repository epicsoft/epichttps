/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.webserver;

import java.net.URL;

import org.springframework.stereotype.Service;

import de.epicsoft.epichttps.terminal.Process;
import de.epicsoft.epichttps.terminal.ProcessStatus;
import de.epicsoft.epichttps.terminal.Terminal;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CurlAPI {

	@NonNull
	private final Terminal terminal;

	public String getContent(final URL url, final String processMsg) {
		final String cmd = this.terminal.build("curl --connect-timeout 2 --silent -f -L", url.toExternalForm());
		final Process process = new Process(cmd, processMsg, /* silent */ true);
		this.terminal.execute(process);

		while (process.getStatus() != ProcessStatus.SUCCESS && process.getStatus() != ProcessStatus.FAILED) {
			try {
				log.debug("CURL get Content - Status::{}", process.getStatus());
				Thread.sleep(10);
			} catch (final InterruptedException e) {
				log.error(e.getMessage(), e);
			}
		}
		return process.getOutput();
	}
}
