/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.webserver;

import java.nio.file.Path;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Die Reload-Datei ist ein Kommunikationsweg aus dem Docker-Container heraus und soll einem Dienst (Watcher) mitteilen, sobald eine Aktulisierung vorliegt und der Webserver neu
 * geladen werden soll. Dies kann passieren, wenn z.B. sich das Zertifikat aendert oder Einstellungen vorgenommen wurden und dadurch die Konfigurationsdateien geaendert wurden.<br>
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@RequiredArgsConstructor
public abstract class AbstractWebserverService implements WebserverService {

	private static final String RELOAD_FILENAME = "reload";
	private static final long RELOAD_WAIT = 2000L;

	@NonNull
	protected final SettingsService settingsService;
	@NonNull
	protected final FileSystemAPI fs;

	// Ein Reload-Thread global fuer alle Threads und Services
	private Thread reloadThread = null;

	/**
	 * Ein Thread wird fuer X Sekunden gestartet und aktualisiert im Anschluss die Reload-Datei, sollten in dieser Zeit weitere Anfragen fuer den Reload eingehen, so werden sie
	 * ignoriert. Dies soll bewirken, dass die Meldungen fuer den Webserver-Neustart konsolidiert werden und nach Moeglichkeit zum Schluss eine einzige Datei aktulisiert (erstellt)
	 * wird und nicht mehrere hintereinander.
	 */
	@Override
	public synchronized void reloadWebserver() {
		if (this.reloadThread == null || !this.reloadThread.isAlive()) {
			this.reloadThread = this.buildReloadThred();
			this.reloadThread.start();
		}
	}

	private Thread buildReloadThred() {
		return new Thread(() -> {
			try {
				Thread.sleep(RELOAD_WAIT);
			} catch (final InterruptedException e) {
				log.error(e.getMessage(), e);
			}

			final Settings env = this.settingsService.getEnv();
			final Path reloadFile = this.fs.getPath(env.get(SettingsKey.LE_ROOT_DIR), env.get(SettingsKey.CUSTOM_SUBDIR), RELOAD_FILENAME);
			final String now = String.valueOf(ZonedDateTime.now(ZoneOffset.UTC).toEpochSecond());
			log.info("Send reload to Webserver at '{}'", now);
			this.fs.saveFile(reloadFile, now);
		});
	}
}