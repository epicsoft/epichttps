/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.webserver;

import java.nio.file.Path;

import de.epicsoft.epichttps.domain.Domain;
import de.epicsoft.epichttps.domain.DomainInfo;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public interface WebserverService {

	static final String WEBROOT_SUBDIR = "www";

	/**
	 * Erstellt eine Datei fuer das Hostsystem, im dem die letzte Aenderungszeit gesepeichert ist. Sobald sich die Datei aendert, soll der Webserver neugestartet werden.
	 */
	void reloadWebserver();

	/**
	 * Prueft ob fuer die Domain Aenderungen vorgenommen wurden und ein erneutes Bauen erforderlich ist.
	 *
	 * @param info {@link DomainInfo}
	 * @return {@link Boolean}
	 */
	Boolean isBuildNeeded(DomainInfo info);

	/**
	 * Initiale Erstellung der Konfiguration fuer die Application
	 */
	void createOrUpdateApplicationDomain();

	/**
	 * Erstellt oder aktulisiert alle erforderlichen Dateien fuer den Webserver fuer die Authentifizierung
	 *
	 * @param info {@link DomainInfo}
	 */
	void createOrUpdateAuth(DomainInfo info);

	/**
	 * Erstellt oder aktulisiert alle erforderlichen Dateien fuer den Webserver fuer die HTTPS Verschluesselung
	 *
	 * @param domain {@link Domain}
	 */
	void createOrUpdateHttps(Domain domain);

	/**
	 * Prueft fuer eine Domain, ob LE-Authentifizierung richtig konfiguriert und installiert wurde. Dabei wird eine Test-Datei angelegt und versucht ueber die Domain aufzurufen.
	 *
	 * @param info {@link DomainInfo}
	 * @return {@link Boolean}
	 */
	Boolean isAuthInstalled(DomainInfo info);

	/**
	 * Liefert den Pfad fur das Verzeichnis zum erstellen der Auth-Dateien
	 *
	 * @param info {@link DomainInfo}
	 * @return {@link Path}
	 */
	Path getAuthDir(DomainInfo info);

}