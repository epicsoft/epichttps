/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.webserver;

import java.net.URL;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Streams;

import de.epicsoft.epichttps.Profiles;
import de.epicsoft.epichttps.certificate.Certificate;
import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.common.TemplateService;
import de.epicsoft.epichttps.common.event.RedirectToHttpsUpdatedEvent;
import de.epicsoft.epichttps.common.exception.PrivateKeyNotFound;
import de.epicsoft.epichttps.domain.Domain;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.domain.DomainInfoService;
import de.epicsoft.epichttps.key.Key;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Service
@Profile(Profiles.NGINX)
public class NginxWebserverService extends AbstractWebserverService {

	private static final String NGINX_SUBDIR = "nginx";
	private static final String LE_AUTH_PATH = "/.well-known/acme-challenge/";
	private static final String AUTH_SCHEMA = "http://";
	private static final String AUTH_INCLUDE_TPL = "nginx_auth_include";
	private static final String AUTH_DOMAIN_TPL = "nginx_auth_domain";
	private static final String HTTPS_REDIRECT_TPL = "nginx_https_redirect";
	private static final String HTTPS_GLOBAL_REDIRECT_TPL = "nginx_https_global_redirect";
	private static final String REDIRECT_FILENAME = "redirect.inc";
	private static final String GLOBAL_REDIRECT_FILENAME = "redirect.conf";
	private static final String TLS_INCLUDE_TPL = "nginx_tls";
	private static final String EPICHTTPS_TPL = "nginx_epichttps";
	private static final String EPICHTTPS_FILENAME = "epichttps.conf";

	private static final String VALUE_ON = "on";
	private static final String VALUE_OFF = "off";

	@NonNull
	private final CurlAPI curl;
	@NonNull
	private final TemplateService templateService;
	@NonNull
	private final EpicMessageSource msg;
	@NonNull
	private final ApplicationEventPublisher publisher;
	@NonNull
	private final DomainInfoService domainInfoService;

	@Autowired
	public NginxWebserverService(final TemplateService templateService, final SettingsService settingsService, final FileSystemAPI fs, final CurlAPI curl,
			final EpicMessageSource msg, final ApplicationEventPublisher publisher, final DomainInfoService domainInfoService) {
		super(settingsService, fs);
		this.templateService = templateService;
		this.curl = curl;
		this.msg = msg;
		this.publisher = publisher;
		this.domainInfoService = domainInfoService;
	}

	@Override
	public Boolean isBuildNeeded(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		final NginxProps nginxProps = new NginxProps(info);
		Boolean buildNeeded = this.fs.isFileChanged(nginxProps.getAuthConfigFile(), this.buildAuthDomainTpl(nginxProps));
		buildNeeded = buildNeeded || this.fs.isFileChanged(nginxProps.getAuthIncludeFile(), this.buildAuthIncludeTpl(nginxProps));
		buildNeeded = buildNeeded || this.fs.isFileChanged(nginxProps.getRedirectFile(), this.buildRedirectToHttpsTpl(nginxProps));

		return buildNeeded;
	}

	@Override
	public void createOrUpdateApplicationDomain() {
		final Settings g = this.settingsService.getGlobal();
		final String authDomain = g.get(SettingsKey.AUTH_DOMAIN);
		final Path dir = this.fs.getPath(g.get(SettingsKey.LE_ROOT_DIR), g.get(SettingsKey.CUSTOM_SUBDIR), g.get(SettingsKey.LE_GENERAL_SUBDIR), g.get(SettingsKey.DOMAIN_CERT_SUBDIR));
		final DomainInfo info = DomainInfo.builder().id("authepichttps").name(authDomain).directory(dir).build();

		final NginxProps nginxProps = new NginxProps(info);
		this.createEpicHttpsDomain(nginxProps);
	}

	@Override
	public void createOrUpdateAuth(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		final NginxProps nginxProps = new NginxProps(info);
		this.updateAuthDomain(nginxProps);
		this.updateAuthInclude(nginxProps);
		this.updateRedirectToHttps(nginxProps);
		this.updateGlobalRedirectToHttps(this.domainInfoService.getAll());
	}

	@Override
	public void createOrUpdateHttps(final Domain domain) {
		Assert.notNull(domain, "Domain  must be not null");
		Assert.notNull(domain.getCurrentPrivateKey(), "Private Key must be not null");
		Assert.notNull(domain.getDhParam(), "DH Param must be not null");
		Assert.notNull(domain.getPrivateKeys(), "Private Keys must be not null");
		Assert.notNull(domain.getBackupKeys(), "Backup Keys must be not null");
		Assert.notNull(domain.getCurrentCertificate(), "Current certificate must be not null");

		final NginxProps nginxProps = new NginxProps(domain.getInfo());
		this.updateTlsInclude(nginxProps, domain);
	}

	@Override
	public Boolean isAuthInstalled(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		final NginxProps nginxProps = new NginxProps(info);
		if (nginxProps.isAuthInstalledForceSuccess()) {
			return true;
		}

		log.debug("Auth Test for {}", info.getName());
		final Path testFile = this.buildAuthTestFile(nginxProps);
		try {
			final URL testUrl = new URL(String.format("%s%s%s%s", AUTH_SCHEMA, info.getName(), LE_AUTH_PATH, testFile.getFileName().toString()));
			log.debug("Auth Test URL '{}'", testUrl);
			final String content = this.curl.getContent(testUrl, this.msg.getMessage("auth.process.check", info.getName()));
			return StringUtils.hasText(content) && content.contains(testFile.getFileName().toString());
		} catch (final Exception e) {
			log.error(e.getMessage(), e);
		} finally {
			this.fs.deleteFile(testFile);
		}
		return false;
	}

	@Override
	public Path getAuthDir(final DomainInfo info) {
		return new NginxProps(info).getWebrootDir();
	}

	/**
	 * Erstellt eine zufaellige Datei mit selben Inhalt wie der Name und speichert diese im Webroot Verzeichnis ab.
	 *
	 * @return {@link Path}
	 */
	private Path buildAuthTestFile(final NginxProps nginxProps) {
		final String testContent = UUID.randomUUID().toString();

		final Path testFile = this.fs.getPath(nginxProps.getWebrootDir().toString(), LE_AUTH_PATH, testContent);
		this.fs.saveFile(testFile, testContent);
		return testFile;
	}

	private void updateAuthDomain(final NginxProps nginxProps) {
		final String authDomainContent = this.buildAuthDomainTpl(nginxProps);
		final Path authDomainFile = nginxProps.getAuthConfigFile();

		if (this.fs.saveFileIfChanged(authDomainFile, authDomainContent)) {
			log.info("Nginx auth subdomain file updated '{}'", authDomainFile.toString());
			this.reloadWebserver();
		} else {
			log.debug("Nginx auth subdomain file is already up to date '{}'", authDomainFile.toString());
		}
	}

	private String buildAuthDomainTpl(final NginxProps nginxProps) {
		final String ipv4 = Strings.isNullOrEmpty(nginxProps.getAuthIpv4()) ? "" : String.format("%s:", nginxProps.getAuthIpv4());
		final String ipv6 = Strings.isNullOrEmpty(nginxProps.getAuthIpv6()) ? "::" : nginxProps.getAuthIpv6();

		final Map<String, Object> data = new HashMap<>();
		data.put("ipv4", ipv4);
		data.put("ipv6", ipv6);
		data.put("webroot", nginxProps.getHostWebrootDir());
		data.put("port", nginxProps.getAuthPort());
		data.put("domain", nginxProps.getAuthDomain());
		return this.templateService.render(AUTH_DOMAIN_TPL, data);
	}

	private void updateAuthInclude(final NginxProps nginxProps) {
		final String authIncludeContent = this.buildAuthIncludeTpl(nginxProps);
		final Path authIncludeFile = nginxProps.getAuthIncludeFile();
		if (this.fs.saveFileIfChanged(authIncludeFile, authIncludeContent)) {
			log.info("Nginx auth include file updated '{}'", authIncludeFile.toString());
			this.reloadWebserver();
		} else {
			log.debug("Nginx auth include file is already up to date '{}'", authIncludeFile.toString());
		}
	}

	private String buildAuthIncludeTpl(final NginxProps nginxProps) {
		final Map<String, Object> data = new HashMap<>();
		data.put("hostauthincludefile", nginxProps.getHostAuthIncludeFile().toString());
		data.put("domain", nginxProps.getAuthDomain());
		data.put("port", nginxProps.getAuthPort());
		return this.templateService.render(AUTH_INCLUDE_TPL, data);
	}

	private void updateRedirectToHttps(final NginxProps nginxProps) {
		final String redirectTpl = this.buildRedirectToHttpsTpl(nginxProps);
		final Path redirectFile = nginxProps.getRedirectFile();
		if (this.fs.saveFileIfChanged(redirectFile, redirectTpl)) {
			log.info("Nginx HTTP redirect file updated '{}'", redirectFile.toString());
			this.publisher.publishEvent(new RedirectToHttpsUpdatedEvent());
			this.reloadWebserver();
		} else {
			log.debug("Nginx HTTP redirect file is already up to date '{}'", redirectFile.toString());
		}
	}

	private String buildRedirectToHttpsTpl(final NginxProps nginxProps) {
		final int httpPort = nginxProps.getHttpPort();

		final Set<String> ipv4 = nginxProps.getIpv4().stream().map(ip -> String.format("%s:%s", ip, httpPort)).collect(Collectors.toSet());
		if (CollectionUtils.isEmpty(ipv4)) {
			ipv4.add(String.valueOf(httpPort));
		}
		final Set<String> ipv6 = nginxProps.getIpv6().stream().map(ip -> String.format("[%s]:%s", ip, httpPort)).collect(Collectors.toSet());
		if (CollectionUtils.isEmpty(ipv6)) {
			ipv6.add(String.format("[::]:%s", httpPort));
		}

		final Map<String, Object> data = new HashMap<>();
		data.put("ipv4", ipv4);
		data.put("ipv6", ipv6);
		data.put("domains", nginxProps.getAlternativeNames());
		data.put("hostauthincludefile", nginxProps.getHostAuthIncludeFile().toString());
		data.put("httpsport", nginxProps.getHttpsPort());

		return this.templateService.render(HTTPS_REDIRECT_TPL, data);
	}

	private void updateGlobalRedirectToHttps(final Set<DomainInfo> infos) {
		if (infos.size() > 0) {
			final Set<String> domains = new HashSet<>();
			infos.forEach(info -> {
				final NginxProps nginxProps = new NginxProps(info);
				if (this.fs.isFile(this.fs.getPath(nginxProps.getNginxSubDir().toString(), REDIRECT_FILENAME))) {
					domains.add(this.fs.getPath(nginxProps.getHostNginxDir().toString(), REDIRECT_FILENAME).toString());
				}
			});

			final Map<String, Object> data = new HashMap<>();
			data.put("domains", domains);
			final String redirectTpl = this.templateService.render(HTTPS_GLOBAL_REDIRECT_TPL, data);

			final NginxProps nginxProps = new NginxProps(infos.iterator().next());
			final Path redirectFile = this.fs.getPath(nginxProps.getGlobalNginxDir().toString(), GLOBAL_REDIRECT_FILENAME);

			if (this.fs.saveFileIfChanged(redirectFile, redirectTpl)) {
				log.info("Nginx HTTP global redirect file updated '{}'", redirectFile.toString());
				this.reloadWebserver();
			} else {
				log.debug("Nginx HTTP global redirect file is already up to date '{}'", redirectFile.toString());
			}
		} else {
			log.debug("Nginx HTTP global redirect file: domains not given");
		}
	}

	private void createEpicHttpsDomain(final NginxProps nginxProps) {
		final String ipv4 = Strings.isNullOrEmpty(nginxProps.getAuthIpv4()) ? "" : String.format("%s:", nginxProps.getAuthIpv4());
		final String ipv6 = Strings.isNullOrEmpty(nginxProps.getAuthIpv6()) ? "::" : nginxProps.getAuthIpv6();

		final Map<String, Object> data = new HashMap<>();
		data.put("host_global_nginx", nginxProps.getHostGlobalNginxDir().toString());
		data.put("filename", EPICHTTPS_FILENAME);
		data.put("ipv4", ipv4);
		data.put("ipv6", ipv6);
		data.put("host_webroot", nginxProps.getHostWebrootDir().toString());
		data.put("domain", nginxProps.getAuthDomain());
		data.put("host_global_cert", nginxProps.getHostGlobalCertDir().toString());
		data.put("proxy_pass", nginxProps.getProxyPath());
		final String epucHttpsTpl = this.templateService.render(EPICHTTPS_TPL, data);

		final Path epicHttpsPath = this.fs.getPath(nginxProps.getGlobalNginxDir().toString(), EPICHTTPS_FILENAME);
		this.fs.saveFileIfChanged(epicHttpsPath, epucHttpsTpl);
		log.info("Nginx configuration created '{}'", epicHttpsPath.toString());
		this.reloadWebserver();
	}

	private void updateTlsInclude(final NginxProps nginxProps, final Domain domain) {
		final String tlsIncludeContent = this.buildTlsIncludeTpl(nginxProps, domain);
		final Path tlsIncludeFile = nginxProps.getTlsIncludeFile();

		if (this.fs.saveFileIfChanged(tlsIncludeFile, tlsIncludeContent)) {
			log.info("Nginx TLS include file updated '{}'", tlsIncludeFile.toString());
			this.reloadWebserver();
		} else {
			log.debug("Nginx TLS include file is already up to date '{}'", tlsIncludeFile.toString());
		}
	}

	private String buildTlsIncludeTpl(final NginxProps nginxProps, final Domain domain) {
		/* @formatter:off */
		final Set<String> resolverIps = Stream.concat(
					nginxProps.getResolverIpv4().stream(),
					nginxProps.getResolverIpv6().stream().map(ip -> String.format("[%s]", ip)))
				.collect(Collectors.toSet());
		/* @formatter:on */
		final Key usedPrivateKey = this.getPrivateKeyForCertificate(domain.getCurrentCertificate(), domain.getPrivateKeys(), domain.getAllPrivateKeys());

		final Map<String, Object> data = new HashMap<>();
		data.put("server_tokens_enable", nginxProps.isServerTokensEnable());
		data.put("server_tokens", nginxProps.isServerTokens() ? VALUE_ON : VALUE_OFF);
		data.put("header_content_type_option_enable", nginxProps.isHeaderContentTypeOptionsEnable());
		data.put("header_content_type_option", nginxProps.getHeaderContentTypeOptions());
		data.put("header_xss_protection_enable", nginxProps.isHeaderXssProtectionEnable());
		data.put("header_xss_protection", nginxProps.getHeaderXssProtection());
		data.put("header_frame_option_enable", nginxProps.isHeaderFrameOptionEnable());
		data.put("header_frame_option", nginxProps.getHeaderFrameOption());
		data.put("header_hsts_enable", nginxProps.isHstsEnable());
		data.put("hsts_content", this.buildHstsHeaderContent(nginxProps));
		data.put("header_hpkp_enable", nginxProps.isHpkpEnable());
		data.put("hpkp_content", this.buildHpkpHeaderContent(nginxProps, domain, nginxProps.getHpkpReportEnforce()));
		data.put("header_hpkp_report_enable", nginxProps.isHpkpReportEnalbe());
		data.put("hpkp_report_content", this.buildHpkpHeaderContent(nginxProps, domain, nginxProps.getHpkpReportOny()));
		data.put("cert_dir", nginxProps.getHostCertDir().toString());
		data.put("key_dir", nginxProps.getHostKeyDir().toString());
		data.put("cert_file", domain.getCurrentCertificate().getFile().getFileName());
		data.put("cert_key", usedPrivateKey.getKeyFile().getFileName());
		data.put("dhparam", domain.getDhParam().getName());
		data.put("dhparam_enable", nginxProps.isSslDhParamEnable());
		data.put("session_timeout_enable", nginxProps.isSslSessionTimeoutEnable());
		data.put("session_timeout", nginxProps.getSslSessionTimeout());
		data.put("session_cache_enable", nginxProps.isSslSettionCacheEnable());
		data.put("session_cache", nginxProps.getSslSettionCache());
		data.put("session_tickets_enable", nginxProps.isSslSessionTicketsEnable());
		data.put("session_tickets", nginxProps.isSslSessionTickets() ? VALUE_ON : VALUE_OFF);
		data.put("protocols", nginxProps.getSslProtocols());
		data.put("ciphers", nginxProps.getSslCiphers());
		data.put("prefer_server_ciphers_enable", nginxProps.isSslPreferServerCiphersEnable());
		data.put("prefer_server_ciphers", nginxProps.isSslPreferServerCiphers() ? VALUE_ON : VALUE_OFF);
		data.put("stapling_enable", nginxProps.isSslStaplingEnable());
		data.put("stapling", nginxProps.isSslStapling() ? VALUE_ON : VALUE_OFF);
		data.put("stapling_verify_enable", nginxProps.isSslStaplingVerifyEnable());
		data.put("stapling_verify", nginxProps.isSslStaplingVerify() ? VALUE_ON : VALUE_OFF);
		data.put("trusted_certificate_enable", nginxProps.isSslTrustedCertificateEnable());
		data.put("resolver_timeout_enable", nginxProps.isResolverTimeoutEnable());
		data.put("resolver_timeout", nginxProps.getResolverTimeout());
		data.put("resolver_enable", nginxProps.isResolverIpv4Enable() || nginxProps.isResolverIpv6Enable());
		data.put("resolver_ip", Joiner.on(" ").join(resolverIps));

		return this.templateService.render(TLS_INCLUDE_TPL, data);
	}

	/**
	 * Passenden privaten Schluessel zum verwendeten Zertifikat ermitteln
	 *
	 * @param certificate {@link Certificate}
	 * @return {@link Key}
	 */
	private Key getPrivateKeyForCertificate(final Certificate certificate, final Set<Key> privateKeys, final Set<Key> allPrivateKeys) {
		final String certificatePin = certificate.getPin();
		/* @formatter:off */
		return privateKeys.stream()
		  .filter(key -> key.getPin().equals(certificatePin))
			.findFirst()
			.orElseGet(() -> {
			  final Key invalidPrivateKey = allPrivateKeys.stream()
			    .filter(key -> key.getPin().equals(certificatePin))
				  .findFirst()
				  .orElseThrow(() -> new PrivateKeyNotFound(String.format("private key with pin '%s' not found", certificatePin)));
			  log.error("use invalid private key with pin '{}'", certificatePin);
			  return invalidPrivateKey;
			});
		/* @formatter:on */
	}

	private String buildHstsHeaderContent(final NginxProps nginxProps) {
		final StringBuilder content = new StringBuilder();

		content.append(String.format("max-age=%d", nginxProps.getHstsMaxAge()));
		content.append(nginxProps.isHstsIncludeSubdomains() ? "; includeSubDomains" : "");
		content.append(nginxProps.isHstsPreload() ? "; preload" : "");
		return content.toString();
	}

	private String buildHpkpHeaderContent(final NginxProps nginxProps, final Domain domain, final String reportUri) {
		final StringBuilder content = new StringBuilder();

		// private-pins + backup-pins + sonstige pins
		final Set<String> pins = Streams.stream(Iterables.concat(domain.getPrivateKeys(), domain.getBackupKeys())).map(Key::getPin).collect(Collectors.toSet());
		pins.addAll(nginxProps.getHpkpAddPins());

		pins.stream().forEach(pin -> {
			content.append(content.length() > 0 ? "; " : "");
			content.append(String.format("pin-sha256=\"%s\"", pin));
		});

		content.append(String.format("; max-age=%d", nginxProps.getHpkpMaxAge()));
		content.append(nginxProps.isHpkpIncludeSubdomains() ? "; includeSubDomains" : "");

		if (UrlValidator.getInstance().isValid(reportUri)) {
			content.append(String.format("; report-uri=\"%s\"", reportUri));
		}
		return content.toString();
	}

	@Getter
	private class NginxProps {

		private final Path hostCustomDir; // custom
		private final Path hostDomainDir;
		private final Path hostNginxDir;
		private final Path hostCertDir;
		private final Path hostKeyDir;
		private final Path hostGlobalNginxDir; // custom/global/nginx
		private final Path hostGlobalCertDir; // custom/global/cert
		private final Path hostWebrootDir; // custom/global/nginx/www
		private final Path hostAuthIncludeFile;
		private final Path hostAuthConfigFile;
		private final Path hostTlsIncludeFile;

		private final Path customDir;
		private final Path domainDir;
		private final Path nginxSubDir;
		private final Path globalNginxDir;
		private final Path webrootDir;
		private final Path authIncludeFile;
		private final Path authConfigFile;
		private final Path redirectFile;
		private final Path tlsIncludeFile;

		private final boolean authInstalledForceSuccess;
		private final boolean isAuthDomain;
		private final String authDomain;
		private final int authPort;
		private final String authIpv4;
		private final String authIpv6;
		private final int httpPort;
		private final int httpsPort;
		private final Set<String> ipv4;
		private final Set<String> ipv6;
		private final Set<String> alternativeNames;
		private final String proxyPath;

		private final boolean serverTokensEnable;
		private final boolean resolverIpv4Enable;
		private final boolean resolverIpv6Enable;
		private final boolean resolverTimeoutEnable;
		private final boolean sslSettionCacheEnable;
		private final boolean sslSessionTimeoutEnable;
		private final boolean sslSessionTicketsEnable;
		private final boolean sslPreferServerCiphersEnable;
		private final boolean sslStaplingEnable;
		private final boolean sslStaplingVerifyEnable;
		private final boolean sslEcdhCurveEnable;
		private final boolean sslDhParamEnable;
		private final boolean headerFrameOptionEnable;
		private final boolean headerContentTypeOptionsEnable;
		private final boolean headerXssProtectionEnable;
		private final boolean sslTrustedCertificateEnable;

		private final boolean serverTokens;
		private final Set<String> resolverIpv4;
		private final Set<String> resolverIpv6;
		private final String resolverTimeout;
		private final String sslSettionCache;
		private final String sslSessionTimeout;
		private final boolean sslSessionTickets;
		private final boolean sslPreferServerCiphers;
		private final boolean sslStapling;
		private final boolean sslStaplingVerify;
		private final String sslEcdhCurve;
		private final String headerFrameOption;
		private final String headerContentTypeOptions;
		private final String headerXssProtection;
		private final String sslProtocols;
		private final String sslCiphers;

		private final boolean hstsEnable;
		private final Integer hstsMaxAge;
		private final boolean hstsIncludeSubdomains;
		private final boolean hstsPreload;

		private final boolean hpkpEnable;
		private final Integer hpkpMaxAge;
		private final boolean hpkpIncludeSubdomains;
		private final Set<String> hpkpAddPins;
		private final boolean hpkpReportEnalbe;
		private final String hpkpReportEnforce;
		private final String hpkpReportOny;

		public NginxProps(final DomainInfo info) {
			final Settings e = NginxWebserverService.this.settingsService.getEnv();
			final Settings g = NginxWebserverService.this.settingsService.getGlobal();
			final Settings d = NginxWebserverService.this.settingsService.getDomain(info);
			final Settings h = d.getBool(SettingsKey.OVERWRITE_HSTS_HPKP) ? d : g;
			final Settings w = d.getBool(SettingsKey.OVERWRITE_WEBSERVER) ? d : g;

			this.hostCustomDir = NginxWebserverService.this.fs.getPath(e.get(SettingsKey.HOST_PATH), g.get(SettingsKey.CUSTOM_SUBDIR));
			this.hostDomainDir = NginxWebserverService.this.fs.getPath(this.hostCustomDir.toString(), info.getName());
			this.hostNginxDir = NginxWebserverService.this.fs.getPath(this.hostDomainDir.toString(), NGINX_SUBDIR);
			this.hostCertDir = NginxWebserverService.this.fs.getPath(this.hostDomainDir.toString(), g.get(SettingsKey.DOMAIN_CERT_SUBDIR));
			this.hostKeyDir = NginxWebserverService.this.fs.getPath(this.hostDomainDir.toString(), g.get(SettingsKey.DOMAIN_KEY_SUBDIR));
			this.hostGlobalNginxDir = NginxWebserverService.this.fs.getPath(this.hostCustomDir.toString(), g.get(SettingsKey.LE_GENERAL_SUBDIR), NGINX_SUBDIR);
			this.hostGlobalCertDir = NginxWebserverService.this.fs.getPath(this.hostCustomDir.toString(), g.get(SettingsKey.LE_GENERAL_SUBDIR), e.get(SettingsKey.DOMAIN_CERT_SUBDIR));
			this.hostWebrootDir = NginxWebserverService.this.fs.getPath(this.hostGlobalNginxDir.toString(), WEBROOT_SUBDIR);
			this.hostAuthIncludeFile = NginxWebserverService.this.fs.getPath(this.hostNginxDir.toString(), g.get(SettingsKey.AUTH_INCLUDE_FILENAME));
			this.hostAuthConfigFile = NginxWebserverService.this.fs.getPath(this.hostGlobalNginxDir.toString(), g.get(SettingsKey.AUTH_CONFIGURATION_FILENAME));
			this.hostTlsIncludeFile = NginxWebserverService.this.fs.getPath(this.hostNginxDir.toString(), g.get(SettingsKey.NGINX_TLS_INCLUDE_FILENAME));

			this.customDir = NginxWebserverService.this.fs.getPath(e.get(SettingsKey.LE_ROOT_DIR), g.get(SettingsKey.CUSTOM_SUBDIR));
			this.domainDir = NginxWebserverService.this.fs.getPath(this.customDir.toString(), info.getName());
			this.nginxSubDir = NginxWebserverService.this.fs.getPath(this.domainDir.toString(), NGINX_SUBDIR);
			this.globalNginxDir = NginxWebserverService.this.fs.getPath(this.customDir.toString(), g.get(SettingsKey.LE_GENERAL_SUBDIR), NGINX_SUBDIR);
			this.webrootDir = NginxWebserverService.this.fs.getPath(this.globalNginxDir.toString(), WEBROOT_SUBDIR);
			this.authIncludeFile = NginxWebserverService.this.fs.getPath(this.nginxSubDir.toString(), g.get(SettingsKey.AUTH_INCLUDE_FILENAME));
			this.authConfigFile = NginxWebserverService.this.fs.getPath(this.globalNginxDir.toString(), g.get(SettingsKey.AUTH_CONFIGURATION_FILENAME));
			this.redirectFile = NginxWebserverService.this.fs.getPath(this.nginxSubDir.toString(), REDIRECT_FILENAME);
			this.tlsIncludeFile = NginxWebserverService.this.fs.getPath(this.nginxSubDir.toString(), g.get(SettingsKey.NGINX_TLS_INCLUDE_FILENAME));

			this.authInstalledForceSuccess = g.getBool(SettingsKey.AUTH_INSTALLED_FORCE_SUCCESS);
			this.isAuthDomain = g.get(SettingsKey.AUTH_DOMAIN).equals(info.getName());
			this.authDomain = g.get(SettingsKey.AUTH_DOMAIN);
			this.authPort = g.getInt(SettingsKey.AUTH_PORT);
			this.authIpv4 = g.get(SettingsKey.AUTH_IPV4);
			this.authIpv6 = g.get(SettingsKey.AUTH_IPV6);
			this.httpPort = d.getInt(SettingsKey.DOMAIN_HTTP_PORT);
			this.httpsPort = d.getInt(SettingsKey.DOMAIN_HTTPS_PORT);
			this.ipv6 = d.getSet(SettingsKey.DOMAIN_IPV6);
			this.ipv4 = d.getSet(SettingsKey.DOMAIN_IPV4);
			this.alternativeNames = d.getSet(SettingsKey.DOMAIN_ALTERNATIVE_NAMES);
			this.proxyPath = g.get(SettingsKey.EPICHTTPS_PROXY_PATH);

			this.serverTokensEnable = w.getBool(SettingsKey.NGINX_SERVER_TOKENS_ENABLE);
			this.resolverIpv4Enable = w.getBool(SettingsKey.NGINX_RESOLVER_IPV4_ENABLE);
			this.resolverIpv6Enable = w.getBool(SettingsKey.NGINX_RESOLVER_IPV6_ENABLE);
			this.resolverTimeoutEnable = w.getBool(SettingsKey.NGINX_RESOLVER_TIMEOUT_ENABLE);
			this.sslSettionCacheEnable = w.getBool(SettingsKey.NGINX_SSL_SESSION_CACHE_ENABLE);
			this.sslSessionTimeoutEnable = w.getBool(SettingsKey.NGINX_SSL_SESSION_TIMEOUT_ENABLE);
			this.sslSessionTicketsEnable = w.getBool(SettingsKey.NGINX_SSL_SESSION_TICKETS_ENABLE);
			this.sslPreferServerCiphersEnable = w.getBool(SettingsKey.NGINX_SSL_PREFER_SERVER_CIPHERS_ENABLE);
			this.sslStaplingEnable = w.getBool(SettingsKey.NGINX_SSL_STAPLING_ENABLE);
			this.sslStaplingVerifyEnable = w.getBool(SettingsKey.NGINX_SSL_STAPLING_VERIFY_ENABLE);
			this.sslEcdhCurveEnable = w.getBool(SettingsKey.NGINX_SSL_ECDH_CURVE_ENABLE);
			this.sslDhParamEnable = w.getBool(SettingsKey.NGINX_SSL_DH_PARAM_ENABLE);
			this.headerFrameOptionEnable = w.getBool(SettingsKey.NGINX_HEADER_FRAME_OPTION_ENABLE);
			this.headerContentTypeOptionsEnable = w.getBool(SettingsKey.NGINX_HEADER_CONTENT_TYPE_OPTIONS_ENABLE);
			this.headerXssProtectionEnable = w.getBool(SettingsKey.NGINX_HEADER_XSS_PROTECTION_ENABLE);
			this.sslTrustedCertificateEnable = w.getBool(SettingsKey.NGINX_SSL_TRUSTED_CERTIFICATE_ENABLE);

			this.serverTokens = w.getBool(SettingsKey.NGINX_SERVER_TOKENS);
			this.resolverIpv4 = w.getSet(SettingsKey.NGINX_RESOLVER_IPV4);
			this.resolverIpv6 = w.getSet(SettingsKey.NGINX_RESOLVER_IPV6);
			this.resolverTimeout = w.get(SettingsKey.NGINX_RESOLVER_TIMEOUT);
			this.sslSettionCache = w.get(SettingsKey.NGINX_SSL_SESSION_CACHE);
			this.sslSessionTimeout = w.get(SettingsKey.NGINX_SSL_SESSION_TIMEOUT);
			this.sslSessionTickets = w.getBool(SettingsKey.NGINX_SSL_SESSION_TICKETS);
			this.sslPreferServerCiphers = w.getBool(SettingsKey.NGINX_SSL_PREFER_SERVER_CIPHERS);
			this.sslStapling = w.getBool(SettingsKey.NGINX_SSL_STAPLING);
			this.sslStaplingVerify = w.getBool(SettingsKey.NGINX_SSL_STAPLING_VERIFY);
			this.sslEcdhCurve = w.get(SettingsKey.NGINX_SSL_ECDH_CURVE);
			this.headerFrameOption = w.get(SettingsKey.NGINX_HEADER_FRAME_OPTION);
			this.headerContentTypeOptions = w.get(SettingsKey.NGINX_HEADER_CONTENT_TYPE_OPTIONS);
			this.headerXssProtection = w.get(SettingsKey.NGINX_HEADER_XSS_PROTECTION);
			this.sslProtocols = w.get(SettingsKey.NGINX_SSL_PROTOCOLS);
			this.sslCiphers = w.get(SettingsKey.NGINX_SSL_CIPHERS);

			this.hstsEnable = h.getBool(SettingsKey.HSTS_ENABLE);
			this.hstsMaxAge = h.getInt(SettingsKey.HSTS_MAX_AGE);
			this.hstsIncludeSubdomains = h.getBool(SettingsKey.HSTS_INCLUDE_SUBDOMAINS);
			this.hstsPreload = h.getBool(SettingsKey.HSTS_PRELOAD);

			this.hpkpEnable = h.getBool(SettingsKey.HPKP_ENABLE);
			this.hpkpMaxAge = h.getInt(SettingsKey.HPKP_MAX_AGE);
			this.hpkpIncludeSubdomains = h.getBool(SettingsKey.HPKP_INCLUDE_SUBDOMAINS);
			this.hpkpAddPins = h.getSet(SettingsKey.HPKP_ADD_PINS);
			this.hpkpReportEnalbe = h.getBool(SettingsKey.HPKP_REPORT_ENABLE);
			this.hpkpReportEnforce = h.get(SettingsKey.HPKP_REPORT_ENFORCE);
			this.hpkpReportOny = h.get(SettingsKey.HPKP_REPORT_ONLY);
		}
	}
}