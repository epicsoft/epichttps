/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.key;

import java.nio.file.Path;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.google.common.collect.ImmutableSet;

import de.epicsoft.epichttps.common.EpicDateTime;
import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.common.ImmutableSetCollector;
import de.epicsoft.epichttps.common.OpenSslAPI;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.settings.KeySettings;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class KeyService {

	private static final String PRIVATE_KEY_SETTINGS_TPL = "private_key_%s.properties";
	private static final String PRIVATE_KEY_TPL = "private_key_%s.pem";
	private static final String PRIVATE_KEY_REGEX = "^private_key_(\\w{32})\\.properties$";
	private static final Pattern PRIVATE_KEY_PATTERN = Pattern.compile(PRIVATE_KEY_REGEX);

	private static final String BACKUP_KEY_SETTINGS_TPL = "backup_key_%s.properties";
	private static final String BACKUP_KEY_TPL = "backup_key_%s.pem";
	private static final String BACKUP_KEY_REGEX = "^backup_key_(\\w{32})\\.properties$";
	private static final Pattern BACKUP_KEY_PATTERN = Pattern.compile(BACKUP_KEY_REGEX);

	@NonNull
	private final SettingsService settingsService;
	@NonNull
	private final OpenSslAPI openSsl;
	@NonNull
	private final FileSystemAPI fs;
	@NonNull
	private final EpicMessageSource msg;

	public Key getCurrentPrivateKey(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		/* @formatter:off */
		return this.getValidPrivateKeys(info).stream()
			.sorted(new KeyCreateDateComperator())
			.findFirst()
			.orElse(null);
		/* @formatter:on */
	}

	public Key getLastExpirePrivateKey(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		/* @formatter:off */
		return this.getValidPrivateKeys(info).stream()
				.sorted((key1, key2) -> key2.getExpireDate().compareTo(key1.getExpireDate()))
				.findFirst()
				.orElse(null);
		/* @formatter:on */
	}

	/**
	 * Liefert ein {@link Set} mit allen Private-Keys. Liefert immer ein Object zurueck, kann auch ein leeres {@link Set} sein.
	 *
	 * @param domainInfo {@link DomainInfo}
	 * @return {@link ImmutableSet} mit {@link Key}
	 */
	public ImmutableSet<Key> getAllPrivateKeys(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		/* @formatter:off */
		return this.getAll(info).stream()
				.filter(key -> key.getType() == KeyType.PRIVATE)
				.sorted(new KeyCreateDateComperator())
				.collect(new ImmutableSetCollector<>());
		/* @formatter:on */
	}

	/**
	 * Liefert ein {@link Set} mit allen gueltigen Private-Keys. Liefert immer ein Object zurueck, kann auch ein leeres {@link Set} sein.
	 *
	 * @param domainInfo {@link DomainInfo}
	 * @return {@link ImmutableSet} mit {@link Key}
	 */
	public ImmutableSet<Key> getValidPrivateKeys(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		/* @formatter:off */
		return this.getAllValid(info).stream()
				.filter(key -> key.getType() == KeyType.PRIVATE)
				.sorted(new KeyCreateDateComperator())
				.collect(new ImmutableSetCollector<>());
		/* @formatter:on */
	}

	/**
	 * Liefert ein {@link Set} mit allen gueltigen Backup-Keys. Liefert immer ein Object zurueck, kann auch ein leeres {@link Set} sein.
	 *
	 * @param domainInfo {@link DomainInfo}
	 * @return {@link ImmutableSet} mit {@link Key}
	 */
	public ImmutableSet<Key> getValidBackupKeys(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		/* @formatter:off */
		return this.getAllValid(info).stream()
				.filter(key -> key.getType() == KeyType.BACKUP)
				.sorted(new KeyCreateDateComperator())
				.collect(new ImmutableSetCollector<>());
		/* @formatter:on */
	}

	public Set<Key> getAllValid(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		/* @formatter:off */
		return this.getAll(info).stream()
				.filter(key -> !key.isExpire())
				.filter(key -> EpicDateTime.now().isBefore(key.getExpireDate()))
				.sorted(new KeyCreateDateComperator())
				.collect(Collectors.toSet());
		/* @formatter:on */
	}

	/**
	 * Erstellt je nach Einstellungen private und backup Schluessel incl. passende PINs. Dabei wird die Anzahl der Schluessel beruecksichtigt und ob diese aktiviert sind oder nicht
	 * z.B. backup Schluessel.
	 *
	 * @param info {@link DomainInfo}
	 */
	public void createOrUpdate(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		final KeyProps keyProps = new KeyProps(info);
		this.expirePrivateKeysInToleranceDays(info);

		final int privateKeyQuantity = keyProps.isPrivateKeyRotationEnable() ? keyProps.getPrivateKeyQuantity() : 1;
		log.trace("Domain: '{}' - private key rotation: '{}' - private key quantity: '{}'", info.getName(), keyProps.isPrivateKeyRotationEnable(), privateKeyQuantity);
		final Set<Key> privateKeys = this.getValidPrivateKeys(info);
		this.buildKeys(info, KeyType.PRIVATE, privateKeys, privateKeyQuantity);

		final Set<Key> backupKeys = this.getValidBackupKeys(info);
		if (keyProps.isBackupKeyEnable() && backupKeys.size() < keyProps.getBackupKeyQuantity()) {
			this.buildKeys(info, KeyType.BACKUP, backupKeys, keyProps.getBackupKeyQuantity());
		}
	}

	private void expirePrivateKeysInToleranceDays(final DomainInfo info) {
		final KeyProps keyProps = new KeyProps(info);
		if (keyProps.isPrivateKeyRotationEnable()) {
			/* @formatter:off */
			this.getValidPrivateKeys(info).stream()
				.filter(key -> EpicDateTime.now().isAfter(key.getExpireDate().minusDays(keyProps.getPrivateKeyToleranceDays())))
				.forEach(key -> {
					log.debug("Domain: '{}' - set private key expire: '{}'", info.getName(), key.getKeyFile().toString());
					final KeySettings settings = key.getSettings();
					settings.set(SettingsKey.KEY_EXPIRE, true);
					this.settingsService.save(settings);
				});
			/* @formatter:on */
		}
	}

	public boolean isBuildNeeded(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		final KeyProps keyProps = new KeyProps(info);

		final Set<Key> privateKeys = this.getValidPrivateKeys(info);
		final int privateKeyQuantity = keyProps.isPrivateKeyRotationEnable() ? keyProps.getPrivateKeyQuantity() : 1;
		if (privateKeys.size() < privateKeyQuantity) {
			return true;
		}

		final Set<Key> backupKeys = this.getValidBackupKeys(info);
		if (keyProps.isBackupKeyEnable() && backupKeys.size() < keyProps.getBackupKeyQuantity()) {
			return true;
		}
		return false;
	}

	private Set<Key> getAll(final DomainInfo info) {
		final KeyProps keyProps = new KeyProps(info);
		/* @formatter:off */
		final Set<KeySettings> keySettings = this.fs.list(keyProps.getKeySubDirPath()).stream()
			.filter(file -> this.isKeySettings(file))
			.map(keyProperty -> this.settingsService.getKeySettings(keyProperty))
			.collect(Collectors.toSet());
		/* @formatter:on */

		final Set<Key> keys = new HashSet<>();
		keySettings.forEach(settings -> {
			final String keyType = settings.get(SettingsKey.KEY_TYPE);
			final KeyType type = KeyType.fromString(keyType);
			final String fileName = settings.get(SettingsKey.KEY_FILENAME);
			final Path file = this.fs.getPath(keyProps.getKeySubDirPath().toString(), fileName);
			final Boolean fileExists = this.fs.isFile(file);

			final Key key = Key.builder().type(type).info(info).keyProperty(settings.getKeyProperty()).settings(settings).keyFile(file).keyFileExists(fileExists).build();
			keys.add(key);
		});
		return keys;
	}

	/**
	 * Prueft ob aus der Datei ein Schluessel erstellt werden kann, dabei wird der private Schluessel und Backup-Hash akzeptiert. Backup-Hash wird verwendet, da der backup Schluessel
	 * evtl. an einen sicheren Ort verschoben wurde und somit nicht auf dem Dateisystem existiert.
	 *
	 * @param file {@link Path}
	 * @return {@link Boolean}
	 */
	private Boolean isKeySettings(final Path settingsFile) {
		final String fileName = settingsFile.getFileName().toString();
		return PRIVATE_KEY_PATTERN.matcher(fileName).find() || BACKUP_KEY_PATTERN.matcher(fileName).find();
	}

	private void buildKeys(final DomainInfo info, final KeyType type, final Set<Key> keys, final Integer quantity) {
		for (int i = 0; i < quantity - keys.size(); i++) {
			this.save(this.buildKey(type, info));
		}
	}

	/**
	 * protected for testing
	 */
	protected Key buildKey(final KeyType type, final DomainInfo info) {
		final String keyId = this.createKeyId();
		String keyName, keyPropertyName;
		if (type == KeyType.PRIVATE) {
			keyName = String.format(PRIVATE_KEY_TPL, keyId);
			keyPropertyName = String.format(PRIVATE_KEY_SETTINGS_TPL, keyId);
		} else if (type == KeyType.BACKUP) {
			keyName = String.format(BACKUP_KEY_TPL, keyId);
			keyPropertyName = String.format(BACKUP_KEY_SETTINGS_TPL, keyId);
		} else {
			throw new IllegalArgumentException(String.format("Unknow KeyType::%s", type));
		}

		final KeyProps keyProps = new KeyProps(info);
		final Path file = this.fs.getPath(keyProps.getKeySubDirPath().toString(), keyName);
		final Path keyPropertyFile = this.fs.getPath(keyProps.getKeySubDirPath().toString(), keyPropertyName);
		final KeySettings keySettings = new KeySettings(new Properties(), keyPropertyFile);

		log.debug("Domain: '{}' - build new key: '{} - {}'", info.getName(), type, keyName);
		return Key.builder().type(type).info(info).keyProperty(keyPropertyFile).settings(keySettings).keyFile(file).keyFileExists(false).build();
	}

	private String createKeyId() {
		return UUID.randomUUID().toString().replaceAll("\\{", "").replaceAll("\\}", "").replaceAll("-", "");
	}

	/**
	 * protected for testing
	 */
	protected void save(final Key key) {
		// default expire 100 years
		ZonedDateTime expire = EpicDateTime.now().plusYears(100);
		if (key.getType() == KeyType.PRIVATE) {
			final KeyProps keyProps = new KeyProps(key.getInfo());

			final Key lastKey = this.getLastExpirePrivateKey(key.getInfo());
			final ZonedDateTime lastExpire = lastKey != null ? lastKey.getExpireDate() : EpicDateTime.now();
			expire = lastExpire.plusDays(keyProps.getPrivateKeyPeriodValidityDays());
		}

		final String msgKey = key.getType() == KeyType.PRIVATE ? "key.process.create.private.key" : "key.process.create.backup.key";
		final String message = this.msg.getMessage(msgKey, key.getInfo().getName());
		final String pin = this.openSsl.createKey(key, message);

		final KeySettings settings = key.getSettings();
		settings.set(SettingsKey.KEY_EXPIRE, false);
		settings.set(SettingsKey.KEY_EXPIRE_DATE, expire);
		settings.set(SettingsKey.KEY_CREATE_DATE, EpicDateTime.now());
		settings.set(SettingsKey.KEY_PIN, pin);
		settings.set(SettingsKey.KEY_TYPE, key.getType().toString());
		settings.set(SettingsKey.KEY_FILENAME, key.getKeyFile().getFileName().toString());

		this.settingsService.save(settings);
		log.info("create new key '{}' for domain '{}'", key.getType(), key.getInfo().getName());
	}

	@Getter
	private class KeyProps {

		private final Path keySubDirPath;
		private final boolean privateKeyRotationEnable;
		private final int privateKeyQuantity;
		private final int privateKeyPeriodValidityDays;
		private final int privateKeyToleranceDays;
		private final boolean backupKeyEnable;
		private final int backupKeyQuantity;

		public KeyProps(final DomainInfo info) {
			final Settings g = KeyService.this.settingsService.getGlobal();
			final Settings d = KeyService.this.settingsService.getDomain(info);
			final Settings k = d.getBool(SettingsKey.OVERWRITE_KEYS) ? d : g;

			this.keySubDirPath = KeyService.this.fs.getPath(info.getDirectory().toString(), g.get(SettingsKey.DOMAIN_KEY_SUBDIR));

			this.privateKeyRotationEnable = k.getBool(SettingsKey.PRIVATE_KEY_ROTATION_ENABLE);
			this.privateKeyQuantity = k.getInt(SettingsKey.PRIVATE_KEY_QUANTITY);
			this.privateKeyPeriodValidityDays = k.getInt(SettingsKey.PRIVATE_KEY_PERIOD_VALIDITY_DAYS);
			this.privateKeyToleranceDays = k.getInt(SettingsKey.PRIVATE_KEY_TOLERANCE_DAYS);
			this.backupKeyEnable = k.getBool(SettingsKey.BACKUP_KEY_ENABLE);
			this.backupKeyQuantity = k.getInt(SettingsKey.BACKUP_KEY_QUANTITY);

		}
	}
}