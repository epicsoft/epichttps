/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.key;

import java.nio.file.Path;
import java.time.ZonedDateTime;

import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.settings.KeySettings;
import de.epicsoft.epichttps.settings.SettingsKey;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Builder
@ToString
@EqualsAndHashCode
public class Key {

	@Getter
	@NonNull
	private final KeyType type;

	@Getter
	@NonNull
	private final DomainInfo info;

	@Getter
	@NonNull
	private final Path keyProperty;

	@Getter(value = AccessLevel.PROTECTED)
	@NonNull
	private final KeySettings settings;

	@Getter
	@NonNull
	private final Boolean keyFileExists;

	// Kann bei einem Backup-Key null sein, wenn der Key an einen anderen (sicheren) Ort verschoben wurde.
	@Getter
	private final Path keyFile;

	public String getPin() {
		return this.settings.get(SettingsKey.KEY_PIN);
	}

	public Boolean isExpire() {
		return this.settings.getBool(SettingsKey.KEY_EXPIRE);
	}

	public ZonedDateTime getExpireDate() {
		return ZonedDateTime.parse(this.settings.get(SettingsKey.KEY_EXPIRE_DATE));
	}

	public ZonedDateTime getCreateDate() {
		return ZonedDateTime.parse(this.settings.get(SettingsKey.KEY_CREATE_DATE));
	}
}