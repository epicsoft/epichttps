/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.key;

import java.nio.file.Path;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.common.OpenSslAPI;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Service
@RequiredArgsConstructor
public class DhParamService {

	public static final String DH_PARAM_PREFIX = "dhparam_";
	public static final String DH_PARAM_TPL = DH_PARAM_PREFIX + "%s.pem"; // %s anstatt %d, da ein String erforderlich ist um in RegEx zu erstellen

	@NonNull
	private final SettingsService settingsService;
	@NonNull
	private final FileSystemAPI fs;
	@NonNull
	private final OpenSslAPI openSsl;
	@NonNull
	private final EpicMessageSource msg;

	public Boolean isBuildNeeded(final DomainInfo domainInfo) {
		return this.get(domainInfo) == null;
	}

	public DhParam get(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		final DhParamProps dhParamProps = new DhParamProps(info);
		/* @formatter:off */
		return this.fs.list(dhParamProps.getDomainKeySubDirPath()).stream()
				.filter(file -> dhParamProps.getDhParamName().equals(file.getFileName().toString()))
				.map(file -> new DhParam(file))
				.findAny()
				.orElse(null);
		/* @formatter:on */
	}

	/**
	 * Liefert das aktuelle DH-Param, wenn dies vorhanden ist. Sollte es nicht existieren, wird ein neues erstellt und zurueck gegeben.
	 *
	 * @param info {@link DomainInfo}
	 * @return {@link DhParam}
	 */
	public DhParam create(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		DhParam dhParam = this.get(info);
		if (dhParam == null) {
			final DhParamProps dhParamProps = new DhParamProps(info);
			final DhParam dh = new DhParam(this.fs.getPath(dhParamProps.getDomainKeySubDirPath().toString(), dhParamProps.getDhParamName()));
			final String message = this.msg.getMessage("key.process.create.dh.param", String.valueOf(dhParamProps.getBit()), info.getName());
			dhParam = this.openSsl.createDhParam(dh, message);
		}
		return dhParam;
	}

	@Getter
	private class DhParamProps {

		private final int bit;
		private final String dhParamName;
		private final Path domainKeySubDirPath;

		public DhParamProps(final DomainInfo info) {
			final Settings g = DhParamService.this.settingsService.getGlobal();
			final Settings d = DhParamService.this.settingsService.getDomain(info);
			final Settings k = d.getBool(SettingsKey.OVERWRITE_KEYS) ? d : g;

			this.bit = k.getInt(SettingsKey.DH_PARAM_BIT);
			this.dhParamName = String.format(DH_PARAM_TPL, this.bit);
			this.domainKeySubDirPath = DhParamService.this.fs.getPath(info.getDirectory().toString(), g.get(SettingsKey.DOMAIN_KEY_SUBDIR));
		}
	}
}
