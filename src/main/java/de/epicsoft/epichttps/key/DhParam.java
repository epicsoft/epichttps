/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.key;

import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.Assert;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Getter
@ToString
@EqualsAndHashCode
public class DhParam {

	private static final String BIT_REGEX = String.format(DhParamService.DH_PARAM_TPL, "(.*?[\\d])");
	private static final Pattern BIT_PATTERN = Pattern.compile(BIT_REGEX);

	@NonNull
	private final Path path;

	private Integer bit;

	public DhParam(final Path path) {
		Assert.notNull(path, "Path may be not null");

		this.path = path;

		final Matcher matcher = BIT_PATTERN.matcher(path.getFileName().toString());
		if (matcher.find()) {
			this.bit = Integer.valueOf(matcher.group(1));
		} else {
			throw new IllegalArgumentException(String.format("Path not contains valid DH-Param: ''", path.toString()));
		}
	}

	public String getName() {
		return this.path.getFileName().toString();
	}
}