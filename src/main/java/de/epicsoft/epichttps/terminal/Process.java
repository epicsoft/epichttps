/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.terminal;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.UUID;

import org.springframework.util.Assert;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Getter
@ToString
@EqualsAndHashCode
public class Process {

	private final UUID id;

	private final String command;

	private final String message;

	private final boolean silent;

	private ProcessStatus status;

	private String output;

	private ZonedDateTime startTime;

	private ZonedDateTime endTime;

	public Process(final String command, final String message) {
		this(command, message, false);
	}

	public Process(final String command, final String message, final Boolean silent) {
		Assert.hasText(command, "Command may be not empty");
		Assert.hasText(message, "Message may be not empty");
		Assert.notNull(silent, "Silent may be not null");

		this.id = UUID.randomUUID();
		this.command = command;
		this.message = message;
		this.silent = silent;
		this.status = ProcessStatus.PREPARED;
	}

	/* package */ void starting() {
		Assert.isTrue(this.status == ProcessStatus.PREPARED, "Process is not prepared");

		this.status = ProcessStatus.RUNNING;
		this.startTime = ZonedDateTime.now(ZoneOffset.UTC);
	}

	/* package */ void success(final String output) {
		Assert.isTrue(this.status == ProcessStatus.RUNNING, "Process is not started");

		this.status = ProcessStatus.SUCCESS;
		this.endTime = ZonedDateTime.now(ZoneOffset.UTC);
		this.output = output;
	}

	/* package */ void failed(final String output) {
		Assert.isTrue(this.status == ProcessStatus.RUNNING, "Process is not started");

		this.status = ProcessStatus.FAILED;
		this.endTime = ZonedDateTime.now(ZoneOffset.UTC);
		this.output = output;
	}
}
