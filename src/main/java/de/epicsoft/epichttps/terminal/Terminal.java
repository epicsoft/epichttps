/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.terminal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * {@link Terminal} ist die Schnittstelle zur Kommandozeile des Systems, dies ist erforderlich um Let's Encrypt, OpenSSL und weitere Anwendungen auszufuehren, da die erforderlichen
 * Funktionalitaeten von Java nicht unterstuetzt werden. <br>
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Service
public class Terminal {

	public static final String LINE_BREAK = "\n";

	@Autowired
	private ProcessManager processManager;

	/**
	 * Baut ein Befehl mit Parametern, fuegt dabei zwischen die Parameter immer ein Leerzeichen ein.
	 *
	 * @param commands
	 * @return {@link String}
	 */
	public String build(final String... commands) {
		final StringBuilder sb = new StringBuilder();
		for (final String command : commands) {
			sb.append(command).append(" ");
		}
		return sb.toString().trim();
	}

	/**
	 * Fuehrt einen Befehl auf der Kommandozeile aus. Vor dem Start wird ein Prozess gestartet und man erhaelt eine Prozess-ID, diese muss für jeden Log-Eintrag verwendet werden. Die
	 * Ausgabe des Befehls wird protokoliert und nach Abschluss des Prozesses im Log ausgegeben. Im Fehlerfall wird die Ausgabe des Befehls ebenfalls im Log ausgegeben.
	 *
	 * @param command {@link String} auszufuehrender Befehl
	 * @param message {@link String} wird an {@link ProcessManager} uebergeben
	 */
	public void execute(final Process process) {
		Assert.notNull(process, "Process may be not null");

		this.processManager.start(process);
		log.info("{} - terminal run command '{}'", process.getId(), process.getCommand());

		String output = null;
		try {
			log.info("{} - {} :: {}", process.getId(), process.getMessage(), process.getCommand());
			final java.lang.Process execCmd = Runtime.getRuntime().exec(process.getCommand());
			output = this.readStream(execCmd.getInputStream());
			execCmd.waitFor();

			final ProcessStatus status = execCmd.exitValue() == 0 ? ProcessStatus.SUCCESS : ProcessStatus.FAILED;
			log.info("{} - terminal exit with '{}' and output - {}", process.getId(), execCmd.exitValue(),
					status == ProcessStatus.SUCCESS ? output : this.readStream(execCmd.getErrorStream()));
			this.processManager.stop(process, status, output);
		} catch (final IOException | IllegalThreadStateException | InterruptedException e) {
			log.error(e.getMessage(), e);
			final String msg = StringUtils.hasText(output) ? output : e.getMessage();
			this.processManager.stop(process, ProcessStatus.FAILED, msg);
		}
	}

	private String readStream(final InputStream in) throws IOException {
		final BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String line;
		final StringBuilder lines = new StringBuilder();
		while ((line = reader.readLine()) != null) {
			lines.append(line).append(LINE_BREAK);
		}
		return lines.toString();
	}
}