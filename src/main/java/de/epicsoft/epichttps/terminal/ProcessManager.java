/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.terminal;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import de.epicsoft.epichttps.common.event.ProcessBeginEvent;
import de.epicsoft.epichttps.common.event.ProcessEndEvent;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Service
@RequiredArgsConstructor
public class ProcessManager {

	@NonNull
	private final ApplicationEventPublisher publisher;

	private final Set<UUID> runningProcesses = new HashSet<>();
	private final Set<Process> processes = new HashSet<>();

	public synchronized void start(final Process process) {
		Assert.notNull(process, "Process must be not null");

		process.starting();
		this.processes.add(process);
		this.runningProcesses.add(process.getId());
		this.publisher.publishEvent(new ProcessBeginEvent(process));
	}

	public synchronized void stop(final Process process, final ProcessStatus status, final String output) {
		Assert.notNull(process, "Process must be not null");
		Assert.notNull(status, "Process status must be not null");

		if (status == ProcessStatus.SUCCESS) {
			process.success(output);
		} else if (status == ProcessStatus.FAILED) {
			process.failed(output);
		} else {
			throw new IllegalArgumentException("Process status must be success or failed");
		}

		if (this.runningProcesses.contains(process.getId())) {
			this.runningProcesses.remove(process.getId());
			this.publisher.publishEvent(new ProcessEndEvent(process));
		} else {
			throw new IllegalArgumentException(String.format("Process-ID '%s' not found", process.getId()));
		}
	}

	public boolean isAnyRunning() {
		return !this.runningProcesses.isEmpty();
	}

	public boolean isRunning(final UUID processId) {
		return this.runningProcesses.contains(processId);
	}

	public Set<Process> getRunning() {
		return this.processes.stream().filter(process -> this.runningProcesses.contains(process.getId())).collect(Collectors.toSet());
	}
}
