/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.settings;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import de.epicsoft.epichttps.domain.DomainInfo;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@ToString
public class Settings extends Properties {

	private static final long serialVersionUID = -6488335192854486605L;

	private final static Pattern PLACEHOLDER = Pattern.compile("\\$\\{(.*?)\\}");
	private final static String DELIMITER = ",";

	private final Properties env;

	private Properties global;

	private Properties domain;

	private DomainInfo domainInfo;

	public Settings(final Properties env) {
		Assert.notNull(env, "Environment properties may be not null");
		this.env = env;
	}

	public Settings(final Properties env, final Properties global) {
		this(env);
		Assert.notNull(global, "Global properties may be not null");
		this.global = global;
	}

	public Settings(final Properties env, final Properties global, final Properties domain, final DomainInfo domainInfo) {
		this(env, global);
		Assert.notNull(domain, "Domain properties may be not null");
		this.domain = domain;
		this.domainInfo = domainInfo;
	}

	protected Properties getProperties() {
		if (this.isDomain()) {
			return this.domain;
		} else if (this.isGlobal()) {
			return this.global;
		}
		return this.env;
	}

	protected DomainInfo getDomainInfo() {
		return this.domainInfo;
	}

	@Override
	public String getProperty(final String key) {
		if (this.isDomain() && this.domain.containsKey(key)) {
			return this.replaceVariables(this.domain.getProperty(key));
		} else if (this.isGlobal() && this.global.containsKey(key)) {
			return this.replaceVariables(this.global.getProperty(key));
		}
		return this.replaceVariables(this.env.getProperty(key));
	}

	@Override
	public String getProperty(final String key, final String defaultValue) {
		if (this.isDomain() && this.domain.containsKey(key)) {
			return this.replaceVariables(this.domain.getProperty(key, defaultValue));
		} else if (this.isGlobal() && this.global.containsKey(key)) {
			return this.replaceVariables(this.global.getProperty(key, defaultValue));
		}
		return this.replaceVariables(this.env.getProperty(key, defaultValue));
	}

	@Override
	public synchronized Object setProperty(final String key, final String value) {
		if (this.isGlobal() || this.isDomain() || this instanceof KeySettings) {
			return this.getProperties().setProperty(key, value);
		}

		log.debug("setProperty([String key]{}, [String value]{})", key, value);
		log.debug("setProperty::isGlobal(): {} / global: {} / domain: {}", this.isGlobal(), this.global, this.domain);
		log.debug("setProperty::isDomain(): {} / global: {} / domain: {}", this.isDomain(), this.global, this.domain);
		throw new RuntimeException(String.format("Environments cannot be changed '%s:%s'", key, value));
	}

	public Boolean isEnv() {
		return this.global == null && this.domain == null;
	}

	public Boolean isGlobal() {
		return this.global != null && this.domain == null;
	}

	public Boolean isDomain() {
		return this.global != null && this.domain != null;
	}

	public String get(final SettingsKey key) {
		Assert.notNull(key, "Key may be not null");

		return this.getProperty(key.getName());
	}

	public Integer getInt(final SettingsKey key) {
		Assert.notNull(key, "Key may be not null");

		return Integer.valueOf(this.get(key));
	}

	public Boolean getBool(final SettingsKey key) {
		Assert.notNull(key, "Key may be not null");

		return Boolean.valueOf(this.get(key));
	}

	public Set<String> getSet(final SettingsKey key) {
		Assert.notNull(key, "Key may be not null");

		final String property = this.getProperty(key.getName());
		return StringUtils.hasText(property) ? new HashSet<>(Arrays.asList(property.split(DELIMITER))) : new HashSet<>();
	}

	/**
	 * @param key {@link SettingsKey}
	 * @param value {@link String}
	 * @return the previous value of the specified key in this property list, or {@code null} if it did not have one.
	 */
	public synchronized Object set(final SettingsKey key, final String value) {
		Assert.notNull(key, "Key may be not null");

		return this.setProperty(key.getName(), value != null ? value : "");
	}

	/**
	 * @param key {@link SettingsKey}
	 * @param value {@link Integer}
	 * @return the previous value of the specified key in this property list, or {@code null} if it did not have one.
	 */
	public synchronized Object set(final SettingsKey key, final Integer value) {
		Assert.notNull(key, "Key may be not null");

		return this.setProperty(key.getName(), value != null ? value.toString() : "");
	}

	/**
	 * @param key {@link SettingsKey}
	 * @param value {@link Boolean}
	 * @return the previous value of the specified key in this property list, or {@code null} if it did not have one.
	 */
	public synchronized Object set(final SettingsKey key, final Boolean value) {
		Assert.notNull(key, "Key may be not null");

		return this.setProperty(key.getName(), value != null ? value.toString() : "");
	}

	/**
	 * @param key {@link SettingsKey}
	 * @param value {@link Boolean}
	 * @return the previous value of the specified key in this property list, or {@code null} if it did not have one.
	 */
	public synchronized Object set(final SettingsKey key, final ZonedDateTime value) {
		Assert.notNull(key, "Key may be not null");

		return this.setProperty(key.getName(), value != null ? value.toString() : "");
	}

	/**
	 * @param key {@link SettingsKey}
	 * @param value {@link Set} with {@link String}
	 * @return the previous value of the specified key in this property list, or {@code null} if it did not have one.
	 */
	public synchronized Object set(final SettingsKey key, final Set<String> value) {
		Assert.notNull(key, "Key may be not null");

		return this.setProperty(key.getName(), value != null ? String.join(DELIMITER, value) : "");
	}

	/**
	 * Sucht im Wert nach einem oder mehrere Platzhalter und ersetzt diese
	 *
	 * @param value
	 * @return {@link String}
	 */
	private String replaceVariables(final String value) {
		String text = value;

		if (StringUtils.hasText(text)) {
			final Matcher matcher = PLACEHOLDER.matcher(text);
			while (matcher.find()) {
				final String placeholder = matcher.group(0);
				final String key = matcher.group(1);
				final String property = StringUtils.hasText(this.getProperty(key)) ? this.getProperty(key) : "";
				text = StringUtils.replace(text, placeholder, property);
			}
		}
		return text;
	}

}