/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.settings;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.domain.DomainInfo;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Service
@RequiredArgsConstructor
public class SettingsService {

  @NonNull
  private final AbstractEnvironment environment;

  @NonNull
  private final FileSystemAPI fs;

  @NonNull
  private final Environment env;

  /**
   * Liefert die System-Einstellungen. Die Werte duerfen nicht veraendert werden.
   *
   * @return {@link Settings}
   */
  public Settings getEnv() {
    return new Settings(this.getEnvProperties());
  }

  public Settings getGlobal() {
    return new Settings(this.getEnvProperties(), this.getGlobalProperties());
  }

  public Settings getDomain(final DomainInfo info) {
    Assert.notNull(info, "Domain Info may bo not null");

    return new Settings(this.getEnvProperties(), this.getGlobalProperties(), this.getDomainProperties(info), info);
  }

  public KeySettings getKeySettings(final Path keyProperty) {
    Assert.notNull(keyProperty, "Key-Property may be not null");
    Assert.isTrue(this.fs.isFile(keyProperty), "Key-Property may be exists");

    try {
      final Properties keyProps = this.fs.loadProperties(keyProperty);
      return new KeySettings(keyProps, keyProperty);
    } catch (final IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

  public void save(final Settings settings) {
    if (settings.isGlobal()) {
      this.saveGlobalSettings(settings);
    } else if (settings.isDomain()) {
      this.saveDomainSettings(settings);
    } else if (settings instanceof KeySettings) {
      this.saveKeySettings((KeySettings) settings);
    } else if (settings.isEnv()) {
      throw new IllegalArgumentException("Environments can not be saved");
    } else {
      throw new IllegalArgumentException("Unknow Settings Type");
    }
  }

  public void resetAuth(final Settings domainSettings) {
    Assert.notNull(domainSettings, "Domain Settings may be not null");
    Assert.isTrue(domainSettings.isDomain(), "Settings are not domain settings");

    final Set<SettingsKey> keys = new HashSet<>(
        Arrays.asList(SettingsKey.AUTH_DOMAIN, SettingsKey.AUTH_PORT, SettingsKey.AUTH_INCLUDE_FILENAME, SettingsKey.AUTH_IPV4, SettingsKey.AUTH_IPV6));
    this.reset(keys, domainSettings);
  }

  public void resetCert(final Settings domainSettings) {
    Assert.notNull(domainSettings, "Domain Settings may be not null");
    Assert.isTrue(domainSettings.isDomain(), "Settings are not domain settings");

    final Set<SettingsKey> keys = new HashSet<>(Arrays.asList(SettingsKey.CERT_RENEW_BEFORE_EXPIRY_DAYS, SettingsKey.PRIVATE_KEY_ROTATION_ENABLE, SettingsKey.PRIVATE_KEY_QUANTITY,
        SettingsKey.PRIVATE_KEY_PERIOD_VALIDITY_DAYS, SettingsKey.BACKUP_KEY_ENABLE, SettingsKey.BACKUP_KEY_QUANTITY, SettingsKey.DH_PARAM_BIT));
    this.reset(keys, domainSettings);
  }

  public void resetCsr(final Settings domainSettings) {
    Assert.notNull(domainSettings, "Domain Settings may be not null");
    Assert.isTrue(domainSettings.isDomain(), "Settings are not domain settings");

    final Set<SettingsKey> keys = new HashSet<>(Arrays.asList(SettingsKey.CSR_COUNTRY, SettingsKey.CSR_STATE, SettingsKey.CSR_LOCATION, SettingsKey.CSR_ZIPCODE,
        SettingsKey.CSR_STREET, SettingsKey.CSR_ORGANIZATION, SettingsKey.CSR_ORGANIZATION_UNIT, SettingsKey.CSR_EMAIL));
    this.reset(keys, domainSettings);
  }

  public void resetHstsHpkp(final Settings domainSettings) {
    Assert.notNull(domainSettings, "Domain Settings may be not null");
    Assert.isTrue(domainSettings.isDomain(), "Settings are not domain settings");

    final Set<SettingsKey> keys = new HashSet<>(Arrays.asList(SettingsKey.HSTS_ENABLE, SettingsKey.HSTS_MAX_AGE, SettingsKey.HSTS_INCLUDE_SUBDOMAINS, SettingsKey.HSTS_PRELOAD,
        SettingsKey.HPKP_ENABLE, SettingsKey.HPKP_MAX_AGE, SettingsKey.HPKP_INCLUDE_SUBDOMAINS, SettingsKey.HPKP_ADD_PINS, SettingsKey.HPKP_REPORT_ENABLE,
        SettingsKey.HPKP_REPORT_ENFORCE, SettingsKey.HPKP_REPORT_ONLY));
    this.reset(keys, domainSettings);
  }

  public void resetWebserver(final Settings domainSettings) {
    Assert.notNull(domainSettings, "Domain Settings may be not null");
    Assert.isTrue(domainSettings.isDomain(), "Settings are not domain settings");

    final Set<SettingsKey> keys = new HashSet<>(Arrays.asList(SettingsKey.NGINX_RESOLVER_IPV4, SettingsKey.NGINX_RESOLVER_IPV6, SettingsKey.NGINX_RESOLVER_TIMEOUT,
        SettingsKey.NGINX_SSL_SESSION_CACHE, SettingsKey.NGINX_SSL_SESSION_TIMEOUT, SettingsKey.NGINX_SSL_PROTOCOLS, SettingsKey.NGINX_SSL_CIPHERS,
        SettingsKey.NGINX_SSL_PREFER_SERVER_CIPHERS, SettingsKey.NGINX_SSL_STAPLING, SettingsKey.NGINX_SSL_STAPLING_VERIFY, SettingsKey.NGINX_SSL_ECDH_CURVE,
        SettingsKey.NGINX_HEADER_FRAME_OPTION, SettingsKey.NGINX_HEADER_CONTENT_TYPE_OPTIONS, SettingsKey.NGINX_HEADER_XSS_PROTECTION, SettingsKey.NGINX_RESOLVER_IPV4_ENABLE,
        SettingsKey.NGINX_RESOLVER_IPV6_ENABLE, SettingsKey.NGINX_RESOLVER_TIMEOUT_ENABLE, SettingsKey.NGINX_SSL_SESSION_CACHE_ENABLE, SettingsKey.NGINX_SSL_SESSION_TIMEOUT_ENABLE,
        SettingsKey.NGINX_SSL_PREFER_SERVER_CIPHERS_ENABLE, SettingsKey.NGINX_SSL_STAPLING_ENABLE, SettingsKey.NGINX_SSL_STAPLING_VERIFY_ENABLE,
        SettingsKey.NGINX_SSL_ECDH_CURVE_ENABLE, SettingsKey.NGINX_HEADER_FRAME_OPTION_ENABLE, SettingsKey.NGINX_HEADER_CONTENT_TYPE_OPTIONS_ENABLE,
        SettingsKey.NGINX_HEADER_XSS_PROTECTION_ENABLE));
    this.reset(keys, domainSettings);
  }

  private void reset(final Set<SettingsKey> keys, final Settings domainSettings) {
    final Settings global = this.getGlobal();
    keys.forEach(key -> domainSettings.set(key, global.get(key)));
    this.save(domainSettings);
  }

  private void saveGlobalSettings(final Settings settings) {
    final Settings env = this.getEnv();
    final Path globalConfigFile = this.fs.getPath(env.get(SettingsKey.LE_ROOT_DIR), env.get(SettingsKey.CUSTOM_SUBDIR), env.get(SettingsKey.LE_GENERAL_SUBDIR),
        env.get(SettingsKey.CONFIG_FILE_NAME));
    try {
      this.fs.saveProperties(settings.getProperties(), globalConfigFile);
    } catch (final IOException e) {
      throw new UncheckedIOException(String.format("Can not save global properties '%s'", globalConfigFile.toString()), e);
    }
  }

  private void saveDomainSettings(final Settings settings) {
    final Settings env = this.getEnv();
    final Path domainConfigFile = this.fs.getPath(settings.getDomainInfo().getDirectory().toString(), env.get(SettingsKey.CONFIG_FILE_NAME));
    try {
      this.fs.saveProperties(settings.getProperties(), domainConfigFile);
    } catch (final IOException e) {
      throw new UncheckedIOException(String.format("Can not save domain properties '%s'", settings.getDomainInfo().getName()), e);
    }
  }

  private void saveKeySettings(final KeySettings settings) {
    try {
      this.fs.saveProperties(settings.getProperties(), settings.getKeyProperty());
    } catch (final IOException e) {
      throw new UncheckedIOException(String.format("Can not save key-settings properties '%s'", settings.getKeyProperty().toString()), e);
    }
  }

  /**
   * Erstellt aus den {@link Environment} eine {@link Properties} Datei, dabei werden die Werte aus "application.properties" angezogen, bzw. ein Profil davon.
   *
   * @return {@link Properties}
   */
  private Properties getEnvProperties() {
    final Properties combinedEnv = new Properties();

    for (final PropertySource<?> propertySource : this.environment.getPropertySources()) {
      if (propertySource instanceof MapPropertySource) {
        combinedEnv.putAll(((MapPropertySource) propertySource).getSource());
      }
    }

    /* @formatter:off */
		combinedEnv.keySet().stream()
			.filter(key -> key instanceof String)
			.map(key -> (String) key)
			.filter(key -> StringUtils.hasText(key))
			.filter(key -> this.env.containsProperty(key))
			.forEach(key -> combinedEnv.put(key, this.env.getProperty(key)));
		/* @formatter:on */

    return combinedEnv;
  }

  private Properties getGlobalProperties() {
    final Settings env = this.getEnv();

    final Path generalDir = this.fs.getPath(env.get(SettingsKey.LE_ROOT_DIR), env.get(SettingsKey.CUSTOM_SUBDIR), env.get(SettingsKey.LE_GENERAL_SUBDIR));
    this.fs.createDirectory(generalDir);

    final Path configFile = this.fs.getPath(generalDir.toString(), env.get(SettingsKey.CONFIG_FILE_NAME));
    try {
      return this.fs.loadProperties(configFile);
    } catch (final IOException e) {
      try {
        this.fs.saveProperties(new Properties(), configFile);
        return this.getGlobalProperties();
      } catch (final IOException e1) {
        throw new UncheckedIOException(String.format("Can not load global properties from file '%s'", configFile.toString()), e);
      }
    }
  }

  private Properties getDomainProperties(final DomainInfo domainInfo) {
    final Settings env = this.getEnv();
    final Path configFile = this.fs.getPath(domainInfo.getDirectory().toString(), env.get(SettingsKey.CONFIG_FILE_NAME));
    try {
      return this.fs.loadProperties(configFile);
    } catch (final IOException e) {
      try {
        this.fs.saveProperties(new Properties(), configFile);
        return this.getDomainProperties(domainInfo);
      } catch (final IOException e1) {
        throw new UncheckedIOException(String.format("Can not load domain properties '%s' from file '%s'", domainInfo.getName(), configFile.toString()), e);
      }
    }
  }
}