/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.settings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.Validator;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.ui.common.EpicValidationResult;

/**
 * Diese Klasse ist fuer Konsistenzpruefungen vorgesehen. Damit sollen uebergreifende Einstellungen geprueft werden, ob diese im Konflikt stehen bzw. zusammen fehlerhaftes
 * Verhalten hervorrufen koennen. <br>
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Service
public class ConsistencyValidation {

	@Autowired
	private EpicMessageSource msg;

	/**
	 * Erstellt einen Validator fuer {@link BeanValidationBinder}
	 *
	 * @return {@link Validator}
	 */
	public Validator<Settings> buildValidator() {
		return (value, context) -> {
			try {
				this.validate(value);
			} catch (final IllegalArgumentException e) {
				return EpicValidationResult.error(e.getMessage());
			}
			return EpicValidationResult.ok();
		};
	}

	/**
	 * Fuehrt alle Konsistenzpruefungen durch, beiim ersten Fehler wird eine {@link IllegalArgumentException} geworfen.
	 *
	 * @param settings {@link Settings}
	 * @throws IllegalArgumentException
	 */
	private void validate(final Settings settings) {
		Assert.notNull(settings, "Settings may be not null");

		this.validateHstsAndPrivateKeyRotationTolerance(settings);
	}

	/**
	 * Bei aktiviertem HPKP und privater Schluessel Rotation werden die Werte ueberprueft. So dass die HPKP Dauer die maximale Dauer der Schluessel abzueglich der Toleranz nicht
	 * ueberschreitet.
	 *
	 * @param settings {@link Settings}
	 * @throws IllegalArgumentException
	 */
	private void validateHstsAndPrivateKeyRotationTolerance(final Settings settings) {
		final Boolean hpkpEnable = settings.getBool(SettingsKey.HPKP_ENABLE);
		final Boolean privateKeyRotationEnable = settings.getBool(SettingsKey.PRIVATE_KEY_ROTATION_ENABLE);
		final Boolean backupKeyEnable = settings.getBool(SettingsKey.BACKUP_KEY_ENABLE);

		if (hpkpEnable) {
			if (privateKeyRotationEnable) {
				final Integer hpkpMaxAge = settings.getInt(SettingsKey.HPKP_MAX_AGE);
				final Integer privateKeyPeriodValidityDays = settings.getInt(SettingsKey.PRIVATE_KEY_PERIOD_VALIDITY_DAYS);
				final Integer privateKeyQuantity = settings.getInt(SettingsKey.PRIVATE_KEY_QUANTITY);
				final Integer privateKeyToleranceDays = settings.getInt(SettingsKey.PRIVATE_KEY_TOLERANCE_DAYS);

				final Integer keyMaxAge = (privateKeyPeriodValidityDays * privateKeyQuantity - privateKeyToleranceDays * privateKeyQuantity) * 60 * 60 * 24;
				if (keyMaxAge < hpkpMaxAge) {
					throw new IllegalArgumentException(this.msg.getMessage("validator.hpkp.and.private.key.max.age", keyMaxAge.toString()));
				}

				if (backupKeyEnable) {
					final Integer backupKeyQuantity = settings.getInt(SettingsKey.BACKUP_KEY_QUANTITY);
					if (backupKeyQuantity < privateKeyQuantity) {
						throw new IllegalArgumentException(this.msg.getMessage("validator.hpkp.and.backup.key.quantity"));
					}
				} else {
					throw new IllegalArgumentException(this.msg.getMessage("validator.hpkp.and.backup.key"));
				}
			} else {
				throw new IllegalArgumentException(this.msg.getMessage("validator.hpkp.and.private.key.rotation"));
			}
		}
	}
}
