/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.settings;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Getter
@AllArgsConstructor
public enum SettingsKey {
	/* @formatter:off */

	TEST("TestKey"), // NUR ZUM TESTEN / ONLY FOR TEST

	// epicHTTPS
	CONFIG_FILE_NAME("epicsoft.config.file.name"), // Name der Konfigurationsdatei
	CUSTOM_SUBDIR("epicsoft.custom.subdir"), // Benutzerdefiniertes Verzeichnis fuer epicHTTPS / zB "custom"
	HOST_PATH("epichttps.hostpath"), // Pfad zu /etc/letsencrypt/custom auf dem Host
	EPICHTTPS_NAME("epichttps.name"), // Name der Software
	EPICHTTPS_DOMAIN("epichttps.domain"), // Domain unter der epicHTTP erreichbar ist
	EPICHTTPS_PROXY_PATH("epichttps.proxy.pass"), // Pfad unter dem epicHTTPS erreichbar ist

	// Domain
	DOMAIN_KEY_SUBDIR("epicsoft.domain.key.subdir"), // Unterverzeichnis von einer Domain fuer Schluessel
	DOMAIN_CERT_SUBDIR("epicsoft.domain.cert.subdir"), // Unterverzeichnis von einer Domain fuer Zertifikate

	// Let's Encrypt
	LE_ACCOUNT_EMAIL("epicsoft.le.account.email"), // Let's Entrypt register Email-Adresse
	LE_ACCOUNT_WITHOUT_EMAIL("epicsoft.le.account.without.email"), // Let's Entrypt ohne Email-Adresse erstellt
	LE_ACCOUNT_AGREE_TOS("epicsoft.le.account.agree.tos"), // Let's Entrypt Nutzungsbedingungen akzeptiert
	LE_ACCOUNT_DIR("epicsoft.le.account.dir"), // Let's Encrypt Konten-Verzeichnis
	LE_ROOT_DIR("epicsoft.le.root.dir"), // "etc" Verzeichnis von LetsEncrypt / zB /etc/letsencrypt
	LE_GENERAL_SUBDIR("epicsoft.le.global.subdir"), // Benutzerdefiniertes globales Unterverzeichnis von LetsEncrypt / zB "global"
	LE_EXCECUTION_FILE("epicsoft.le.execution.file"), // Pfad zur LE Ausfuehrungsdatei
	LE_TOS_LINK("epicsoft.le.tos.link"), // Link zu Nutzungsbedingungen von Let's Encrypt
	LE_DRY_MODE("epicsoft.le.dry.mode"), // Let's Entrypt Testmodus

	// Certificate Singning Request (CSR)
	CSR_COUNTRY("epicsoft.csr.country"),
	CSR_STATE("epicsoft.csr.state"),
	CSR_LOCATION("epicsoft.csr.location"),
	CSR_ZIPCODE("epicsoft.csr.zipcode"),
	CSR_STREET("epicsoft.csr.street"),
	CSR_ORGANIZATION("epicsoft.csr.organization"),
	CSR_ORGANIZATION_UNIT("epicsoft.csr.organizational.unit"),
	CSR_EMAIL("epicsoft.csr.email"),

  // Authentifizierung
	AUTH_INSTALLED_FORCE_SUCCESS("epicsoft.auth.installed.force.success"),
	AUTH_DOMAIN("epicsoft.auth.domain"),
	AUTH_PORT("epicsoft.auth.port"),
	AUTH_IPV4("epicsoft.auth.ipv4"),
	AUTH_IPV6("epicsoft.auth.ipv6"),
	AUTH_INCLUDE_FILENAME("epicsoft.auth.include.filename"),
	AUTH_CONFIGURATION_FILENAME("epicsoft.auth.configuration.filename"),

	// HSTS
	HSTS_ENABLE("epicsoft.hsts.enable"),
	HSTS_MAX_AGE("epicsoft.hsts.max.age"),
	HSTS_INCLUDE_SUBDOMAINS("epicsoft.hsts.include.subdomains"),
	HSTS_PRELOAD("epicsoft.hsts.preload"),

	// HPKP
	HPKP_ENABLE("epicsoft.hpkp.enable"),
	HPKP_MAX_AGE("epicsoft.hpkp.max.age"),
	HPKP_INCLUDE_SUBDOMAINS("epicsoft.hpkp.include.subdomains"),
	HPKP_ADD_PINS("epicsoft.hpkp.add.pins"),
	HPKP_REPORT_ENABLE("epicsoft.hpkp.report.enable"),
	HPKP_REPORT_ENFORCE("epicsoft.hpkp.report.enforce"),
	HPKP_REPORT_ONLY("epicsoft.hpkp.report.only"),

	// Nginx
	NGINX_TLS_INCLUDE_FILENAME("epicsoft.nginx.tls.include.filename"),
	NGINX_SERVER_TOKENS("epicsoft.nginx.server.tokens"),
	NGINX_RESOLVER_IPV4("epicsoft.nginx.resolver.ipv4"),
	NGINX_RESOLVER_IPV6("epicsoft.nginx.resolver.ipv6"),
	NGINX_RESOLVER_TIMEOUT("epicsoft.nginx.resolver.timeout"),
	NGINX_SSL_SESSION_CACHE("epicsoft.nginx.ssl.session.cache"),
	NGINX_SSL_SESSION_TIMEOUT("epicsoft.nginx.ssl.session.timeout"),
	NGINX_SSL_SESSION_TICKETS("epicsoft.nginx.ssl.session.tickets"),
	NGINX_SSL_PROTOCOLS("epicsoft.nginx.ssl.protocols"),
	NGINX_SSL_CIPHERS("epicsoft.nginx.ssl.ciphers"),
	NGINX_SSL_PREFER_SERVER_CIPHERS("epicsoft.nginx.ssl.prefer.server.ciphers"),
	NGINX_SSL_STAPLING("epicsoft.nginx.ssl.stapling"),
	NGINX_SSL_STAPLING_VERIFY("epicsoft.nginx.ssl.stapling.verify"),
	NGINX_SSL_ECDH_CURVE("epicsoft.nginx.ssl.ecdh.curve"),
	NGINX_HEADER_FRAME_OPTION("epicsoft.nginx.header.frame.option"),
	NGINX_HEADER_CONTENT_TYPE_OPTIONS("epicsoft.nginx.header.content.type.options"),
	NGINX_HEADER_XSS_PROTECTION("epicsoft.nginx.header.xss.protection"),

	// Nginx - Schalter
	NGINX_SERVER_TOKENS_ENABLE("epicsoft.nginx.server.tokens.enable"),
	NGINX_RESOLVER_IPV4_ENABLE("epicsoft.nginx.resolver.ipv4.enable"),
	NGINX_RESOLVER_IPV6_ENABLE("epicsoft.nginx.resolver.ipv6.enable"),
	NGINX_RESOLVER_TIMEOUT_ENABLE("epicsoft.nginx.resolver.timeout.enable"),
	NGINX_SSL_TRUSTED_CERTIFICATE_ENABLE("epicsoft.nginx.ssl.trusted.certificate.enable"),
	NGINX_SSL_SESSION_CACHE_ENABLE("epicsoft.nginx.ssl.session.cache.enable"),
	NGINX_SSL_SESSION_TIMEOUT_ENABLE("epicsoft.nginx.ssl.session.timeout.enable"),
	NGINX_SSL_SESSION_TICKETS_ENABLE("epicsoft.nginx.ssl.session.tickets.enable"),
	NGINX_SSL_PREFER_SERVER_CIPHERS_ENABLE("epicsoft.nginx.ssl.prefer.server.ciphers.enable"),
	NGINX_SSL_STAPLING_ENABLE("epicsoft.nginx.ssl.stapling.enable"),
	NGINX_SSL_STAPLING_VERIFY_ENABLE("epicsoft.nginx.ssl.stapling.verify.enable"),
	NGINX_SSL_ECDH_CURVE_ENABLE("epicsoft.nginx.ssl.ecdh.curve.enable"),
	NGINX_SSL_DH_PARAM_ENABLE("epicsoft.nginx.ssl.dh.param.enable"),
	NGINX_HEADER_FRAME_OPTION_ENABLE("epicsoft.nginx.header.frame.option.enable"),
	NGINX_HEADER_CONTENT_TYPE_OPTIONS_ENABLE("epicsoft.nginx.header.content.type.options.enable"),
	NGINX_HEADER_XSS_PROTECTION_ENABLE("epicsoft.nginx.header.xss.protection.enable"),

	// Schluessel und Zertifikate
	CERT_RENEW_BEFORE_EXPIRY_DAYS("epicsoft.cert.renew.before.expiry.days"),
	PRIVATE_KEY_ROTATION_ENABLE("epicsoft.private.key.rotation.enable"),
	PRIVATE_KEY_QUANTITY("epicsoft.private.key.quantity"),
	PRIVATE_KEY_PERIOD_VALIDITY_DAYS("epicsoft.private.key.period.validity.days"),
	PRIVATE_KEY_TOLERANCE_DAYS("epicsoft.private.key.tolerance.days"),
	BACKUP_KEY_ENABLE("epicsoft.backup.key.enable"),
	BACKUP_KEY_QUANTITY("epicsoft.backup.key.quantity"),
	DH_PARAM_BIT("epicsoft.dh.param.bit"),

	//Schluessel und Zertifikate - Validierung
	KEYS_QUANTITY_MIN("epicsoft.keys.quantity.min"),
	KEYS_QUANTITY_MAX("epicsoft.keys.quantity.max"),
	KEYS_PERIOD_DAYS_MIN("epicsoft.keys.period.days.min"),
	KEYS_PERIOD_DAYS_MAX("epicsoft.keys.period.days.max"),
	KEYS_ALGORITHM("epicsoft.keys.algorithm"),
	KEYS_SIZE("epicsoft.keys.size"),
	CERT_RENEW_DAYS_MIN("epicsoft.cert.renew.days.min"),
	CERT_RENEW_DAYS_MAX("epicsoft.cert.renew.days.max"),
	DH_PARAM_BIT_MIN("epicsoft.dh.param.bit.min"),
	MESSAGE_DIGEST_ALGORITHM("epicsoft.message.digest.algorithm"),
	CERT_RSA_KEY_SIZE("epicsoft.cert.rsa.key.size"),

	// KeySettings
	KEY_TYPE("key.type"),
	KEY_FILENAME("key.filename"),
	KEY_EXPIRE("key.expire"),
	KEY_EXPIRE_DATE("key.expire.date"),
	KEY_CREATE_DATE("key.create.date"),
	KEY_PIN("key.pin"),

	// Override
	OVERWRITE_CSR("epicsoft.overwrite.csr"),
	OVERWRITE_HSTS_HPKP("epicsoft.overwrite.hsts.hpkp"),
	OVERWRITE_WEBSERVER("epicsoft.overwrite.webserver"),
	OVERWRITE_KEYS("epicsoft.overwrite.keys"),

  // Domain (DomainSettings)
	DOMAIN_ACTIVATED("epicsoft.domain.activated"),
	DOMAIN_IPV4("epicsoft.domain.ipv4"),
	DOMAIN_IPV6("epicsoft.domain.ipv6"),
	DOMAIN_HTTP_PORT("epicsoft.domain.http.port"),
	DOMAIN_HTTPS_PORT("epicsoft.domain.https.port"),
	DOMAIN_ALTERNATIVE_NAMES("epicsoft.domain.alternative.names"),
	DOMAIN_ALTERNATIVE_NAMES_MANDATORY_SUBDOMAIN("epicsoft.domain.alternative.names.mandatory.subdomain"),

	// Spring
	SPRING_PROFILES_ACTIVE("spring.profiles.active");
  /* @formatter:on */

	private final String name;
}
