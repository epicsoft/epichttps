/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.settings;

import java.nio.file.Path;
import java.util.Properties;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class KeySettings extends Settings {

	private static final long serialVersionUID = 8911293103001540437L;

	@Getter
	private final Path keyProperty;

	public KeySettings(final Properties props, final Path keyProperty) {
		super(props);
		this.keyProperty = keyProperty;
	}

	@Override
	public Boolean isEnv() {
		return false;
	}

	@Override
	public Boolean isGlobal() {
		return false;
	}

	@Override
	public Boolean isDomain() {
		return false;
	}
}
