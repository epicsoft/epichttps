/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import de.epicsoft.epichttps.domain.Domain;
import de.epicsoft.epichttps.domain.DomainInfoService;
import de.epicsoft.epichttps.domain.DomainService;
import de.epicsoft.epichttps.facade.BuildFacade;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Service
@RequiredArgsConstructor
public class CronJobService {

	@NonNull
	private final DomainInfoService domainInfoService;
	@NonNull
	private final DomainService domainService;
	@NonNull
	private final BuildFacade buildFacade;

	@Scheduled(cron = "${epicsoft.scheduler.build}")
	public void build() {
		/* @formatter:off */
		this.domainInfoService.getAll().stream()
			.map(info -> this.domainService.get(info))
			.filter(Domain::isActive)
			.filter(domain -> this.buildFacade.isBuildNeeded(domain.getInfo()).isBuildNeeded())
			.forEach(domain -> this.buildFacade.build(domain.getInfo()));
		/* @formatter:on */
	}
}