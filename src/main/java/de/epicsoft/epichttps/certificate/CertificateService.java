/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.certificate;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.google.common.collect.ImmutableSet;

import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.common.ImmutableSetCollector;
import de.epicsoft.epichttps.common.OpenSslAPI;
import de.epicsoft.epichttps.domain.Domain;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.key.Key;
import de.epicsoft.epichttps.key.KeyService;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CertificateService {

	private static final String CERT_SUFFIX = "fullchain.pem";
	private static final int SUBALTNAME_DNSNAME = 2;

	@NonNull
	private final FileSystemAPI fs;
	@NonNull
	private final SettingsService settingsService;
	@NonNull
	private final LetsEncryptAPI le;
	@NonNull
	private final OpenSslAPI openSsl;
	@NonNull
	private final KeyService keyService;

	public ImmutableSet<Certificate> getAllValid(final DomainInfo info) {
		Assert.notNull(info, "Domain Info may be not null");

		return this.getAll(info).stream().filter(cert -> cert.isValid()).collect(new ImmutableSetCollector<>());
	}

	public ImmutableSet<Certificate> getAll(final DomainInfo info) {
		Assert.notNull(info, "Domain Info may be not null");

		final CertificateProps certificateProps = new CertificateProps(info);
		/* @formatter:off */
		return this.fs.list(certificateProps.getCertSubDirPath()).stream()
				.filter(file ->  file.toString().endsWith(CERT_SUFFIX))
				.map(this::buildCertificate)
				.collect(new ImmutableSetCollector<>());
		/* @formatter:on */
	}

	/**
	 * Liefert ein {@link Set} mit gueltigen Zertifikaten zu der uebergebenen {@link DomainInfo}. Liefert immer ein Object zurueck, auch wenn es ein leeres {@link Set} ist.
	 *
	 * @param info {@link DomainInfo}
	 * @return {@link Set} mit {@link Certificate}
	 */
	public Set<Certificate> getValid(final DomainInfo info) {
		Assert.notNull(info, "Domain Info may be not null");

		/* @formatter:off */
		return this.getAll(info).stream()
				.filter(Certificate::isValid)
				.sorted((c1, c2) -> c1.getValidFrom().compareTo(c2.getValidFrom()))
				.collect(Collectors.toSet());
		/* @formatter:on */
	}

	public Certificate getCurrent(final DomainInfo info) {
		Assert.notNull(info, "Domain Info may be not null");

		/* @formatter:off */
		return this.getValid(info).stream()
				.sorted((c1, c2) -> c2.getValidFrom().compareTo(c1.getValidFrom()))
				.findFirst()
				.orElse(null);
		/* @formatter:on */
	}

	public void createOrUpdate(final Domain domain, final Path authDir) {
		Assert.notNull(domain, "Domain may be not null");
		Assert.notNull(domain.getCurrentPrivateKey(), "Private Key may be not null");
		Assert.notNull(domain.getCsr(), "CSR may be not null");
		Assert.notNull(authDir, "Authentication directory may be not null");

		log.debug("Create or Update Certificate for '{}'", domain.getInfo().getName());

		if (this.isBuildNeeded(domain.getInfo())) {
			final CertificateProps certificateProps = new CertificateProps(domain.getInfo());
			this.le.createCertificate(domain.getInfo(), certificateProps, domain.getCurrentPrivateKey(), domain.getCsr(), authDir);
		} else {
			log.debug("Certificate update not mandatory for '{}' current certificate is up-to-date", domain.getInfo().getName());
		}
	}

	/**
	 * @see CertificateService#isBuildNeeded(DomainInfo, Certificate)
	 * @param info {@link DomainInfo}
	 * @return {@link Boolean}
	 */
	public Boolean isBuildNeeded(final DomainInfo info) {
		Assert.notNull(info, "Domain Info may be not null");

		return this.isBuildNeeded(info, this.getCurrent(info));
	}

	/**
	 * Pruefung vom Zertifikat: <br>
	 * 1. Zertifikat ist entweder abgelaufen oder im Zeitraum, wo es aktualisiert werden soll. <br>
	 * 2. Subdomains wurden entfernt oder hinzugefuegt
	 *
	 * @param info {@link DomainInfo}
	 * @param current {@link Certificate}
	 * @return {@link Boolean}
	 */
	public Boolean isBuildNeeded(final DomainInfo info, final Certificate current) {
		Assert.notNull(info, "Domain Info may be not null");

		if (current != null && current.isValid()) {
			final CertificateProps certificateProps = new CertificateProps(info);
			final Boolean expired = ZonedDateTime.now().plusDays(certificateProps.getValidityDays()).compareTo(current.getValidUntil()) >= 0;
			final Boolean subDomainChanged = !certificateProps.getAlternativeNames().equals(current.getAlternativeNames());
			final Boolean privateKeyNotExists = !this.existsPrivateKeyForCertificate(current, this.keyService.getValidPrivateKeys(info));

			return expired || subDomainChanged || privateKeyNotExists;
		}
		return true;
	}

	private Boolean existsPrivateKeyForCertificate(final Certificate certificate, final Set<Key> privateKeys) {
		/* @formatter:off */
		final String certificatePin = certificate.getPin();
		return privateKeys.stream()
				.filter(key -> key.getPin().equals(certificatePin))
				.findFirst()
				.isPresent();
		/* @formatter:on */
	}

	private Certificate buildCertificate(final Path file) {
		final X509Certificate x509 = this.buildX509CertificateFromFile(file);
		/* @formatter:off */
		return Certificate.builder()
				.file(file)
				.name(x509.getSubjectDN().getName())
				.validFrom(ZonedDateTime.ofInstant(x509.getNotBefore().toInstant(), ZoneOffset.UTC))
				.validUntil(ZonedDateTime.ofInstant(x509.getNotAfter().toInstant(), ZoneOffset.UTC))
				.issuer(x509.getIssuerDN().getName())
				.keyAlgorithm(x509.getPublicKey().getAlgorithm())
				.signatureAlgorithm(x509.getSigAlgName())
				.alternativeNames(this.getAlternativeNames(x509))
				.pin(this.openSsl.createPinForPublicKey(x509.getPublicKey()))
				.build();
		/* @formatter:on */
	}

	private X509Certificate buildX509CertificateFromFile(final Path file) {
		try (final InputStream is = Files.newInputStream(file)) {
			final CertificateFactory fact = CertificateFactory.getInstance("X.509");
			return (X509Certificate) fact.generateCertificate(is);
		} catch (IOException | CertificateException e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	private ImmutableSet<String> getAlternativeNames(final X509Certificate x509) {
		try {
			/* @formatter:off */
			return x509.getSubjectAlternativeNames().stream()
				.filter(entry -> ((Integer) entry.get(0)).intValue() == SUBALTNAME_DNSNAME)
				.map(entry -> (String) entry.get(1))
				.collect(new ImmutableSetCollector<>());
			/* @formatter:on */
		} catch (final CertificateParsingException e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	@Getter
	/* package */ class CertificateProps {

		private final Path certSubDirPath;
		private final int validityDays;
		private final Set<String> alternativeNames;
		private final int rsaKeySize;
		private final boolean leDryMode;

		public CertificateProps(final DomainInfo info) {
			final Settings g = CertificateService.this.settingsService.getGlobal();
			final Settings d = CertificateService.this.settingsService.getDomain(info);

			this.certSubDirPath = CertificateService.this.fs.getPath(info.getDirectory().toString(), g.get(SettingsKey.DOMAIN_CERT_SUBDIR));
			this.validityDays = d.getInt(SettingsKey.CERT_RENEW_BEFORE_EXPIRY_DAYS);
			this.alternativeNames = d.getSet(SettingsKey.DOMAIN_ALTERNATIVE_NAMES);
			this.rsaKeySize = d.getInt(SettingsKey.CERT_RSA_KEY_SIZE);
			this.leDryMode = g.getBool(SettingsKey.LE_DRY_MODE);
		}
	}
}