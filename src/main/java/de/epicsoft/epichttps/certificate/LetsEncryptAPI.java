/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.certificate;

import java.nio.file.Path;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import de.epicsoft.epichttps.certificate.CertificateService.CertificateProps;
import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.key.Key;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import de.epicsoft.epichttps.terminal.Process;
import de.epicsoft.epichttps.terminal.Terminal;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class LetsEncryptAPI {

	private static final String DRY_RUN = "--dry-run";

	private static final String CERT_TPL = "%d_cert.pem";
	private static final String CHAIN_TPL = "%d_chain.pem";
	private static final String FULLCHAIN_TPL = "%d_fullchain.pem";

	@NonNull
	private final Terminal terminal;
	@NonNull
	private final SettingsService settingsService;
	@NonNull
	private final EpicMessageSource msg;
	@NonNull
	private final FileSystemAPI fs;

	/**
	 * Erstellt ein neues Let's Encrypt Konto im Standard-Verzeichnis '/etc/letsencrypt/accounts', verwendet dabei die uebergebene Email-Adresse.
	 *
	 * @param email
	 * @param agreeTos
	 */
	public void register(final String email, final Boolean agreeTos) {
		Assert.hasText(email, "Email-Address must be not empty");
		Assert.isTrue(EmailValidator.getInstance().isValid(email), "Email-Address must be valid");
		Assert.isTrue(agreeTos, "Let's Encrypt terms of service not accepted");

		final String cmd = this.terminal.build(this.getExecutionFile().toString(), "register --email", email, "--agree-tos --text");
		this.terminal.execute(new Process(cmd, this.msg.getMessage("le.account.process.create.email", email)));
		log.info("Register LE Account with '{}' Email");
	}

	/**
	 * Erstellt ein neues anonymes Let's Encrypt Konto im Standard-Verzeichnis '/etc/letsencrypt/accounts'.
	 *
	 * @param agreeTos
	 */
	public void registerWithoutEmail(final Boolean agreeTos) {
		Assert.isTrue(agreeTos, "Let's Encrypt terms of service not accepted");

		final String cmd = this.terminal.build(this.getExecutionFile().toString(), "register --register-unsafely-without-email --agree-tos --text");
		this.terminal.execute(new Process(cmd, this.msg.getMessage("le.account.process.create.without.email")));
		log.info("Register LE Accoutn without Email");
	}

	public void createCertificate(final DomainInfo info, final CertificateProps certificateProps, final Key privateKey, final Csr csr, final Path authDir) {
		Assert.notNull(info, "Domain Info must be not null");
		Assert.notNull(certificateProps, "Certificate Properties must be not null");
		Assert.notNull(privateKey, "Private Key must be not null");
		Assert.notNull(privateKey.getKeyFile(), "Private Key File must be not null");
		Assert.notNull(csr, "CSR must be not null");
		Assert.notNull(authDir, "Auth directory must be not null");

		final long now = ZonedDateTime.now(ZoneOffset.UTC).toEpochSecond();
		final Path cert = this.fs.getPath(certificateProps.getCertSubDirPath().toString(), String.format(CERT_TPL, now));
		final Path chain = this.fs.getPath(certificateProps.getCertSubDirPath().toString(), String.format(CHAIN_TPL, now));
		final Path fullChain = this.fs.getPath(certificateProps.getCertSubDirPath().toString(), String.format(FULLCHAIN_TPL, now));

		String cmd = this.terminal.build(this.getExecutionFile().toString());
		cmd = this.terminal.build(cmd, "certonly");
		cmd = this.terminal.build(cmd, "--rsa-key-size", String.valueOf(certificateProps.getRsaKeySize()));
		cmd = this.terminal.build(cmd, "--text");
		// cmd = this.terminal.build(cmd, "--key-path", privateKey.getKeyFile().toString());
		cmd = this.terminal.build(cmd, "--csr", csr.getCsrFile().toString());
		cmd = this.terminal.build(cmd, "--cert-path", cert.toString());
		cmd = this.terminal.build(cmd, "--chain-path", chain.toString());
		cmd = this.terminal.build(cmd, "--fullchain-path", fullChain.toString());
		cmd = this.terminal.build(cmd, "--webroot --webroot-path", authDir.toString());
		if (certificateProps.isLeDryMode()) {
			cmd = this.terminal.build(cmd, DRY_RUN);
		}

		final String message = this.msg.getMessage("le.process.cert.create", info.getName());
		this.terminal.execute(new Process(cmd, message));
	}

	private Path getExecutionFile() {
		final Settings globalSettings = this.settingsService.getGlobal();
		return this.fs.getPath(globalSettings.get(SettingsKey.LE_EXCECUTION_FILE));
	}
}