/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.certificate;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.google.common.collect.ImmutableSet;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.common.OpenSslAPI;
import de.epicsoft.epichttps.common.TemplateService;
import de.epicsoft.epichttps.common.exception.EpicIllegalArgumentException;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.key.Key;
import de.epicsoft.epichttps.key.KeyService;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CsrService {

	private static final String CSR_CONFIG_TPL = "request_cnf";
	public static final String CSR_CONFIG_FILENAME = "request.cnf";
	public static final String CSR_FILENAME = "request.csr";

	@NonNull
	private final SettingsService settingsService;
	@NonNull
	private final TemplateService templateService;
	@NonNull
	private final FileSystemAPI fs;
	@NonNull
	private final EpicMessageSource msg;
	@NonNull
	private final OpenSslAPI openSsl;
	@NonNull
	private final KeyService keyService;

	public Csr get(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		if (!this.isBuildNeeded(info)) {
			final CsrProps csrProps = new CsrProps(info);
			return Csr.builder().csrFile(csrProps.getCsrFile()).csrConfigFile(csrProps.getCsrConfigFile()).build();
		}
		return null;
	}

	public Boolean isBuildNeeded(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		final CsrProps csrProps = new CsrProps(info);
		final String requestTpl = this.buildCsrConfigTemplate(info);
		return !(this.fs.isFile(csrProps.getCsrFile()) && this.fs.isFile(csrProps.getCsrConfigFile()) && this.fs.getContent(csrProps.getCsrConfigFile()).equals(requestTpl));
	}

	public Csr createOrUpdate(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		final CsrProps csrProps = new CsrProps(info);
		final Path csrConfigFile = csrProps.getCsrConfigFile();
		final String requestTpl = this.buildCsrConfigTemplate(info);

		if (this.fs.saveFileIfChanged(csrConfigFile, requestTpl)) {
			log.info("CSR config file updated '{}'", csrConfigFile.toString());
		} else {
			log.debug("CSR config file is already up to date '{}'", csrConfigFile.toString());
		}

		final Path csrFile = this.createOrUpdateCsr(info, this.keyService.getCurrentPrivateKey(info));
		return Csr.builder().csrFile(csrFile).csrConfigFile(csrConfigFile).build();
	}

	public String buildCsrConfigTemplate(final DomainInfo info) {
		Assert.notNull(info, "Domain Info must be not null");

		final CsrProps csrProps = new CsrProps(info);
		final Map<String, Object> data = new HashMap<>();
		data.put("keysize", csrProps.getKeySize());
		data.put("domain", info.getName());
		data.put("country", csrProps.getCountry());
		data.put("state", csrProps.getState());
		data.put("location", csrProps.getLocation());
		data.put("zipcode", csrProps.getZipCode());
		data.put("street", csrProps.getStreet());
		data.put("organization", csrProps.getOrganization());
		data.put("organizationunit", csrProps.getOrganizationUnit());
		data.put("email", csrProps.getEmail());
		data.put("selfsigned", SelfSignedCertificateService.DOMAIN_INFO_ID.equals(info.getId()));

		int counter = 0;
		final Set<String> subDomainsData = new LinkedHashSet<>();
		for (final String subDomain : csrProps.getAlternativeNames()) {
			subDomainsData.add(String.format("DNS.%d = %s", ++counter, subDomain));
		}
		data.put("subdomains", subDomainsData);

		return this.templateService.render(CSR_CONFIG_TPL, data);
	}

	private Path createOrUpdateCsr(final DomainInfo info, final Key privateKey) {
		final String message = this.msg.getMessage("key.process.create.csr", info.getName());
		final CsrProps csrProps = new CsrProps(info);
		return this.openSsl.createCsr(csrProps.getCsrFile(), csrProps.getCsrConfigFile(), privateKey, message);
	}

	@Getter
	private class CsrProps {

		private final String country;
		private final String state;
		private final String location;
		private final String zipCode;
		private final String street;
		private final String organization;
		private final String organizationUnit;
		private final String email;
		private final Path domainCertSubDir;
		private final Path csrFile;
		private final Path csrConfigFile;
		private final int keySize;
		private final ImmutableSet<String> alternativeNames;

		public CsrProps(final DomainInfo info) {
			final Settings g = CsrService.this.settingsService.getGlobal();
			final Settings d = CsrService.this.settingsService.getDomain(info);
			final Settings c = d.getBool(SettingsKey.OVERWRITE_CSR) ? d : g;

			this.country = c.get(SettingsKey.CSR_COUNTRY);
			this.state = c.get(SettingsKey.CSR_STATE);
			this.location = c.get(SettingsKey.CSR_LOCATION);
			this.zipCode = c.get(SettingsKey.CSR_ZIPCODE);
			this.street = c.get(SettingsKey.CSR_STREET);
			this.organization = c.get(SettingsKey.CSR_ORGANIZATION);
			this.organizationUnit = c.get(SettingsKey.CSR_ORGANIZATION_UNIT);
			this.email = c.get(SettingsKey.CSR_EMAIL);
			this.domainCertSubDir = CsrService.this.fs.getPath(info.getDirectory().toString(), g.get(SettingsKey.DOMAIN_CERT_SUBDIR));
			this.csrFile = CsrService.this.fs.getPath(this.domainCertSubDir.toString(), CSR_FILENAME);
			this.csrConfigFile = CsrService.this.fs.getPath(this.domainCertSubDir.toString(), CSR_CONFIG_FILENAME);
			this.keySize = c.getInt(SettingsKey.KEYS_SIZE);

			final Set<String> alternativeNames = d.getSet(SettingsKey.DOMAIN_ALTERNATIVE_NAMES);
			alternativeNames.add(info.getName());
			this.alternativeNames = ImmutableSet.copyOf(alternativeNames);

			this.validate();
		}

		private void validate() {
			if (!StringUtils.hasText(this.email)) {
				throw new EpicIllegalArgumentException(CsrService.this.msg.getMessage("validator.csr.email.empty"));
			}
			if (!EmailValidator.getInstance().isValid(this.email)) {
				throw new EpicIllegalArgumentException(CsrService.this.msg.getMessage("validator.csr.email.invalid"));
			}
		}
	}
}