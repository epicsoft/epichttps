/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.certificate;

import java.nio.file.Path;

import org.springframework.stereotype.Service;

import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.common.OpenSslAPI;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Service
@RequiredArgsConstructor
public class SelfSignedCertificateService {

	public static final String DOMAIN_INFO_ID = "selfsignedepichttps";

	private static final String CERT_FILENAME = "self_signed.crt";
	private static final String KEY_FILENAME = "self_signed.key";
	private static final String CSR_CONFIG_FILENAME = CsrService.CSR_CONFIG_FILENAME;
	private static final String CSR_FILENAME = CsrService.CSR_FILENAME;

	@NonNull
	private final SettingsService settingsService;
	@NonNull
	private final FileSystemAPI fs;
	@NonNull
	private final CsrService csrService;
	@NonNull
	private final OpenSslAPI openSsl;

	public void createSelfSignedCertificate() {
		final Settings env = this.settingsService.getEnv();
		final Path dir = this.fs.getPath(env.get(SettingsKey.LE_ROOT_DIR), env.get(SettingsKey.CUSTOM_SUBDIR), env.get(SettingsKey.LE_GENERAL_SUBDIR),
				env.get(SettingsKey.DOMAIN_CERT_SUBDIR));
		final Path cert = this.fs.getPath(dir.toString(), CERT_FILENAME);
		final Path key = this.fs.getPath(dir.toString(), KEY_FILENAME);
		final Path csrConfig = this.fs.getPath(dir.toString(), CSR_CONFIG_FILENAME);
		final Path csr = this.fs.getPath(dir.toString(), CSR_FILENAME);

		final String authDomain = env.get(SettingsKey.AUTH_DOMAIN);
		final DomainInfo info = DomainInfo.builder().id(DOMAIN_INFO_ID).name(authDomain).directory(dir).build();
		this.fs.createDirectory(dir);
		this.fs.saveFileIfChanged(csrConfig, this.csrService.buildCsrConfigTemplate(info));
		this.openSsl.createSelfSignedCertificate(key, cert, csr, csrConfig);

		// delete config.properties file, if exists
		final String configFileName = env.get(SettingsKey.CONFIG_FILE_NAME);
		this.fs.deleteFile(this.fs.getPath(dir.toString(), configFileName));
	}
}
