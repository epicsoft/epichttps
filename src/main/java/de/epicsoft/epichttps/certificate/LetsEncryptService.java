/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.certificate;

import java.nio.file.Path;

import org.springframework.stereotype.Service;

import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Service
@RequiredArgsConstructor
public class LetsEncryptService {

	@NonNull
	private final SettingsService settingsService;
	@NonNull
	private final FileSystemAPI fs;
	@NonNull
	private final LetsEncryptAPI api;

	/**
	 * Prueft ob ein LetsEncrypt Konto angelegt wurde, undabhaengig ob mit oder ohne Email-Adresse. <br>
	 * Annahme: wenn das LetsEncrypt Verzeichnis 'accounts' vorhanden ist, dann ist ein LetsEncrypt Konto vorhanden.
	 *
	 * @return {@link Boolean}
	 */
	public boolean isAccoutExists() {
		final Settings env = this.settingsService.getEnv();
		final Path leAccount = this.fs.getPath(env.get(SettingsKey.LE_ROOT_DIR), env.get(SettingsKey.LE_ACCOUNT_DIR));
		return this.fs.isDirectory(leAccount);
	}

	/**
	 * Erstellt einen neuen LE Account ohne Email. Dies sollte nur fuer Testzwecke verwendet werden und nicht im profuktiven Einsatz, da es hoeheres Sicherheitsrisiko darstellt.
	 *
	 * @param agreeTos
	 */
	public void createAccount(final Boolean agreeTos) {
		this.api.registerWithoutEmail(agreeTos);
	}

	/**
	 * Erstellt einen neuen LE Account mit Email.
	 *
	 * @param email
	 * @param agreeTos
	 */
	public void createAccount(final String email, final Boolean agreeTos) {
		this.api.register(email, agreeTos);
	}
}