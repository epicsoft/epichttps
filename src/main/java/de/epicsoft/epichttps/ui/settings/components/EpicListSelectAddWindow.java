/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.settings.components;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import de.epicsoft.epichttps.common.EpicMessageSource;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EpicListSelectAddWindow extends Window {

	private static final long serialVersionUID = 8552193039951581278L;

	@Autowired
	private EpicMessageSource msg;

	private final BeanValidationBinder<String> binder = new BeanValidationBinder<>(String.class);
	private final TextField input = new EpicTextField();

	private AddCallback addCallback;
	private ValidateCallback validateCallback;

	@PostConstruct
	public void init() {
		log.trace("EpicListSelectAddWindow::init");

		this.setClosable(true);
		this.setModal(true);
		this.setResizable(false);
		this.setDraggable(false);
		this.setWidth(30, Unit.EM);
		this.setHeight(10, Unit.EM);

		/* @formatter:off */
		this.binder.forField(this.input)
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.text.empty"))
			.withValidator(value -> this.validateCallback != null ? this.validateCallback.isValid(value) : true, this.msg.getMessage("validator.text.invalid"))
			.withValidator(value -> this.addCallback != null, this.msg.getMessage("validator.list.text.callback.empty"))
			.bind(source -> "", (bean, fieldvalue) -> {});
		/* @formatter:on */

		final HorizontalLayout buttons = new HorizontalLayout(this.buildAddButton(), this.buildCloseButton());
		buttons.setSpacing(true);

		final Label title = new Label(this.msg.getMessage("settings.list.text.window.title"));
		title.addStyleName(ValoTheme.LABEL_BOLD);

		final VerticalLayout content = new VerticalLayout(title, this.input, buttons);
		content.setSpacing(true);
		content.setMargin(true);

		this.setContent(content);
	}

	public void enter() {
		log.trace("EpicListSelectAddWindow::enter");

		this.input.setValue("");
		this.input.focus();
	}

	public void setAddCallback(final AddCallback callback) {
		this.addCallback = callback;
	}

	public void setValidateCallback(final ValidateCallback callback) {
		this.validateCallback = callback;
	}

	private Component buildAddButton() {
		final Button add = new Button();
		add.setCaption(this.msg.getMessage("settings.list.text.window.button.add"));
		add.addStyleName(ValoTheme.BUTTON_TINY);
		add.setClickShortcut(KeyCode.ENTER);
		add.addClickListener(e -> {
			if (this.binder.isValid()) {
				this.addCallback.addValue(this.input.getValue());
				this.close();
			} else {
				this.binder.validate();
			}
		});
		return add;
	}

	private Component buildCloseButton() {
		final Button close = new Button();
		close.setCaption(this.msg.getMessage("settings.list.text.window.button.cancel"));
		close.addStyleName(ValoTheme.BUTTON_TINY);
		close.addStyleName(ValoTheme.BUTTON_DANGER);
		close.addClickListener(e -> this.close());
		return close;
	}

	@FunctionalInterface
	public interface AddCallback {

		public void addValue(String value);
	}

	@FunctionalInterface
	public interface ValidateCallback {

		public boolean isValid(String value);
	}
}
