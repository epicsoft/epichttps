/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.settings;

import javax.annotation.PostConstruct;

import org.apache.commons.validator.routines.DomainValidator;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.facade.SettingsFacade;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.ui.common.SaveCancelButtons;
import de.epicsoft.epichttps.ui.settings.components.EpicTextField;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@SpringComponent("settingsTabAuth")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SettingsTabAuth extends FormLayout implements SettingsTab {

	private static final long serialVersionUID = -8412897720008489193L;

	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	protected transient SettingsFacade settingsFacade;

	@Autowired
	protected SaveCancelButtons buttons;

	private final TextField domain = new EpicTextField();
	private final TextField port = new EpicTextField();
	private final TextField includeFileName = new EpicTextField();
	private final TextField ipv4 = new EpicTextField();
	private final TextField ipv6 = new EpicTextField();
	protected final BeanValidationBinder<Settings> binder = new BeanValidationBinder<>(Settings.class);

	@PostConstruct
	public void init() {
		log.trace("SettingsTabAuth::init");

		this.domain.setCaption(this.msg.getMessage("settings.field.name.auth.domain"));
		this.port.setCaption(this.msg.getMessage("settings.field.name.auth.port"));
		this.includeFileName.setCaption(this.msg.getMessage("settings.field.name.auth.include.filename"));
		this.ipv4.setCaption(this.msg.getMessage("settings.field.name.auth.ipv4"));
		this.ipv6.setCaption(this.msg.getMessage("settings.field.name.auth.ipv6"));

		/* @formatter:off */
		this.binder.forField(this.domain)
		  .withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.email.empty"))
			.withValidator(value -> DomainValidator.getInstance().isValid(value), this.msg.getMessage("validator.domain.invalid"))
			.bind(source -> source.get(SettingsKey.AUTH_DOMAIN), (bean, fieldvalue) -> bean.set(SettingsKey.AUTH_DOMAIN, fieldvalue));
		this.binder.forField(this.port)
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.port.empty"))
			.withConverter(new StringToIntegerConverter(this.msg.getMessage("validator.port.numeric")))
			.withValidator(new IntegerRangeValidator(this.msg.getMessage("validator.port.range"), 1, 65535))
			.bind(source -> source.getInt(SettingsKey.AUTH_PORT), (bean, fieldvalue) -> bean.set(SettingsKey.AUTH_PORT, fieldvalue));
		this.binder.forField(this.includeFileName)
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.filename.empty"))
			.bind(source -> source.get(SettingsKey.AUTH_INCLUDE_FILENAME), (bean, fieldvalue) -> bean.set(SettingsKey.AUTH_INCLUDE_FILENAME, fieldvalue));
		this.binder.forField(this.ipv4)
		  .withValidator(value -> StringUtils.isEmpty(value) || InetAddressValidator.getInstance().isValidInet4Address(value), this.msg.getMessage("validator.ipv4.invalid"))
			.bind(source -> source.get(SettingsKey.AUTH_IPV4), (bean, fieldvalue) -> bean.set(SettingsKey.AUTH_IPV4, fieldvalue));
		this.binder.forField(this.ipv6)
		  .withValidator(value-> StringUtils.isEmpty(value) || InetAddressValidator.getInstance().isValidInet6Address(value), this.msg.getMessage("validator.ipv6.invalid"))
			.bind(source -> source.get(SettingsKey.AUTH_IPV6), (bean, fieldvalue) -> bean.set(SettingsKey.AUTH_IPV6, fieldvalue));
		/* @formatter:on */

		this.buttons.getSave().addClickListener(event -> {
			if (this.binder.isValid()) {
				this.settingsFacade.save(this.binder.getBean());
				Notification.show(this.msg.getMessage("settings.button.save.success"), Type.HUMANIZED_MESSAGE);
			} else {
				this.binder.validate();
			}
		});

		this.addComponents(this.domain, this.port, this.includeFileName, this.ipv4, this.ipv6);
		this.addComponent(this.buttons);
		this.setSizeFull();
		this.setMargin(true);
	}

	@Override
	public void enter() {
		log.trace("SettingsTabAuth::enter");

		final Settings global = this.settingsFacade.getGlobal();
		this.binder.setBean(global);
	}
}