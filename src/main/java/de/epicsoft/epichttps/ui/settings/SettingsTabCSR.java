/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.settings;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.facade.SettingsFacade;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.ui.common.SaveCancelButtons;
import de.epicsoft.epichttps.ui.settings.components.EpicTextField;
import lombok.extern.slf4j.Slf4j;

/**
 * CSR Certificate Signing Request (deutsch „Zertifikatsregistrierungsanforderung“) <br>
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@SpringComponent("settingsTabCSR")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SettingsTabCSR extends FormLayout implements SettingsTab {

	private static final long serialVersionUID = -2350682569020302032L;

	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	protected transient SettingsFacade settingsFacade;

	@Autowired
	protected SaveCancelButtons buttons;

	private final TextField country = new EpicTextField();
	private final TextField state = new EpicTextField();
	private final TextField location = new EpicTextField();
	private final TextField zipcode = new EpicTextField();
	private final TextField street = new EpicTextField();
	private final TextField organization = new EpicTextField();
	private final TextField organizationUnit = new EpicTextField();
	private final TextField email = new EpicTextField();
	protected final BeanValidationBinder<Settings> binder = new BeanValidationBinder<>(Settings.class);

	@PostConstruct
	public void init() {
		log.trace("SettingsTabCSR::init");

		this.country.setCaption(this.msg.getMessage("settings.field.name.csr.country"));
		this.country.setDescription(this.msg.getMessage("settings.field.description.csr.country"));
		this.state.setCaption(this.msg.getMessage("settings.field.name.csr.state"));
		this.state.setDescription(this.msg.getMessage("settings.field.description.csr.state"));
		this.location.setCaption(this.msg.getMessage("settings.field.name.csr.location"));
		this.location.setDescription(this.msg.getMessage("settings.field.description.csr.location"));
		this.zipcode.setCaption(this.msg.getMessage("settings.field.name.csr.zipcode"));
		this.zipcode.setDescription(this.msg.getMessage("settings.field.description.csr.zipcode"));
		this.street.setCaption(this.msg.getMessage("settings.field.name.csr.street"));
		this.street.setDescription(this.msg.getMessage("settings.field.description.csr.street"));
		this.organization.setCaption(this.msg.getMessage("settings.field.name.csr.organization"));
		this.organization.setDescription(this.msg.getMessage("settings.field.description.csr.organization"));
		this.organizationUnit.setCaption(this.msg.getMessage("settings.field.name.csr.organization.unit"));
		this.organizationUnit.setDescription(this.msg.getMessage("settings.field.description.csr.organization.unit"));
		this.email.setCaption(this.msg.getMessage("settings.field.name.csr.email"));
		this.email.setDescription(this.msg.getMessage("settings.field.description.csr.email"));

		/* @formatter:off */
		this.binder.forField(this.country)
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.country.empty"))
			.withValidator(value -> value.length() == 2, this.msg.getMessage("validator.crs.country.invalid"))
			.bind(source -> source.get(SettingsKey.CSR_COUNTRY), (bean, fieldvalue) -> bean.set(SettingsKey.CSR_COUNTRY, fieldvalue));
		this.binder.forField(this.state)
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.state.empty"))
			.bind(source -> source.get(SettingsKey.CSR_STATE), (bean, fieldvalue) -> bean.set(SettingsKey.CSR_STATE, fieldvalue));
		this.binder.forField(this.location)
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.location.empty"))
			.bind(source -> source.get(SettingsKey.CSR_LOCATION), (bean, fieldvalue) -> bean.set(SettingsKey.CSR_LOCATION, fieldvalue));
		this.binder.forField(this.zipcode)
		  .bind(source -> source.get(SettingsKey.CSR_ZIPCODE), (bean, fieldvalue) -> bean.set(SettingsKey.CSR_ZIPCODE, fieldvalue));
		this.binder.forField(this.street)
		  .bind(source -> source.get(SettingsKey.CSR_STREET), (bean, fieldvalue) -> bean.set(SettingsKey.CSR_STREET, fieldvalue));
		this.binder.forField(this.organization)
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.organization.empty"))
			.bind(source -> source.get(SettingsKey.CSR_ORGANIZATION), (bean, fieldvalue) -> bean.set(SettingsKey.CSR_ORGANIZATION, fieldvalue));
		this.binder.forField(this.organizationUnit)
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.organization.unit.empty"))
			.bind(source -> source.get(SettingsKey.CSR_ORGANIZATION_UNIT), (bean, fieldvalue) -> bean.set(SettingsKey.CSR_ORGANIZATION_UNIT, fieldvalue));
		this.binder.forField(this.email)
		  .withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.email.empty"))
			.withValidator(new EmailValidator(this.msg.getMessage("validator.email.invalid")))
			.bind(source -> source.get(SettingsKey.CSR_EMAIL), (bean, fieldvalue) -> bean.set(SettingsKey.CSR_EMAIL, fieldvalue));
		/* @formatter:on */

		this.buttons.getSave().addClickListener(event -> {
			if (this.binder.isValid()) {
				this.settingsFacade.save(this.binder.getBean());
				Notification.show(this.msg.getMessage("settings.button.save.success"), Type.HUMANIZED_MESSAGE);
			} else {
				this.binder.validate();
			}
		});

		this.addComponents(this.country, this.state, this.location, this.zipcode, this.street, this.organization, this.organizationUnit, this.email);
		this.addComponent(this.buttons);
		this.setSizeFull();
		this.setMargin(true);
	}

	@Override
	public void enter() {
		log.trace("SettingsTabCSR::enter");

		final Settings global = this.settingsFacade.getGlobal();
		this.binder.setBean(global);
	}
}