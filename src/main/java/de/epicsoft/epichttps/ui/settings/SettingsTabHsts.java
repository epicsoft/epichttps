/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.settings;

import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;

import com.google.common.base.Strings;
import com.vaadin.data.BeanValidationBinder;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.common.NumberUtils;
import de.epicsoft.epichttps.facade.SettingsFacade;
import de.epicsoft.epichttps.settings.ConsistencyValidation;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.ui.common.HorizontalLine;
import de.epicsoft.epichttps.ui.common.SaveCancelButtons;
import de.epicsoft.epichttps.ui.settings.components.EpicListSelect;
import de.epicsoft.epichttps.ui.settings.components.EpicTextField;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@SpringComponent("settingsTabHsts")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SettingsTabHsts extends FormLayout implements SettingsTab {

	private static final long serialVersionUID = 3811246162229579202L;

	private static final String REGEX_PIN = "^[a-zA-Z0-9+/]+={0,2}$";

	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	private transient ConsistencyValidation consistencyValidation;
	@Autowired
	protected transient SettingsFacade settingsFacade;

	@Autowired
	private EpicListSelect hpkpPins;
	@Autowired
	protected SaveCancelButtons buttons;

	private final CheckBox hstsEnable = new CheckBox();
	private final TextField hstsMaxAge = new EpicTextField();
	private final CheckBox hstsIncludeSubdomains = new CheckBox();
	private final CheckBox hstsPreload = new CheckBox();

	private final CheckBox hpkpEnable = new CheckBox();
	private final TextField hpkpMaxAge = new EpicTextField();
	private final CheckBox hpkpIncludeSubdomains = new CheckBox();
	private final CheckBox hpkpReportEnable = new CheckBox();
	private final TextField hpkpReportEnforce = new EpicTextField();
	private final TextField hpkpReportOnly = new EpicTextField();

	protected final BeanValidationBinder<Settings> binder = new BeanValidationBinder<>(Settings.class);

	@PostConstruct
	public void init() {
		log.trace("SettingsTabHsts::init");

		this.hstsEnable.setCaption(this.msg.getMessage("settings.field.name.hsts.enable"));
		this.hstsMaxAge.setCaption(this.msg.getMessage("settings.field.name.hsts.max.age"));
		this.hstsMaxAge.setDescription(this.msg.getMessage("settings.field.name.hsts.max.age.description"));
		this.hstsIncludeSubdomains.setCaption(this.msg.getMessage("settings.field.name.hsts.include.subdomains"));
		this.hstsPreload.setCaption(this.msg.getMessage("settings.field.name.hsts.preload"));

		this.hpkpEnable.setCaption(this.msg.getMessage("settings.field.name.hpkp.enable"));
		this.hpkpMaxAge.setCaption(this.msg.getMessage("settings.field.name.hpkp.max.age"));
		this.hpkpIncludeSubdomains.setCaption(this.msg.getMessage("settings.field.name.hpkp.include.subdomains"));
		this.hpkpPins.setCaption(this.msg.getMessage("settings.field.name.hpkp.add.pins"));
		this.hpkpReportEnable.setCaption(this.msg.getMessage("settings.field.name.hpkp.report.enable"));
		this.hpkpReportEnforce.setCaption(this.msg.getMessage("settings.field.name.hpkp.report.enforce"));
		this.hpkpReportOnly.setCaption(this.msg.getMessage("settings.field.name.hpkp.report.only"));

		this.enabledHsts(false);
		this.enabledHpkp(false);
		this.hstsEnable.addValueChangeListener(event -> this.enabledHsts(event.getValue()));
		this.hpkpEnable.addValueChangeListener(event -> this.enabledHpkp(event.getValue()));
		this.hpkpReportEnable.addValueChangeListener(event -> this.enabledHpkpReport(event.getValue()));
		this.hpkpPins.setListWidth(30);

		/* @formatter:off */
		this.binder.forField(this.hstsEnable)
			.bind(source -> source.getBool(SettingsKey.HSTS_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.HSTS_ENABLE, fieldvalue));
		this.binder.forField(this.hstsMaxAge)
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.text.empty"))
			.withValidator(value -> NumberUtils.isNumeric(value), this.msg.getMessage("validator.text.numeric"))
			.withValidator(value -> Integer.valueOf(value) >= 0, this.msg.getMessage("validator.text.numeric.negative"))
			.bind(source -> source.get(SettingsKey.HSTS_MAX_AGE), (bean, fieldvalue) -> bean.set(SettingsKey.HSTS_MAX_AGE, fieldvalue));
		this.binder.forField(this.hstsIncludeSubdomains)
			.bind(source -> source.getBool(SettingsKey.HSTS_INCLUDE_SUBDOMAINS), (bean, fieldvalue) -> bean.set(SettingsKey.HSTS_INCLUDE_SUBDOMAINS, fieldvalue));
		this.binder.forField(this.hstsPreload)
		  .bind(source -> source.getBool(SettingsKey.HSTS_PRELOAD), (bean, fieldvalue) -> bean.set(SettingsKey.HSTS_PRELOAD, fieldvalue));
		this.binder.forField(this.hpkpEnable)
		  .bind(source -> source.getBool(SettingsKey.HPKP_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.HPKP_ENABLE, fieldvalue));
		this.binder.forField(this.hpkpMaxAge)
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.text.empty"))
			.withValidator(value -> NumberUtils.isNumeric(value), this.msg.getMessage("validator.text.numeric"))
			.withValidator(value -> Integer.valueOf(value) >= 0, this.msg.getMessage("validator.text.numeric.negative"))
			.bind(source -> source.get(SettingsKey.HPKP_MAX_AGE), (bean, fieldvalue) -> bean.set(SettingsKey.HPKP_MAX_AGE, fieldvalue));
		this.binder.forField(this.hpkpIncludeSubdomains)
		 	.bind(source -> source.getBool(SettingsKey.HPKP_INCLUDE_SUBDOMAINS), (bean, fieldvalue) -> bean.set(SettingsKey.HPKP_INCLUDE_SUBDOMAINS, fieldvalue));
		this.binder.forField(this.hpkpPins)
			.bind(source -> source.getSet(SettingsKey.HPKP_ADD_PINS), (bean, fieldvalue) -> bean.set(SettingsKey.HPKP_ADD_PINS, fieldvalue));
		this.binder.forField(this.hpkpReportEnable)
			.bind(source -> source.getBool(SettingsKey.HPKP_REPORT_ENABLE), (bean, fieldvalue) -> {bean.set(SettingsKey.HPKP_REPORT_ENABLE, fieldvalue);});
		this.binder.forField(this.hpkpReportEnforce)
			.withValidator(value -> StringUtils.isEmpty(value) || UrlValidator.getInstance().isValid(value), this.msg.getMessage("validator.hpkp.report.invalid"))
			.bind(source -> source.get(SettingsKey.HPKP_REPORT_ENFORCE), (bean, fieldvalue) -> bean.set(SettingsKey.HPKP_REPORT_ENFORCE, fieldvalue));
		this.binder.forField(this.hpkpReportOnly)
			.withValidator(value -> StringUtils.isEmpty(value) || UrlValidator.getInstance().isValid(value), this.msg.getMessage("validator.hpkp.report.invalid"))
			.bind(source -> source.get(SettingsKey.HPKP_REPORT_ONLY), (bean, fieldvalue) -> bean.set(SettingsKey.HPKP_REPORT_ONLY, fieldvalue));
		this.binder.withValidator(this.consistencyValidation.buildValidator());
		/* @formatter:on */

		this.hpkpPins.setValidateCallback(value -> {
			boolean valid = !Strings.isNullOrEmpty(value);
			valid = valid && Pattern.compile(REGEX_PIN).matcher(value).find();
			return valid;
		});

		this.buttons.getSave().addClickListener(event -> {
			if (this.binder.isValid()) {
				this.hpkpPins.selectAll();
				this.settingsFacade.save(this.binder.getBean());
				Notification.show(this.msg.getMessage("settings.button.save.success"), Type.HUMANIZED_MESSAGE);
			} else {
				this.showError(this.binder.validate());
			}
		});

		// final Label hstsInfo = new Label(this.msg.getMessage("info.hsts.wiki"), ContentMode.HTML);
		// hstsInfo.addStyleName(ValoTheme.LABEL_H3);
		// final Label hpkpInfo = new Label(this.msg.getMessage("info.hpkp.wiki"), ContentMode.HTML);
		// hpkpInfo.addStyleName(ValoTheme.LABEL_H3);

		// this.addComponent(hstsInfo);
		this.addComponents(this.hstsEnable, this.hstsMaxAge, this.hstsIncludeSubdomains, this.hstsPreload);
		this.addComponent(new HorizontalLine());
		// this.addComponent(hpkpInfo);
		this.addComponents(this.hpkpEnable, this.hpkpMaxAge, this.hpkpIncludeSubdomains, this.hpkpPins);
		this.addComponents(this.hpkpReportEnable, this.hpkpReportEnforce, this.hpkpReportOnly);
		this.addComponent(this.buttons);
		this.setSizeFull();
		this.setMargin(true);
	}

	@Override
	public void enter() {
		log.trace("SettingsTabHsts::enter");

		final Settings global = this.settingsFacade.getGlobal();
		this.binder.setBean(global);
	}

	private void enabledHsts(final Boolean enabled) {
		this.hstsMaxAge.setEnabled(enabled);
		this.hstsIncludeSubdomains.setEnabled(enabled);
		this.hstsPreload.setEnabled(enabled);
	}

	private void enabledHpkp(final Boolean enabled) {
		this.hpkpMaxAge.setEnabled(enabled);
		this.hpkpIncludeSubdomains.setEnabled(enabled);
		this.hpkpPins.setEnabled(enabled);
		this.hpkpReportEnable.setEnabled(enabled);
		this.enabledHpkpReport(enabled && this.hpkpReportEnable.getValue());
	}

	private void enabledHpkpReport(final Boolean enabled) {
		this.hpkpReportEnforce.setEnabled(enabled);
		this.hpkpReportOnly.setEnabled(enabled);
	}
}