/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.settings.components;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.data.HasValue;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.Registration;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import de.epicsoft.epichttps.ui.settings.components.EpicListSelectAddWindow.ValidateCallback;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EpicListSelect extends HorizontalLayout implements HasValue<Set<String>> {

	private static final long serialVersionUID = 1915826236106016774L;

	@Autowired
	private EpicListSelectAddWindow addWindow;

	private final ListSelect<String> list = new ListSelect<>();
	private final Set<String> values = new HashSet<>();
	private final Button add = new Button();
	private final Button del = new Button();
	private final VerticalLayout buttons = new VerticalLayout();
	private ValidateCallback removeCallback = null;

	@PostConstruct
	public void init() {
		log.trace("EpicListSelect::init");

		this.add.setIcon(VaadinIcons.PLUS);
		this.add.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.add.addStyleName(ValoTheme.BUTTON_ICON_ONLY);
		this.add.addStyleName(ValoTheme.BUTTON_SMALL);
		this.add.setClickShortcut(KeyCode.ENTER);
		this.add.addClickListener(e -> {
			UI.getCurrent().addWindow(this.addWindow);
			this.addWindow.enter();
		});

		this.del.setIcon(VaadinIcons.MINUS);
		this.del.addStyleName(ValoTheme.BUTTON_DANGER);
		this.del.addStyleName(ValoTheme.BUTTON_ICON_ONLY);
		this.del.addStyleName(ValoTheme.BUTTON_SMALL);
		this.del.addClickListener(event -> {
			final Set<String> value = this.list.getSelectedItems();
			if (this.removeCallback == null) {
				this.values.removeAll(value);
			} else if (this.removeCallback != null) {
				value.stream().filter(val -> this.removeCallback.isValid(val)).forEach(val -> this.values.remove(val));
			}
			this.list.setItems(this.values);
		});

		this.list.setWidth(20, Unit.EM);
		this.list.setHeight(6, Unit.EM);

		this.buttons.addComponents(this.add, this.del);
		this.buttons.setMargin(false);

		this.addWindow.setAddCallback(value -> {
			this.values.add(value);
			this.list.setItems(this.values);
		});

		this.addComponents(this.list, this.buttons);
	}

	public void selectAll() {
		this.list.select(this.values.stream().toArray(String[]::new));
	}

	public void setValidateCallback(final ValidateCallback callback) {
		this.addWindow.setValidateCallback(callback);
	}

	public void setRemoveValidateCallback(final ValidateCallback removeCallback) {
		this.removeCallback = removeCallback;
	}

	public void setListHeight(final float em) {
		this.list.setHeight(em, Unit.EM);
	}

	public void setListWidth(final float em) {
		this.list.setWidth(em, Unit.EM);
	}

	@Override
	public void setValue(final Set<String> value) {
		this.values.clear();
		if (value != null) {
			this.values.addAll(value);
		}
		this.list.setItems(this.values);
	}

	@Override
	public Set<String> getValue() {
		return new HashSet<>(this.values);
	}

	@Override
	public Registration addValueChangeListener(final ValueChangeListener<Set<String>> listener) {
		return this.list.addValueChangeListener(listener);
	}

	@Override
	public void setRequiredIndicatorVisible(final boolean requiredIndicatorVisible) {
		this.list.setRequiredIndicatorVisible(requiredIndicatorVisible);
	}

	@Override
	public boolean isRequiredIndicatorVisible() {
		return this.list.isRequiredIndicatorVisible();
	}

	@Override
	public void setReadOnly(final boolean readOnly) {
		super.setReadOnly(readOnly);
	}

	@Override
	public boolean isReadOnly() {
		return super.isReadOnly();
	}
}