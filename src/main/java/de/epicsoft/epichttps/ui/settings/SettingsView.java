/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.settings;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.ui.common.EpicView;
import de.epicsoft.epichttps.ui.common.ViewHelper;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@UIScope
@SpringComponent
@SpringView(name = SettingsView.NAME)
public class SettingsView extends VerticalLayout implements EpicView {

	private static final long serialVersionUID = -8516797840410710661L;

	public static final String NAME = "settings";

	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	private transient ViewHelper viewHelper;

	@Autowired
	@Qualifier("settingsTabCSR")
	private SettingsTabCSR tabCSR;
	@Autowired
	@Qualifier("settingsTabAuth")
	private SettingsTabAuth tabAuth;
	@Autowired
	@Qualifier("settingsTabHsts")
	private SettingsTabHsts tabHsts;
	@Autowired
	@Qualifier("settingsTabWebserver")
	private SettingsTabWebserver tabWebserver;
	@Autowired
	@Qualifier("settingsTabCert")
	private SettingsTabCert tabCert;

	private final TabSheet tabs = new TabSheet();

	@PostConstruct
	public void init() {
		log.trace("SettinigsView:init");

		final Component header = this.viewHelper.buildHeader(this.msg.getMessage("settings.header.title"));
		final Component content = this.buildContent();

		this.addComponents(header, content);
		this.setExpandRatio(content, 1f);
		this.setSizeFull();
		this.setSpacing(true);
		this.setMargin(false);
	}

	@Override
	public void enter(final ViewChangeEvent event) {
		log.trace("SettinigsView::enter");

		final SettingsTabAuth defaultTab = this.tabAuth;
		defaultTab.enter();
		this.tabs.setSelectedTab(defaultTab);
	}

	private Component buildContent() {
		this.tabs.addStyleName(ValoTheme.TABSHEET_EQUAL_WIDTH_TABS);
		this.tabs.addTab(this.tabAuth, this.msg.getMessage("settings.tab.name.auth"));
		this.tabs.addTab(this.tabCSR, this.msg.getMessage("settings.tab.name.csr"));
		this.tabs.addTab(this.tabHsts, this.msg.getMessage("settings.tab.name.hsts.hpkp"));
		this.tabs.addTab(this.tabWebserver, this.tabWebserver.getTabCaption());
		this.tabs.addTab(this.tabCert, this.msg.getMessage("settings.tab.name.cert"));

		this.tabs.addSelectedTabChangeListener(event -> {
			final Component tab = event.getTabSheet().getSelectedTab();
			if (tab instanceof SettingsTab) {
				((SettingsTab) tab).enter();
			}
		});

		final VerticalLayout wrapper = new VerticalLayout(this.tabs);
		wrapper.setHeightUndefined();
		wrapper.setWidth(100, Unit.PERCENTAGE);

		final Panel panel = new Panel(wrapper);
		panel.addStyleName(ValoTheme.PANEL_WELL);
		panel.setSizeFull();
		return panel;
	}
}