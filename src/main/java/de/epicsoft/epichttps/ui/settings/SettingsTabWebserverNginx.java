/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.settings;

import javax.annotation.PostConstruct;

import org.apache.commons.validator.routines.InetAddressValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import de.epicsoft.epichttps.Profiles;
import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.facade.SettingsFacade;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.ui.common.HorizontalLine;
import de.epicsoft.epichttps.ui.common.SaveCancelButtons;
import de.epicsoft.epichttps.ui.settings.components.EpicListSelect;
import de.epicsoft.epichttps.ui.settings.components.EpicTextArea;
import de.epicsoft.epichttps.ui.settings.components.EpicTextField;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Profile(Profiles.NGINX)
@SpringComponent("settingsTabWebserver")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SettingsTabWebserverNginx extends HorizontalLayout implements SettingsTabWebserver {

	private static final long serialVersionUID = 2799164656656998284L;

	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	protected transient SettingsFacade settingsFacade;

	@Autowired
	protected SaveCancelButtons buttons;

	@Autowired
	private EpicListSelect nginxResolverIpv4;
	@Autowired
	private EpicListSelect nginxResolverIpv6;

	private final Label enableTitle = new Label();

	private final TextField nginxResolverTimeout = new EpicTextField();
	private final TextField nginxSslSessionCache = new EpicTextField();
	private final TextField nginxSslSessionTimeout = new EpicTextField();
	private final TextField nginxSslProtocols = new EpicTextField();
	private final TextArea nginxSslCiphers = new EpicTextArea();
	private final CheckBox nginxSslPreferServerCiphers = new CheckBox();
	private final CheckBox nginxSslStapling = new CheckBox();
	private final CheckBox nginxSslStaplingVerify = new CheckBox();
	private final TextField nginxSslEcdhCurve = new EpicTextField();
	private final TextField nginxHeaderFrameOption = new EpicTextField();
	private final TextField nginxHeaderContentTypeOptions = new EpicTextField();
	private final TextField nginxHeaderXssProtection = new EpicTextField();

	private final CheckBox nginxResolverIpv4Enable = new CheckBox();
	private final CheckBox nginxResolverIpv6Enable = new CheckBox();
	private final CheckBox nginxResolverTimeoutEnable = new CheckBox();
	private final CheckBox nginxSslSessionCacheEnable = new CheckBox();
	private final CheckBox nginxSslSessionTimeoutEnable = new CheckBox();
	private final CheckBox nginxSslPreferServerCiphersEnable = new CheckBox();
	private final CheckBox nginxSslStaplingEnable = new CheckBox();
	private final CheckBox nginxSslStaplingVerifyEnable = new CheckBox();
	private final CheckBox nginxSslEcdhCurveEnable = new CheckBox();
	private final CheckBox nginxHeaderFrameOptionEnable = new CheckBox();
	private final CheckBox nginxHeaderContentTypeOptionsEnable = new CheckBox();
	private final CheckBox nginxHeaderXssProtectionEnable = new CheckBox();
	private final CheckBox nginxSslTrustedCertificateEnable = new CheckBox();

	protected final BeanValidationBinder<Settings> binder = new BeanValidationBinder<>(Settings.class);

	@PostConstruct
	public void init() {
		log.trace("SettingsTabWebserverNginx::init");

		this.enabledFields(false);
		this.enableTitle.setCaption(this.msg.getMessage("settings.title.name.web.enabled.elements"));

		this.nginxResolverIpv4.setCaption(this.msg.getMessage("settings.field.name.web.resolver.ipv4"));
		this.nginxResolverIpv6.setCaption(this.msg.getMessage("settings.field.name.web.resolver.ipv6"));
		this.nginxResolverTimeout.setCaption(this.msg.getMessage("settings.field.name.web.resolver.timeout"));
		this.nginxSslSessionCache.setCaption(this.msg.getMessage("settings.field.name.web.ssl.session.cache"));
		this.nginxSslSessionTimeout.setCaption(this.msg.getMessage("settings.field.name.web.ssl.session.timeout"));
		this.nginxSslProtocols.setCaption(this.msg.getMessage("settings.field.name.web.ssl.protocols"));
		this.nginxSslCiphers.setCaption(this.msg.getMessage("settings.field.name.web.ssl.ciphers"));
		this.nginxSslPreferServerCiphers.setCaption(this.msg.getMessage("settings.field.name.web.ssl.prefer.server.ciphers"));
		this.nginxSslStapling.setCaption(this.msg.getMessage("settings.field.name.web.ssl.stapling"));
		this.nginxSslStaplingVerify.setCaption(this.msg.getMessage("settings.field.name.web.ssl.stapling.verify"));
		this.nginxSslEcdhCurve.setCaption(this.msg.getMessage("settings.field.name.web.ssl.ecdh.curve"));
		this.nginxHeaderFrameOption.setCaption(this.msg.getMessage("settings.field.name.web.header.frame.option"));
		this.nginxHeaderContentTypeOptions.setCaption(this.msg.getMessage("settings.field.name.web.header.content.type.options"));
		this.nginxHeaderXssProtection.setCaption(this.msg.getMessage("settings.field.name.web.header.xss.protection"));

		this.nginxResolverIpv4Enable.setCaption(this.msg.getMessage("settings.field.name.web.resolver.ipv4"));
		this.nginxResolverIpv6Enable.setCaption(this.msg.getMessage("settings.field.name.web.resolver.ipv6"));
		this.nginxResolverTimeoutEnable.setCaption(this.msg.getMessage("settings.field.name.web.resolver.timeout"));
		this.nginxSslSessionCacheEnable.setCaption(this.msg.getMessage("settings.field.name.web.ssl.session.cache"));
		this.nginxSslSessionTimeoutEnable.setCaption(this.msg.getMessage("settings.field.name.web.ssl.session.timeout"));
		this.nginxSslPreferServerCiphersEnable.setCaption(this.msg.getMessage("settings.field.name.web.ssl.prefer.server.ciphers"));
		this.nginxSslStaplingEnable.setCaption(this.msg.getMessage("settings.field.name.web.ssl.stapling"));
		this.nginxSslStaplingVerifyEnable.setCaption(this.msg.getMessage("settings.field.name.web.ssl.stapling.verify"));
		this.nginxSslEcdhCurveEnable.setCaption(this.msg.getMessage("settings.field.name.web.ssl.ecdh.curve"));
		this.nginxHeaderFrameOptionEnable.setCaption(this.msg.getMessage("settings.field.name.web.header.frame.option"));
		this.nginxHeaderContentTypeOptionsEnable.setCaption(this.msg.getMessage("settings.field.name.web.header.content.type.options"));
		this.nginxHeaderXssProtectionEnable.setCaption(this.msg.getMessage("settings.field.name.web.header.xss.protection"));
		this.nginxSslTrustedCertificateEnable.setCaption(this.msg.getMessage("settings.field.name.web.ssl.trusted.certificate"));

		this.nginxResolverIpv4Enable.addValueChangeListener(event -> this.nginxResolverIpv4.setEnabled(event.getValue()));
		this.nginxResolverIpv6Enable.addValueChangeListener(event -> this.nginxResolverIpv6.setEnabled(event.getValue()));
		this.nginxResolverTimeoutEnable.addValueChangeListener(event -> this.nginxResolverTimeout.setEnabled(event.getValue()));
		this.nginxSslSessionCacheEnable.addValueChangeListener(event -> this.nginxSslSessionCache.setEnabled(event.getValue()));
		this.nginxSslSessionTimeoutEnable.addValueChangeListener(event -> this.nginxSslSessionTimeout.setEnabled(event.getValue()));
		this.nginxSslPreferServerCiphersEnable.addValueChangeListener(event -> this.nginxSslPreferServerCiphers.setEnabled(event.getValue()));
		this.nginxSslStaplingEnable.addValueChangeListener(event -> this.nginxSslStapling.setEnabled(event.getValue()));
		this.nginxSslStaplingVerifyEnable.addValueChangeListener(event -> this.nginxSslStaplingVerify.setEnabled(event.getValue()));
		this.nginxSslEcdhCurveEnable.addValueChangeListener(event -> this.nginxSslEcdhCurve.setEnabled(event.getValue()));
		this.nginxHeaderFrameOptionEnable.addValueChangeListener(event -> this.nginxHeaderFrameOption.setEnabled(event.getValue()));
		this.nginxHeaderContentTypeOptionsEnable.addValueChangeListener(event -> this.nginxHeaderContentTypeOptions.setEnabled(event.getValue()));
		this.nginxHeaderXssProtectionEnable.addValueChangeListener(event -> this.nginxHeaderXssProtection.setEnabled(event.getValue()));

		/* @formatter:off */
		this.binder.forField(this.nginxResolverIpv4)
			.bind(source -> source.getSet(SettingsKey.NGINX_RESOLVER_IPV4), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_RESOLVER_IPV4, fieldvalue));
		this.binder.forField(this.nginxResolverIpv6)
			.bind(source -> source.getSet(SettingsKey.NGINX_RESOLVER_IPV6), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_RESOLVER_IPV6, fieldvalue));
		this.binder.forField(this.nginxResolverTimeout)
			.bind(source -> source.get(SettingsKey.NGINX_RESOLVER_TIMEOUT), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_RESOLVER_TIMEOUT, fieldvalue));
		this.binder.forField(this.nginxSslSessionCache)
			.bind(source -> source.get(SettingsKey.NGINX_SSL_SESSION_CACHE), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_SSL_SESSION_CACHE, fieldvalue));
		this.binder.forField(this.nginxSslSessionTimeout)
			.bind(source -> source.get(SettingsKey.NGINX_SSL_SESSION_TIMEOUT), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_SSL_SESSION_TIMEOUT, fieldvalue));
		this.binder.forField(this.nginxSslProtocols)
			.bind(source -> source.get(SettingsKey.NGINX_SSL_PROTOCOLS), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_SSL_PROTOCOLS, fieldvalue));
		this.binder.forField(this.nginxSslCiphers)
			.bind(source -> source.get(SettingsKey.NGINX_SSL_CIPHERS), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_SSL_CIPHERS, fieldvalue));
		this.binder.forField(this.nginxSslPreferServerCiphers)
			.bind(source -> source.getBool(SettingsKey.NGINX_SSL_PREFER_SERVER_CIPHERS), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_SSL_PREFER_SERVER_CIPHERS, fieldvalue));
		this.binder.forField(this.nginxSslStapling)
			.bind(source -> source.getBool(SettingsKey.NGINX_SSL_STAPLING), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_SSL_STAPLING, fieldvalue));
		this.binder.forField(this.nginxSslStaplingVerify)
			.bind(source -> source.getBool(SettingsKey.NGINX_SSL_STAPLING_VERIFY), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_SSL_STAPLING_VERIFY, fieldvalue));
		this.binder.forField(this.nginxSslEcdhCurve)
			.bind(source -> source.get(SettingsKey.NGINX_SSL_ECDH_CURVE), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_SSL_ECDH_CURVE, fieldvalue));
		this.binder.forField(this.nginxHeaderFrameOption)
			.bind(source -> source.get(SettingsKey.NGINX_HEADER_FRAME_OPTION), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_HEADER_FRAME_OPTION, fieldvalue));
		this.binder.forField(this.nginxHeaderContentTypeOptions)
			.bind(source -> source.get(SettingsKey.NGINX_HEADER_CONTENT_TYPE_OPTIONS), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_HEADER_CONTENT_TYPE_OPTIONS, fieldvalue));
		this.binder.forField(this.nginxHeaderXssProtection)
			.bind(source -> source.get(SettingsKey.NGINX_HEADER_XSS_PROTECTION), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_HEADER_XSS_PROTECTION, fieldvalue));

		this.binder.forField(this.nginxResolverIpv4Enable)
			.bind(source -> source.getBool(SettingsKey.NGINX_RESOLVER_IPV4_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_RESOLVER_IPV4_ENABLE, fieldvalue));
		this.binder.forField(this.nginxResolverIpv6Enable)
			.bind(source -> source.getBool(SettingsKey.NGINX_RESOLVER_IPV6_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_RESOLVER_IPV6_ENABLE, fieldvalue));
		this.binder.forField(this.nginxResolverTimeoutEnable)
			.bind(source -> source.getBool(SettingsKey.NGINX_RESOLVER_TIMEOUT_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_RESOLVER_TIMEOUT_ENABLE, fieldvalue));
		this.binder.forField(this.nginxSslSessionCacheEnable)
			.bind(source -> source.getBool(SettingsKey.NGINX_SSL_SESSION_CACHE_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_SSL_SESSION_CACHE_ENABLE, fieldvalue));
		this.binder.forField(this.nginxSslSessionTimeoutEnable)
			.bind(source -> source.getBool(SettingsKey.NGINX_SSL_SESSION_TIMEOUT_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_SSL_SESSION_TIMEOUT_ENABLE, fieldvalue));
		this.binder.forField(this.nginxSslPreferServerCiphersEnable)
			.bind(source -> source.getBool(SettingsKey.NGINX_SSL_PREFER_SERVER_CIPHERS_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_SSL_PREFER_SERVER_CIPHERS_ENABLE, fieldvalue));
		this.binder.forField(this.nginxSslStaplingEnable)
			.bind(source -> source.getBool(SettingsKey.NGINX_SSL_STAPLING_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_SSL_STAPLING_ENABLE, fieldvalue));
		this.binder.forField(this.nginxSslStaplingVerifyEnable)
			.bind(source -> source.getBool(SettingsKey.NGINX_SSL_STAPLING_VERIFY_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_SSL_STAPLING_VERIFY_ENABLE, fieldvalue));
		this.binder.forField(this.nginxSslEcdhCurveEnable)
			.bind(source -> source.getBool(SettingsKey.NGINX_SSL_ECDH_CURVE_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_SSL_ECDH_CURVE_ENABLE, fieldvalue));
		this.binder.forField(this.nginxHeaderFrameOptionEnable)
			.bind(source -> source.getBool(SettingsKey.NGINX_HEADER_FRAME_OPTION_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_HEADER_FRAME_OPTION_ENABLE, fieldvalue));
		this.binder.forField(this.nginxHeaderContentTypeOptionsEnable)
			.bind(source -> source.getBool(SettingsKey.NGINX_HEADER_CONTENT_TYPE_OPTIONS_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_HEADER_CONTENT_TYPE_OPTIONS_ENABLE, fieldvalue));
		this.binder.forField(this.nginxHeaderXssProtectionEnable)
			.bind(source -> source.getBool(SettingsKey.NGINX_HEADER_XSS_PROTECTION_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_HEADER_XSS_PROTECTION_ENABLE, fieldvalue));
		this.binder.forField(this.nginxSslTrustedCertificateEnable)
			.bind(source -> source.getBool(SettingsKey.NGINX_SSL_TRUSTED_CERTIFICATE_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.NGINX_SSL_TRUSTED_CERTIFICATE_ENABLE, fieldvalue));
		/* @formatter:on */

		this.nginxResolverIpv4.setValidateCallback(value -> InetAddressValidator.getInstance().isValidInet4Address(value));
		this.nginxResolverIpv6.setValidateCallback(value -> InetAddressValidator.getInstance().isValidInet6Address(value));

		this.buttons.getSave().addClickListener(event -> {
			if (this.binder.isValid()) {
				this.nginxResolverIpv4.selectAll();
				this.nginxResolverIpv6.selectAll();
				this.settingsFacade.save(this.binder.getBean());
				Notification.show(this.msg.getMessage("settings.button.save.success"), Type.HUMANIZED_MESSAGE);
			} else {
				this.binder.validate();
			}
		});

		final FormLayout form = new FormLayout();
		form.addComponents(this.nginxHeaderFrameOption, this.nginxHeaderContentTypeOptions, this.nginxHeaderXssProtection);
		form.addComponent(new HorizontalLine());
		form.addComponents(this.nginxSslSessionCache, this.nginxSslSessionTimeout, this.nginxSslProtocols);
		form.addComponents(this.nginxSslCiphers, this.nginxSslPreferServerCiphers);
		form.addComponents(this.nginxSslStapling, this.nginxSslStaplingVerify);
		form.addComponents(this.nginxSslEcdhCurve);
		form.addComponents(this.nginxResolverTimeout, this.nginxResolverIpv4, this.nginxResolverIpv6);
		form.addComponent(this.buttons);
		form.setSizeFull();

		final VerticalLayout enables = new VerticalLayout(this.enableTitle);
		enables.addComponents(this.nginxHeaderFrameOptionEnable, this.nginxHeaderContentTypeOptionsEnable, this.nginxHeaderXssProtectionEnable);
		enables.addComponents(this.nginxSslTrustedCertificateEnable, this.nginxSslSessionCacheEnable, this.nginxSslSessionTimeoutEnable, this.nginxSslPreferServerCiphersEnable);
		enables.addComponents(this.nginxSslStaplingEnable, this.nginxSslStaplingVerifyEnable, this.nginxSslEcdhCurveEnable);
		enables.addComponents(this.nginxResolverTimeoutEnable, this.nginxResolverIpv4Enable, this.nginxResolverIpv6Enable);
		enables.setSizeFull();

		this.addComponents(form, enables);
		this.setSizeFull();
		this.setMargin(true);
	}

	@Override
	public String getTabCaption() {
		return this.msg.getMessage("settings.tab.name.nginx");
	}

	@Override
	public void enter() {
		log.trace("SettingsTabWebserverNginx::enter");

		final Settings global = this.settingsFacade.getGlobal();
		this.binder.setBean(global);
	}

	private void enabledFields(final Boolean enabled) {
		this.nginxResolverIpv4.setEnabled(enabled);
		this.nginxResolverIpv6.setEnabled(enabled);
		this.nginxResolverTimeout.setEnabled(enabled);
		this.nginxSslSessionCache.setEnabled(enabled);
		this.nginxSslSessionTimeout.setEnabled(enabled);
		this.nginxSslPreferServerCiphers.setEnabled(enabled);
		this.nginxSslStapling.setEnabled(enabled);
		this.nginxSslStaplingVerify.setEnabled(enabled);
		this.nginxSslEcdhCurve.setEnabled(enabled);
		this.nginxHeaderFrameOption.setEnabled(enabled);
		this.nginxHeaderContentTypeOptions.setEnabled(enabled);
		this.nginxHeaderXssProtection.setEnabled(enabled);
	}
}