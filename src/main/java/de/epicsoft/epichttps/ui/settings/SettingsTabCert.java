/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.settings;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.facade.SettingsFacade;
import de.epicsoft.epichttps.settings.ConsistencyValidation;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.ui.common.SaveCancelButtons;
import de.epicsoft.epichttps.ui.settings.components.EpicTextField;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@SpringComponent("settingsTabCert")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SettingsTabCert extends FormLayout implements SettingsTab {

	private static final long serialVersionUID = 658335357954620275L;

	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	private transient ConsistencyValidation consistencyValidation;
	@Autowired
	protected transient SettingsFacade settingsFacade;

	@Autowired
	protected SaveCancelButtons buttons;

	private final TextField certRenewBeforeExpiryDays = new EpicTextField();
	private final CheckBox privateKeyRotationEnable = new CheckBox();
	private final TextField privateKeyQuantity = new EpicTextField();
	private final TextField privateKeyPeriodValidityDays = new EpicTextField();
	private final CheckBox backupKeyEnable = new CheckBox();
	private final TextField backupKeyQuantity = new EpicTextField();
	private final TextField dhParamBit = new EpicTextField();
	protected final BeanValidationBinder<Settings> binder = new BeanValidationBinder<>(Settings.class);

	@PostConstruct
	public void init() {
		log.trace("SettingsTabCert::init");

		final Settings env = this.settingsFacade.getEnv();
		final Integer keysQuantityMin = env.getInt(SettingsKey.KEYS_QUANTITY_MIN);
		final Integer keysQuantityMax = env.getInt(SettingsKey.KEYS_QUANTITY_MAX);
		final Integer keysPeriodDaysMin = env.getInt(SettingsKey.KEYS_PERIOD_DAYS_MIN);
		final Integer keysPeriodDaysMax = env.getInt(SettingsKey.KEYS_PERIOD_DAYS_MAX);
		final Integer certRenewDaysMin = env.getInt(SettingsKey.CERT_RENEW_DAYS_MIN);
		final Integer certRenewDaysMax = env.getInt(SettingsKey.CERT_RENEW_DAYS_MAX);
		final Integer dhParamBitMin = env.getInt(SettingsKey.DH_PARAM_BIT_MIN);

		this.enabledPrivateRotationKey(false);
		this.enabledBackupKey(false);

		this.certRenewBeforeExpiryDays.setCaptionAsHtml(true);
		this.certRenewBeforeExpiryDays
				.setCaption(String.format("%s %s", this.msg.getMessage("settings.field.name.cert.renew.before.expiry.days"), VaadinIcons.QUESTION_CIRCLE.getHtml()));
		this.certRenewBeforeExpiryDays.setDescription(this.msg.getMessage("help.cert.renew.before.expiry.days"));
		this.privateKeyRotationEnable.setCaption(this.msg.getMessage("settings.field.name.private.key.rotation.enable"));
		this.privateKeyQuantity.setCaption(this.msg.getMessage("settings.field.name.private.key.quantity"));
		this.privateKeyPeriodValidityDays.setCaption(this.msg.getMessage("settings.field.name.private.key.period.validity.days"));
		this.backupKeyEnable.setCaption(this.msg.getMessage("settings.field.name.backup.key.enable"));
		this.backupKeyQuantity.setCaption(this.msg.getMessage("settings.field.name.backup.key.quantity"));
		this.dhParamBit.setCaptionAsHtml(true);
		this.dhParamBit.setCaption(String.format("%s %s", this.msg.getMessage("settings.field.name.dh.param.bit"), VaadinIcons.QUESTION_CIRCLE.getHtml()));
		this.dhParamBit.setDescription(this.msg.getMessage("help.dh.param"));

		this.privateKeyRotationEnable.addValueChangeListener(event -> this.enabledPrivateRotationKey(event.getValue()));
		this.backupKeyEnable.addValueChangeListener(event -> this.enabledBackupKey(event.getValue()));

		/* @formatter:off */
		this.binder.forField(this.certRenewBeforeExpiryDays)
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.text.empty"))
			.withConverter(new StringToIntegerConverter(this.msg.getMessage("validator.text.numeric")))
			.withValidator(value -> Integer.valueOf(value) >= certRenewDaysMin, this.msg.getMessage("validator.cert.renew.short", certRenewDaysMin))
			.withValidator(value -> Integer.valueOf(value) <= certRenewDaysMax, this.msg.getMessage("validator.cert.renew.large", certRenewDaysMax))
			.bind(source -> source.getInt(SettingsKey.CERT_RENEW_BEFORE_EXPIRY_DAYS), (bean, fieldvalue) -> bean.set(SettingsKey.CERT_RENEW_BEFORE_EXPIRY_DAYS, fieldvalue));
		this.binder.forField(this.privateKeyRotationEnable)
			.bind(source -> source.getBool(SettingsKey.PRIVATE_KEY_ROTATION_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.PRIVATE_KEY_ROTATION_ENABLE, fieldvalue));
		this.binder.forField(this.privateKeyQuantity)
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.text.empty"))
			.withConverter(new StringToIntegerConverter(this.msg.getMessage("validator.text.numeric")))
			.withValidator(value -> Integer.valueOf(value) >= keysQuantityMin, this.msg.getMessage("validator.key.quantity.short", keysQuantityMin))
			.withValidator(value -> Integer.valueOf(value) <= keysQuantityMax, this.msg.getMessage("validator.key.quantity.large", keysQuantityMax))
			.bind(source -> source.getInt(SettingsKey.PRIVATE_KEY_QUANTITY), (bean, fieldvalue) -> bean.set(SettingsKey.PRIVATE_KEY_QUANTITY, fieldvalue));
		this.binder.forField(this.privateKeyPeriodValidityDays)
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.text.empty"))
			.withConverter(new StringToIntegerConverter(this.msg.getMessage("validator.text.numeric")))
			.withValidator(value -> Integer.valueOf(value) >= keysPeriodDaysMin, this.msg.getMessage("validator.key.poriod.short", keysPeriodDaysMin))
			.withValidator(value -> Integer.valueOf(value) <= keysPeriodDaysMax, this.msg.getMessage("validator.key.poriod.large", keysPeriodDaysMax))
			.bind(source -> source.getInt(SettingsKey.PRIVATE_KEY_PERIOD_VALIDITY_DAYS), (bean, fieldvalue) -> bean.set(SettingsKey.PRIVATE_KEY_PERIOD_VALIDITY_DAYS, fieldvalue));
		this.binder.forField(this.backupKeyEnable)
			.bind(source -> source.getBool(SettingsKey.BACKUP_KEY_ENABLE), (bean, fieldvalue) -> bean.set(SettingsKey.BACKUP_KEY_ENABLE, fieldvalue));
		this.binder.forField(this.backupKeyQuantity)
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.text.empty"))
			.withConverter(new StringToIntegerConverter(this.msg.getMessage("validator.text.numeric")))
			.withValidator(value -> Integer.valueOf(value) >= keysQuantityMin, this.msg.getMessage("validator.key.quantity.short", keysQuantityMin))
			.withValidator(value -> Integer.valueOf(value) <= keysQuantityMax, this.msg.getMessage("validator.key.quantity.large", keysQuantityMax))
			.bind(source -> source.getInt(SettingsKey.BACKUP_KEY_QUANTITY), (bean, fieldvalue) -> bean.set(SettingsKey.BACKUP_KEY_QUANTITY, fieldvalue));
		this.binder.forField(this.dhParamBit)
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.text.empty"))
			.withConverter(new StringToIntegerConverter(this.msg.getMessage("validator.text.numeric")))
			.withValidator(value -> Integer.valueOf(value) >= dhParamBitMin, this.msg.getMessage("validator.dh.param.small", dhParamBitMin))
			.bind(source -> source.getInt(SettingsKey.DH_PARAM_BIT), (bean, fieldvalue) -> bean.set(SettingsKey.DH_PARAM_BIT, fieldvalue));
		this.binder.withValidator(this.consistencyValidation.buildValidator());
		/* @formatter:on */

		this.buttons.getSave().addClickListener(event -> {
			if (this.binder.isValid()) {
				this.settingsFacade.save(this.binder.getBean());
				Notification.show(this.msg.getMessage("settings.button.save.success"), Type.HUMANIZED_MESSAGE);
			} else {
				this.showError(this.binder.validate());
			}
		});

		this.addComponents(this.certRenewBeforeExpiryDays);
		this.addComponents(this.privateKeyRotationEnable, this.privateKeyQuantity, this.privateKeyPeriodValidityDays);
		this.addComponents(this.backupKeyEnable, this.backupKeyQuantity, this.dhParamBit);
		this.addComponent(this.buttons);
		this.setSizeFull();
		this.setMargin(true);
	}

	@Override
	public void enter() {
		log.trace("SettingsTabCert::enterGlobalSettings");

		final Settings global = this.settingsFacade.getGlobal();
		this.binder.setBean(global);
	}

	private void enabledPrivateRotationKey(final Boolean enabled) {
		this.privateKeyQuantity.setEnabled(enabled);
	}

	private void enabledBackupKey(final Boolean enabled) {
		this.backupKeyQuantity.setEnabled(enabled);
	}
}