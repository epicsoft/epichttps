/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.le;

import javax.annotation.PostConstruct;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Link;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.facade.LetsEncryptFacade;
import de.epicsoft.epichttps.facade.SettingsFacade;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.ui.common.EpicValidationResult;
import de.epicsoft.epichttps.ui.common.EpicView;
import de.epicsoft.epichttps.ui.common.SaveCancelButtons;
import de.epicsoft.epichttps.ui.common.ViewHelper;
import de.epicsoft.epichttps.ui.overview.OverviewView;
import de.epicsoft.epichttps.ui.settings.components.EpicTextField;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@UIScope
@SpringComponent
@SpringView(name = LetsEncryptView.NAME)
public class LetsEncryptView extends VerticalLayout implements EpicView {

	private static final long serialVersionUID = 7173726074014910397L;

	public static final String NAME = "letsencryt";

	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	private transient LetsEncryptFacade letsEncryptFacade;
	@Autowired
	private transient ViewHelper viewHelper;
	@Autowired
	private SettingsFacade settingsFacade;

	@Autowired
	private SaveCancelButtons buttons;

	private final BeanValidationBinder<Settings> binder = new BeanValidationBinder<>(Settings.class);

	private final TextField leEmail = new EpicTextField();
	private final CheckBox withoutEmail = new CheckBox();
	private final CheckBox agreeTos = new CheckBox();
	private final Link tosLink = new Link();

	@PostConstruct
	public void init() {
		log.trace("LetsEncryptView::init");

		final Settings env = this.settingsFacade.getEnv();

		this.withoutEmail.setCaptionAsHtml(true);

		this.leEmail.setCaption(this.msg.getMessage("le.account.email"));
		this.withoutEmail.setCaption(this.msg.getMessage("le.account.without.email"));
		this.agreeTos.setCaption(this.msg.getMessage("le.account.agree.tos"));
		this.tosLink.setCaption(this.msg.getMessage("le.account.tos"));

		this.leEmail.setEnabled(true);
		this.withoutEmail.addValueChangeListener(event -> this.leEmail.setEnabled(!event.getValue()));
		this.withoutEmail.addValueChangeListener(event -> this.binder.validate());
		this.tosLink.setResource(new ExternalResource(env.get(SettingsKey.LE_TOS_LINK)));
		this.tosLink.setTargetName("_blank");

		/* @formatter:off */
		this.binder.forField(this.leEmail)
			.withValidator(value -> this.withoutEmail.getValue() || StringUtils.hasText(value), this.msg.getMessage("validator.email.empty"))
			.withValidator((value, context) -> {
				final boolean valid = this.withoutEmail.getValue() || EmailValidator.getInstance().isValid(value);
				return valid ? EpicValidationResult.ok(): EpicValidationResult.error(this.msg.getMessage("validator.email.invalid")) ;
			})
			.bind(source -> source.get(SettingsKey.LE_ACCOUNT_EMAIL), (bean, fieldvalue) -> bean.set(SettingsKey.LE_ACCOUNT_EMAIL, fieldvalue));
		this.binder.forField(this.withoutEmail)
			.bind(source -> source.getBool(SettingsKey.LE_ACCOUNT_WITHOUT_EMAIL), (bean, fieldvalue) -> bean.set(SettingsKey.LE_ACCOUNT_WITHOUT_EMAIL, fieldvalue));
		this.binder.forField(this.agreeTos)
			.withValidator(value -> value, this.msg.getMessage("validator.le.account.tos.not.accepted"))
			.bind(source -> source.getBool(SettingsKey.LE_ACCOUNT_AGREE_TOS), (bean, fieldvalue) -> bean.set(SettingsKey.LE_ACCOUNT_AGREE_TOS, fieldvalue));
		/* @formatter:on */

		this.buttons.getSave().addClickListener(event -> {
			if (this.binder.isValid()) {
				this.letsEncryptFacade.createAccount(this.binder.getBean());
				UI.getCurrent().getNavigator().navigateTo(OverviewView.NAME);
			} else {
				this.binder.validate();
			}
		});

		final FormLayout form = new FormLayout();
		form.setMargin(false);
		form.addComponents(this.leEmail, this.withoutEmail, this.tosLink, this.agreeTos, this.buttons);

		final Layout content = this.viewHelper.buildContent();
		content.addComponent(form);

		this.addComponent(this.viewHelper.buildHeader(this.msg.getMessage("le.account.register")));
		this.addComponent(content);
		this.setSpacing(true);
		this.setMargin(false);
	}

	@Override
	public void enter(final ViewChangeEvent event) {
		log.trace("LetsEncryptView::enter");

		final Settings global = this.settingsFacade.getGlobal();
		this.binder.setBean(global);
	}
}
