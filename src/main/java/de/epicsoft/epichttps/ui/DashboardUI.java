/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;

import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.ui.Transport;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import de.epicsoft.epichttps.EpicHttpsApplication;
import de.epicsoft.epichttps.common.event.AfterViewChangeEvent;
import de.epicsoft.epichttps.ui.common.AccessDeniedView;
import de.epicsoft.epichttps.ui.common.Resource;
import de.epicsoft.epichttps.ui.overview.OverviewView;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@UIScope
@Push(transport = Transport.WEBSOCKET_XHR)
@Title(DashboardUI.TITLE)
@Theme(ValoTheme.THEME_NAME)
@SpringUI(path = Resource.DASHBOARD)
public class DashboardUI extends UI {

	private static final long serialVersionUID = -2826217936350251524L;

	public static final String TITLE = EpicHttpsApplication.NAME;

	@Autowired
	private transient ApplicationContext context;
	@Autowired
	private transient SpringViewProvider viewProvider;
	@Autowired
	private transient ApplicationEventPublisher publisher;

	@Override
	protected void init(final VaadinRequest request) {
		log.debug("DashboardUI::init");

		final Content content = new Content();
		final Navigator navigator = this.buildNavigator(content);

		this.setNavigator(navigator);
		this.setContent(this.buildMainContent(content));

		UI.getCurrent().setErrorHandler(event -> {
			final Throwable e = event.getThrowable();
			log.error("UI::{}::{}::{}", this.getClass().getSimpleName(), e.getClass().getSimpleName(), e.getMessage(), e);
			Page.getCurrent().setLocation(Resource.LOGIN);
		});

		VaadinSession.getCurrent().setErrorHandler(event -> {
			final Throwable e = event.getThrowable();
			log.error("Session::{}::{}::{}", this.getClass().getSimpleName(), e.getClass().getSimpleName(), e.getMessage(), e);
			Page.getCurrent().setLocation(Resource.LOGIN);
		});

		navigator.navigateTo(OverviewView.NAME);
	}

	private Component buildMainContent(final Content content) {

		final Navigation navigation = this.context.getBean(Navigation.class);
		navigation.setWidth(250, Unit.PIXELS);
		navigation.setSpacing(false);
		navigation.setMargin(false);

		// Main Window
		final VerticalLayout right = new VerticalLayout();
		right.setSizeFull();
		right.setMargin(false);
		right.setSpacing(false);
		right.addComponent(content);
		right.setExpandRatio(content, 1.0f);
		right.setComponentAlignment(content, Alignment.TOP_LEFT);

		// Navigation / Right
		final HorizontalLayout nav = new HorizontalLayout();
		nav.setMargin(false);
		nav.setSpacing(false);
		nav.setSizeFull();
		nav.addComponent(navigation);
		nav.setExpandRatio(navigation, 0.0f);
		nav.setComponentAlignment(navigation, Alignment.TOP_LEFT);
		nav.addComponent(right);
		nav.setExpandRatio(right, 1.0f);
		nav.setComponentAlignment(right, Alignment.TOP_LEFT);

		// Main
		final VerticalLayout main = new VerticalLayout();
		main.setHeightUndefined();
		main.setWidth(100, Unit.PERCENTAGE);
		main.addComponent(nav);
		main.setExpandRatio(nav, 1.0f);
		main.setComponentAlignment(nav, Alignment.TOP_LEFT);
		main.setMargin(false);
		main.setSpacing(false);

		return main;
	}

	private Navigator buildNavigator(final Content content) {
		final Navigator nav = new Navigator(UI.getCurrent(), content);
		nav.addProvider(this.viewProvider);
		nav.setErrorView(AccessDeniedView.class);
		nav.addViewChangeListener(new ViewChangeListener() {

			private static final long serialVersionUID = 7163793214689053525L;

			@Override
			public boolean beforeViewChange(final ViewChangeEvent event) {
				return true;
			}

			@Override
			public void afterViewChange(final ViewChangeEvent event) {
				DashboardUI.this.publisher.publishEvent(new AfterViewChangeEvent(event.getViewName(), event.getParameters()));
			}
		});
		return nav;
	}
}