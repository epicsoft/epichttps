/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.about;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.ui.common.EpicView;
import de.epicsoft.epichttps.ui.common.HorizontalLine;
import de.epicsoft.epichttps.ui.common.ViewHelper;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@UIScope
@SpringComponent
@SpringView(name = AboutView.NAME)
public class AboutView extends VerticalLayout implements EpicView {

	private static final long serialVersionUID = -7185982382014249632L;

	public static final String NAME = "about";

	@Value("${info.build.version}")
	private String buildVersion;

	@Autowired
	private transient EpicMessageSource msg;

	@Autowired
	private ViewHelper viewHelper;

	@PostConstruct
	public void init() {
		log.trace("AboutView::init");

		this.setSpacing(true);
		this.setMargin(false);
		this.addComponent(this.viewHelper.buildHeader(this.msg.getMessage("title.main")));
		this.addComponent(this.buildContent());
	}

	@Override
	public void enter(final ViewChangeEvent event) {
		log.trace("AboutView::enter");
	}

	private Component buildContent() {
		// Title
		final Label title = new Label(String.format("%s - %s", this.msg.getMessage("title.main"), this.msg.getMessage("title.sub")));
		title.addStyleName(ValoTheme.LABEL_BOLD);
		// Version
		final Label version = new Label(String.format("%s: %s", this.msg.getMessage("about.version"), this.buildVersion));
		// Links - epicsoft / Docker Hub / Bitbucket / ...
		final Link epicsoftDe = new Link(this.msg.getMessage("link.epicsoft.de"), new ExternalResource(this.msg.getMessage("link.epicsoft.de")));
		epicsoftDe.setTargetName("_blank");
		final Link dockerHubLink = new Link(this.msg.getMessage("link.docker.hub"), new ExternalResource(this.msg.getMessage("link.docker.hub")));
		dockerHubLink.setTargetName("_blank");
		final Link ticketsLink = new Link(this.msg.getMessage("link.tickets"), new ExternalResource(this.msg.getMessage("link.tickets")));
		ticketsLink.setTargetName("_blank");
		final Link bitbucketLink = new Link(this.msg.getMessage("link.source"), new ExternalResource(this.msg.getMessage("link.source")));
		bitbucketLink.setTargetName("_blank");
		final Link dockerfileLink = new Link(this.msg.getMessage("link.source.docker"), new ExternalResource(this.msg.getMessage("link.source.docker")));
		bitbucketLink.setTargetName("_blank");

		final VerticalLayout content = this.viewHelper.buildContent();
		content.addComponents(new HorizontalLayout(title, epicsoftDe));
		content.addComponent(version);
		content.addComponent(new Label(this.msg.getMessage("about.licence")));
		content.addComponent(new Label(this.msg.getMessage("about.copyright")));
		content.addComponent(new Label(this.msg.getMessage("about.autor")));
		content.addComponent(new HorizontalLine());
		content.addComponent(new HorizontalLayout(new Label(this.msg.getMessage("about.docker.hub")), dockerHubLink));
		content.addComponent(new HorizontalLayout(new Label(this.msg.getMessage("about.tickets")), ticketsLink));
		content.addComponent(new HorizontalLayout(new Label(this.msg.getMessage("about.source")), bitbucketLink));
		content.addComponent(new HorizontalLayout(new Label(this.msg.getMessage("about.dockerfile")), dockerfileLink));

		return content;
	}
}
