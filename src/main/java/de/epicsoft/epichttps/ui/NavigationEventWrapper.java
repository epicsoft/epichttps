/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui;

import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.vaadin.ui.UI;

import de.epicsoft.epichttps.common.event.DomainCreatedEvent;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Vaadin ist nicht immer vorhanden, deswegen kann eine Bean mit vaadin-ui Scope nicht immer direkt angesteuert werden. Hierfuer ist der Wrapper, der zuerst prueft, ob eine
 * Vaadin-UI gerade laeuft. <br>
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class NavigationEventWrapper {

	@NonNull
	private final ApplicationContext context;

	@EventListener(DomainCreatedEvent.class)
	public void updateDomainButtons() {
		if (UI.getCurrent() != null) {
			final Navigation nav = this.context.getBean(Navigation.class);
			if (nav != null) {
				nav.updateDomainButtons();
			} else {
				log.error("de.epicsoft.epichttps.ui.Navigation IS NULL");
			}
		}
	}
}