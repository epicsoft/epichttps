/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.domain;

import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import de.epicsoft.epichttps.certificate.Certificate;
import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.common.exception.EpicIllegalArgumentException;
import de.epicsoft.epichttps.domain.Domain;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.facade.BuildFacade;
import de.epicsoft.epichttps.facade.BuildInfo;
import de.epicsoft.epichttps.facade.DomainFacade;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.ui.common.EpicView;
import de.epicsoft.epichttps.ui.common.ViewHelper;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@UIScope
@SpringComponent
@SpringView(name = DomainEditView.NAME)
public class DomainEditView extends VerticalLayout implements EpicView {

	private static final long serialVersionUID = -8120907637291626896L;

	public static final String NAME = "domainedit";

	@Autowired
	private transient DomainFacade domainFacade;
	@Autowired
	private transient BuildFacade buildFacade;
	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	private transient DateTimeFormatter dateTimeFormatter;

	@Autowired
	private ViewHelper viewHelper;

	private final Label title = new Label();
	private final VerticalLayout left = new VerticalLayout();
	private final VerticalLayout right = new VerticalLayout();
	private final HorizontalLayout content = new HorizontalLayout();
	private final VerticalLayout warnings = new VerticalLayout();

	@PostConstruct
	public void init() {
		log.trace("DomainEditView::init");

		this.right.setSpacing(false);
		this.right.setMargin(false);
		this.left.setSpacing(false);
		this.left.setMargin(false);
		this.warnings.setMargin(false);
		this.warnings.setSpacing(true);
		this.content.setSpacing(true);
		this.content.setMargin(false);

		final VerticalLayout contentWrapper = this.viewHelper.buildContent();
		contentWrapper.addComponent(this.content);
		contentWrapper.addStyleName(ValoTheme.LAYOUT_CARD);
		contentWrapper.setWidth(100, Unit.PERCENTAGE);

		this.addComponent(this.viewHelper.buildHeader(this.title));
		this.addComponent(contentWrapper);
		this.setSpacing(true);
		this.setMargin(false);
	}

	@Override
	public void enter(final ViewChangeEvent event) {
		log.trace("DomainEditView::enter");

		final String id = event.getParameters();
		final DomainInfo info = this.domainFacade.getDomainInfoById(id);
		this.buildContent(this.domainFacade.getDomainByDomainInfo(info));
	}

	private void buildContent(final Domain domain) {
		this.left.removeAllComponents();
		this.right.removeAllComponents();
		this.warnings.removeAllComponents();

		this.title.setValue(domain.getInfo().getName());
		this.left.addComponent(this.buildCertificate(domain));
		this.right.addComponent(this.buildKeys(domain));

		final List<Component> warningComponents = this.viewHelper.buildDomainWarnings(domain);
		this.warnings.setVisible(warningComponents.size() > 0);
		this.warnings.addComponents(warningComponents.toArray(new Component[warningComponents.size()]));

		final VerticalLayout wrapper = new VerticalLayout();
		wrapper.setMargin(false);
		wrapper.setSpacing(true);
		wrapper.addComponent(this.buildButtons(domain));
		wrapper.addComponent(this.warnings);
		wrapper.addComponent(new HorizontalLayout(this.left, this.right));

		this.content.removeAllComponents();
		this.content.addComponents(wrapper);
	}

	private Component buildButtons(final Domain domain) {
		final Button settings = new Button(this.msg.getMessage("domain.settings"));
		settings.addStyleName(ValoTheme.BUTTON_TINY);
		settings.addClickListener(event -> UI.getCurrent().getNavigator().navigateTo(String.format("%s/%s", DomainSettingsView.NAME, domain.getInfo().getId())));

		final Button build = new Button(this.msg.getMessage("domain.build"));
		build.setDescription(this.msg.getMessage("help.domain.build"));
		build.addStyleName(ValoTheme.BUTTON_TINY);
		build.setEnabled(domain.isActive());
		build.addClickListener(event -> {
			build.setEnabled(false);
			UI.getCurrent().access(() -> {
				/*
				 * Workaround fuer 'ui.access' mit einen zusaetzlichen Thread, da der regulaere Weg die UI blockiert. Der zweite Thread laeuft so wie erwartet, asynchron zur UI und gibt
				 * die Meldungen an die UI zurueck, sobald der Thread abgeschlossen ist.
				 */
				final UI ui = UI.getCurrent();
				new Thread(() -> {
					UI.setCurrent(ui);
					try {
						log.trace("Build: starting for domain '{}'", domain.getInfo().getName());
						final BuildInfo buildInfo = this.buildFacade.build(domain.getInfo());
						if (buildInfo.isBuildNeeded()) {
							UI.getCurrent().getSession().lock();
							log.info("Build: success for domain '{}'", domain.getInfo().getName());
							this.buildContent(this.domainFacade.getDomainByDomainInfo(domain.getInfo()));
							Notification.show(this.msg.getMessage("domain.build.success"));
							UI.getCurrent().getSession().unlock();
						} else {
							log.debug("Build: not requred for domain '{}'", domain.getInfo().getName());
							Notification.show(this.msg.getMessage("domain.build.not.required"));
						}
					} catch (final EpicIllegalArgumentException e1) {
						log.error("Build: failure for domain '{}'", domain.getInfo().getName(), e1);
						Notification.show(this.msg.getMessage("domain.build.error", e1.getMessage()), Type.ERROR_MESSAGE);
					} catch (final Exception e2) {
						log.error("Build: unhandled error for domain '{}'", domain.getInfo().getName(), e2);
						Notification.show(this.msg.getMessage("domain.build.error", "unhandled error, see log"), Type.ERROR_MESSAGE);
					} finally {
						build.setEnabled(true);
						UI.getCurrent().push();
						if (UI.getCurrent().getSession().hasLock()) {
							UI.getCurrent().getSession().unlock();
						}
					}
				}).start();
			});
		});

		final HorizontalLayout buttons = new HorizontalLayout(build, settings);
		buttons.setSpacing(true);
		buttons.setMargin(false);

		return buttons;
	}

	private Component buildKeys(final Domain domain) {
		final Label privateKeys = new Label();
		privateKeys.setCaption(this.msg.getMessage("domain.private.keys"));
		privateKeys.setValue(String.valueOf(domain.getPrivateKeys().size()));

		final Label backupKeys = new Label();
		backupKeys.setCaption(this.msg.getMessage("domain.backup.keys"));
		backupKeys.setValue(String.valueOf(domain.getBackupKeys().size()));

		final Label dhParam = new Label();
		dhParam.setCaption(this.msg.getMessage("domain.dhparam"));
		dhParam.setValue(domain.getDhParam() != null ? String.format("%d bit", domain.getDhParam().getBit()) : "-");

		final FormLayout keysContent = new FormLayout();
		keysContent.addComponents(privateKeys, backupKeys, dhParam);
		keysContent.setMargin(new MarginInfo(false, true, false, true));
		keysContent.setSpacing(false);

		final Panel keys = new Panel();
		keys.setCaption(this.msg.getMessage("domain.keys"));
		keys.setContent(keysContent);
		return keys;
	}

	private Component buildCertificate(final Domain domain) {
		final Label issuer = new Label();
		issuer.setCaption(this.msg.getMessage("domain.certificate.issuer"));
		issuer.setValue("-");

		final Label validFrom = new Label();
		validFrom.setCaption(this.msg.getMessage("domain.certificate.valid.from"));
		validFrom.setValue("-");

		final Label validUntil = new Label();
		validUntil.setCaption(this.msg.getMessage("domain.certificate.valid.to"));
		validUntil.setValue("-");

		final Label keyAlgorithm = new Label();
		keyAlgorithm.setCaption(this.msg.getMessage("domain.certificate.key.algorithm"));
		keyAlgorithm.setValue("-");

		final Label signatureAlgorithm = new Label();
		signatureAlgorithm.setCaption(this.msg.getMessage("domain.certificate.signature.algorithm"));
		signatureAlgorithm.setValue("-");

		final Label alternativeNames = new Label();
		alternativeNames.setCaption(this.msg.getMessage("domain.certificate.alternative.names"));
		alternativeNames.setValue("-");
		alternativeNames.setContentMode(ContentMode.HTML);

		final Label pin = new Label();
		pin.setCaption(this.msg.getMessage("domain.certificate.pin"));
		pin.setValue("-");
		pin.setVisible(domain.getSettings().getBool(SettingsKey.PRIVATE_KEY_ROTATION_ENABLE));

		final FormLayout certContent = new FormLayout();
		certContent.addComponents(issuer, validFrom, validUntil, keyAlgorithm, signatureAlgorithm, pin, alternativeNames);
		certContent.setMargin(new MarginInfo(false, true, false, true));
		certContent.setSpacing(false);

		final Panel certificate = new Panel();
		certificate.setCaption(this.msg.getMessage("domain.certificate"));
		certificate.setContent(certContent);

		final Certificate cert = domain.getCurrentCertificate();
		if (cert != null) {
			issuer.setValue(cert.getIssuer());
			validFrom.setValue(cert.getValidFrom().format(this.dateTimeFormatter));
			validUntil.setValue(cert.getValidUntil().format(this.dateTimeFormatter));
			keyAlgorithm.setValue(cert.getKeyAlgorithm());
			signatureAlgorithm.setValue(cert.getSignatureAlgorithm());
			pin.setValue(cert.getPin());
			alternativeNames.setValue(String.join("<br>", cert.getAlternativeNames()));
		}

		return certificate;
	}
}