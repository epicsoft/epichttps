/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.domain;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.UI;

import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.ui.settings.SettingsTabHsts;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DomainSettingsTabHsts extends SettingsTabHsts implements DomainSettingsTab {

	private static final long serialVersionUID = -5474645523209922646L;

	private String domainEditView;

	@PostConstruct
	@Override
	public void init() {
		super.init();
		this.buttons.getCancel().addClickListener(this::cancelEdit);
	}

	@Override
	public void enter(final DomainInfo domainInfo) {
		log.trace("DomainSettingsTabHsts::enter");

		final Settings domainSettings = this.settingsFacade.getDomain(domainInfo);
		this.binder.setBean(domainSettings);

		this.domainEditView = String.format("%s/%s", DomainEditView.NAME, domainInfo.getId());
	}

	@Override
	public void enter() {
		throw new RuntimeException("Method 'enter' not supported use enter(DomainInfo)");
	}

	private void cancelEdit(@SuppressWarnings("unused") final ClickEvent event) {
		UI.getCurrent().getNavigator().navigateTo(this.domainEditView);
	}
}