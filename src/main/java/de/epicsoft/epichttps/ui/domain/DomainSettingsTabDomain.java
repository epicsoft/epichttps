/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.domain;

import javax.annotation.PostConstruct;

import org.apache.commons.validator.routines.DomainValidator;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.facade.DomainFacade;
import de.epicsoft.epichttps.facade.SettingsFacade;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.ui.common.SaveCancelButtons;
import de.epicsoft.epichttps.ui.settings.SettingsTab;
import de.epicsoft.epichttps.ui.settings.components.EpicListSelect;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DomainSettingsTabDomain extends FormLayout implements SettingsTab, DomainSettingsTab {

  private static final long serialVersionUID = -6942848820831075437L;

  @Autowired
  private transient EpicMessageSource msg;

  @Autowired
  private transient SettingsFacade settingsFacade;

  @Autowired
  private transient DomainFacade domainFacade;

  @Autowired
  private SaveCancelButtons buttons;

  @Autowired
  private EpicListSelect ipv4;

  @Autowired
  private EpicListSelect ipv6;

  @Autowired
  private EpicListSelect subDomains;

  private final CheckBox active = new CheckBox();

  private final TextField httpPort = new TextField();

  private final TextField httpsPort = new TextField();

  protected final BeanValidationBinder<Settings> binder = new BeanValidationBinder<>(Settings.class);

  private String domainEditView;

  @PostConstruct
  public void init() {
    log.trace("DomainSettingsTabDomaion::init");

    this.buttons.getCancel().addClickListener(this::cancelEdit);

    this.ipv4.setCaption(this.msg.getMessage("settings.field.name.domain.ipv4"));
    this.ipv6.setCaption(this.msg.getMessage("settings.field.name.domain.ipv6"));
    this.subDomains.setCaption(this.msg.getMessage("settings.field.name.domain.subdomains"));
    this.active.setCaption(this.msg.getMessage("settings.field.name.domain.active"));
    this.httpPort.setCaption(this.msg.getMessage("settings.field.name.domain.port.http"));
    this.httpsPort.setCaption(this.msg.getMessage("settings.field.name.domain.port.https"));

    this.subDomains.setListHeight(14);

    /* @formatter:off */
		this.binder.forField(this.ipv4)
			.bind(source -> source.getSet(SettingsKey.DOMAIN_IPV4), (bean, fieldvalue) -> bean.set(SettingsKey.DOMAIN_IPV4, fieldvalue));
		this.binder.forField(this.ipv6)
			.bind(source -> source.getSet(SettingsKey.DOMAIN_IPV6), (bean, fieldvalue) -> bean.set(SettingsKey.DOMAIN_IPV6, fieldvalue));
		this.binder.forField(this.subDomains)
			.bind(source -> source.getSet(SettingsKey.DOMAIN_ALTERNATIVE_NAMES), (bean, fieldvalue) -> bean.set(SettingsKey.DOMAIN_ALTERNATIVE_NAMES, fieldvalue));
		this.binder.forField(this.active)
			.bind(source -> source.getBool(SettingsKey.DOMAIN_ACTIVATED), (bean , value) -> bean.set(SettingsKey.DOMAIN_ACTIVATED, value));
		this.binder.forField(this.httpPort)
			.withConverter(new StringToIntegerConverter(this.msg.getMessage("validator.port.numeric")))
			.withValidator(new IntegerRangeValidator(this.msg.getMessage("validator.port.range"), 1, 65535))
			.bind(source -> source.getInt(SettingsKey.DOMAIN_HTTP_PORT), (bean , value) -> bean.set(SettingsKey.DOMAIN_HTTP_PORT, value));
		this.binder.forField(this.httpsPort)
			.withConverter(new StringToIntegerConverter(this.msg.getMessage("validator.port.numeric")))
			.withValidator(new IntegerRangeValidator(this.msg.getMessage("validator.port.range"), 1, 65535))
			.bind(source -> source.getInt(SettingsKey.DOMAIN_HTTPS_PORT), (bean , value) -> bean.set(SettingsKey.DOMAIN_HTTPS_PORT, value));
		/* @formatter:on */

    this.ipv4.setValidateCallback(value -> InetAddressValidator.getInstance().isValidInet4Address(value));
    this.ipv6.setValidateCallback(value -> InetAddressValidator.getInstance().isValidInet6Address(value));

    this.buttons.getSave().addClickListener(event -> {
      if (this.binder.isValid()) {
        this.ipv4.selectAll();
        this.ipv6.selectAll();
        this.subDomains.selectAll();
        this.settingsFacade.save(this.binder.getBean());
        Notification.show(this.msg.getMessage("settings.button.save.success"), Type.HUMANIZED_MESSAGE);
      } else {
        this.binder.validate();
      }
    });

    this.addComponents(this.active, this.httpPort, this.httpsPort, this.ipv4, this.ipv6, this.subDomains);
    this.addComponent(this.buttons);
    this.setSizeFull();
    this.setMargin(true);
  }

  @Override
  public void enter(final DomainInfo info) {
    log.trace("DomainSettingsTabDomaion::enter");

    final Settings settings = this.settingsFacade.getDomain(info);

    this.subDomains.setRemoveValidateCallback(value -> !info.getName().equals(value));
    this.subDomains.setValidateCallback(value -> {
      boolean valid = DomainValidator.getInstance().isValid(value);
      // Subdomain nicht als eigene domain eingetragen
      valid = valid && !this.domainFacade.getAllDomainInfos().parallelStream().filter(i -> i.getName().equalsIgnoreCase(value)).findAny().isPresent();
      if (settings.getBool(SettingsKey.DOMAIN_ALTERNATIVE_NAMES_MANDATORY_SUBDOMAIN)) {
        // Eingabe ist Subdomain bzw. Sub-Subdomain der aktuellen Domain
        valid = valid && value.endsWith(String.format(".%s", info.getName()));
      }
      return valid;
    });

    this.binder.setBean(settings);
    this.domainEditView = String.format("%s/%s", DomainEditView.NAME, info.getId());
  }

  @Override
  public void enter() {
    throw new RuntimeException("Method 'enter' not supported use enter(DomainInfo)");
  }

  private void cancelEdit(final ClickEvent event) {
    UI.getCurrent().getNavigator().navigateTo(this.domainEditView);
  }
}