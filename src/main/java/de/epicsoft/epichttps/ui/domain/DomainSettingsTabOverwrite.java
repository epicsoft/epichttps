/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.domain;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.UI;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.facade.SettingsFacade;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import de.epicsoft.epichttps.ui.common.SaveCancelButtons;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DomainSettingsTabOverwrite extends FormLayout implements DomainSettingsTab {

  private static final long serialVersionUID = 3715122965673226214L;

  @Autowired
  private transient EpicMessageSource msg;

  @Autowired
  private transient SettingsFacade settingsFacade;

  @Autowired
  private transient SettingsService settingsService;

  @Autowired
  private SaveCancelButtons buttons;

  @Getter
  private final CheckBox csr = new CheckBox();

  @Getter
  private final CheckBox hstsHpkp = new CheckBox();

  @Getter
  private final CheckBox webserver = new CheckBox();

  @Getter
  private final CheckBox keys = new CheckBox();

  private final BeanValidationBinder<Settings> binder = new BeanValidationBinder<>(Settings.class);

  private OnSaveAction action;

  private String domainEditView;

  @PostConstruct
  public void init() {
    log.trace("DomainSettingsTabOverwrite::init");

    this.csr.setCaption(this.msg.getMessage("settings.field.name.overwrite.csr"));
    this.hstsHpkp.setCaption(this.msg.getMessage("settings.field.name.overwrite.hsts.hpkp"));
    this.webserver.setCaption(this.msg.getMessage("settings.field.name.overwrite.webserver"));
    this.keys.setCaption(this.msg.getMessage("settings.field.name.overwrite.keys"));

    /* @formatter:off */
		this.binder.forField(this.csr)
			.bind(source -> source.getBool(SettingsKey.OVERWRITE_CSR), (bean, fieldvalue) -> bean.set(SettingsKey.OVERWRITE_CSR, fieldvalue));
		this.binder.forField(this.hstsHpkp)
			.bind(source -> source.getBool(SettingsKey.OVERWRITE_HSTS_HPKP), (bean, fieldvalue) -> bean.set(SettingsKey.OVERWRITE_HSTS_HPKP, fieldvalue));
		this.binder.forField(this.webserver)
			.bind(source -> source.getBool(SettingsKey.OVERWRITE_WEBSERVER), (bean, fieldvalue) -> bean.set(SettingsKey.OVERWRITE_WEBSERVER, fieldvalue));
		this.binder.forField(this.keys)
		  .bind(source -> source.getBool(SettingsKey.OVERWRITE_KEYS), (bean, fieldvalue) -> bean.set(SettingsKey.OVERWRITE_KEYS, fieldvalue));
		/* @formatter:on */

    this.buttons.getCancel().addClickListener(this::cancelEdit);
    this.buttons.getSave().addClickListener(event -> {
      if (this.binder.isValid()) {
        final Settings settings = this.binder.getBean();
        this.settingsFacade.save(settings);

        if (this.action != null) {
          this.action.action(settings);
        }
      } else {
        this.binder.validate();
      }
    });

    this.addComponents(this.csr, this.hstsHpkp, this.webserver, this.keys);
    this.addComponent(this.buttons);
    this.setSizeFull();
    this.setMargin(true);
  }

  @Override
  public void enter(final DomainInfo info) {
    log.trace("DomainSettingsTabOverwrite::enter");

    final Settings settings = this.settingsService.getDomain(info);
    this.binder.setBean(settings);

    this.domainEditView = String.format("%s/%s", DomainEditView.NAME, info.getId());
  }

  public void onSave(final OnSaveAction action) {
    this.action = action;
  }

  @FunctionalInterface
  public interface OnSaveAction {

    void action(Settings settings);
  }

  private void cancelEdit(final ClickEvent event) {
    UI.getCurrent().getNavigator().navigateTo(this.domainEditView);
  }
}