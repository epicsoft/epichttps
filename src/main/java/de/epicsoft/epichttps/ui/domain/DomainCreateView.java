/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.domain;

import javax.annotation.PostConstruct;

import org.apache.commons.validator.routines.DomainValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.vaadin.data.BeanValidationBinder;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.facade.DomainFacade;
import de.epicsoft.epichttps.facade.LetsEncryptFacade;
import de.epicsoft.epichttps.ui.common.EpicView;
import de.epicsoft.epichttps.ui.common.SaveCancelButtons;
import de.epicsoft.epichttps.ui.common.ViewHelper;
import de.epicsoft.epichttps.ui.settings.components.EpicTextField;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@UIScope
@SpringComponent
@SpringView(name = DomainCreateView.NAME)
public class DomainCreateView extends VerticalLayout implements EpicView {

	private static final long serialVersionUID = 1072346927432189536L;

	public static final String NAME = "domaincreate";

	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	private transient LetsEncryptFacade letsEncryptFacade;
	@Autowired
	private transient ViewHelper viewHelper;
	@Autowired
	private transient DomainFacade domainFacade;

	@Autowired
	private SaveCancelButtons buttons;

	private final BeanValidationBinder<DomainCreateForm> binder = new BeanValidationBinder<>(DomainCreateForm.class);
	private final TextField domain = new EpicTextField();

	@PostConstruct
	public void init() {
		log.trace("DomainCreateView::init");

		this.domain.setCaption(this.msg.getMessage("domain.field.name"));
		this.buttons.getSave().setCaption(this.msg.getMessage("domain.button.add"));
		this.buttons.getCancel().setCaption(this.msg.getMessage("domain.button.cancel"));

		/* @formatter:off */
		this.binder.forField(this.domain)
		  .withValidator(value -> this.letsEncryptFacade.isAccoutExists(), this.msg.getMessage("validator.le.account.not.exsits"))
			.withValidator(value -> StringUtils.hasText(value), this.msg.getMessage("validator.text.empty"))
			.withValidator(value -> DomainValidator.getInstance().isValid(value), this.msg.getMessage("validator.domain.invalid"))
			.withValidator(value ->
				!this.domainFacade.getAllDomainInfos().parallelStream().filter(info -> info.getName().equalsIgnoreCase(value)).findAny().isPresent(),
				this.msg.getMessage("validator.domain.exists"))
			.bind(source -> source.getDomain(), (bean, fieldvalue) -> bean.setDomain(fieldvalue));
		/* @formatter:on */

		this.buttons.getSave().addClickListener(event -> {
			if (this.binder.isValid()) {
				try {
					final DomainInfo info = this.domainFacade.createDomain(this.binder.getBean().getDomain());
					log.info("DomainInfo:{}", info.toString());
					UI.getCurrent().getNavigator().navigateTo(String.format("%s/%s", DomainEditView.NAME, info.getId()));
				} catch (final Exception e) {
					log.error(e.getMessage(), e);
				}
			} else {
				this.binder.validate();
			}
		});

		final FormLayout form = new FormLayout();
		form.setMargin(false);
		form.addComponents(this.domain, this.buttons);

		final Layout content = this.viewHelper.buildContent();
		content.addComponent(form);

		this.addComponent(this.viewHelper.buildHeader(this.msg.getMessage("domain.new.title")));
		this.addComponent(content);
		this.setSpacing(true);
		this.setMargin(false);
	}

	@Override
	public void enter(final ViewChangeEvent event) {
		log.trace("DomainCreateView::enter");

		this.binder.setBean(new DomainCreateForm());
	}
}
