/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.domain;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.facade.DomainFacade;
import de.epicsoft.epichttps.facade.SettingsFacade;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.ui.common.EpicView;
import de.epicsoft.epichttps.ui.common.ViewHelper;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@UIScope
@SpringComponent
@SpringView(name = DomainSettingsView.NAME)
public class DomainSettingsView extends VerticalLayout implements EpicView {

	private static final long serialVersionUID = 2396613180380691643L;

	public static final String NAME = "domainsettings";

	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	private transient ViewHelper viewHelper;
	@Autowired
	private transient DomainFacade domainFacade;
	@Autowired
	private transient SettingsFacade settingsFacade;

	@Autowired
	private DomainSettingsTabOverwrite overwrite;
	@Autowired
	private DomainSettingsTabCSR csr;
	@Autowired
	private DomainSettingsTabHsts hsts;
	@Autowired
	private DomainSettingsTabWebserver webserver;
	@Autowired
	private DomainSettingsTabCert cert;
	@Autowired
	private DomainSettingsTabDomain domain;

	private final Label title = new Label();
	private final TabSheet tabs = new TabSheet();
	private DomainInfo currentDomainInfo;

	private Tab tabCsr;
	private Tab tabHsts;
	private Tab tabWebserver;
	private Tab tabCert;

	@PostConstruct
	public void init() {
		log.trace("DomainSettingsView:init");

		final Component header = this.viewHelper.buildHeader(this.title);
		final Component content = this.buildContent();

		this.addComponents(header, content);
		this.setExpandRatio(content, 1f);
		this.setSizeFull();
		this.setSpacing(true);
		this.setMargin(false);

		this.overwrite.onSave(this::setTabVisibility);
	}

	@Override
	public void enter(final ViewChangeEvent event) {
		log.trace("DomainSettingsView::enter");

		final String id = event.getParameters();
		this.currentDomainInfo = this.domainFacade.getDomainInfoById(id);

		this.title.setValue(String.format("%s - %s", this.currentDomainInfo.getName(), this.msg.getMessage("settings.header.title")));

		final DomainSettingsTab defaultTab = this.domain;
		defaultTab.enter(this.currentDomainInfo);
		this.tabs.setSelectedTab(defaultTab);

		final Settings domainSettings = this.settingsFacade.getDomain(this.currentDomainInfo);
		this.setTabVisibility(domainSettings);
	}

	private void setTabVisibility(final Settings domainSettings) {
		if (!domainSettings.getBool(SettingsKey.OVERWRITE_CSR)) {
			this.settingsFacade.resetCsr(domainSettings);
		}
		if (!domainSettings.getBool(SettingsKey.OVERWRITE_HSTS_HPKP)) {
			this.settingsFacade.resetHstsHpkp(domainSettings);
		}
		if (!domainSettings.getBool(SettingsKey.OVERWRITE_WEBSERVER)) {
			this.settingsFacade.resetWebserver(domainSettings);
		}
		if (!domainSettings.getBool(SettingsKey.OVERWRITE_KEYS)) {
			this.settingsFacade.resetCert(domainSettings);
		}

		this.tabCsr.setVisible(domainSettings.getBool(SettingsKey.OVERWRITE_CSR));
		this.tabHsts.setVisible(domainSettings.getBool(SettingsKey.OVERWRITE_HSTS_HPKP));
		this.tabWebserver.setVisible(domainSettings.getBool(SettingsKey.OVERWRITE_WEBSERVER));
		this.tabCert.setVisible(domainSettings.getBool(SettingsKey.OVERWRITE_KEYS));
	}

	private Component buildContent() {
		this.tabs.addStyleName(ValoTheme.TABSHEET_EQUAL_WIDTH_TABS);
		this.tabs.addTab(this.overwrite, this.msg.getMessage("settings.tab.name.overwrite"));
		this.tabs.addTab(this.domain, this.msg.getMessage("settings.tab.name.domain"));
		this.tabCsr = this.tabs.addTab(this.csr, this.msg.getMessage("settings.tab.name.csr"));
		this.tabHsts = this.tabs.addTab(this.hsts, this.msg.getMessage("settings.tab.name.hsts.hpkp"));
		this.tabWebserver = this.tabs.addTab(this.webserver, this.webserver.getTabCaption());
		this.tabCert = this.tabs.addTab(this.cert, this.msg.getMessage("settings.tab.name.cert"));

		this.tabs.addSelectedTabChangeListener(event -> {
			final Component tab = event.getTabSheet().getSelectedTab();
			if (tab instanceof DomainSettingsTab) {
				((DomainSettingsTab) tab).enter(this.currentDomainInfo);
			}
		});

		final VerticalLayout wrapper = new VerticalLayout(this.tabs);
		wrapper.setHeightUndefined();
		wrapper.setWidth(100, Unit.PERCENTAGE);

		final Panel panel = new Panel(wrapper);
		panel.addStyleName(ValoTheme.PANEL_WELL);
		panel.setSizeFull();
		return panel;
	}
}