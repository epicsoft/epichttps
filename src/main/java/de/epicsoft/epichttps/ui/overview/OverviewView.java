/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.overview;

import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.facade.DomainFacade;
import de.epicsoft.epichttps.facade.LetsEncryptFacade;
import de.epicsoft.epichttps.ui.common.EpicView;
import de.epicsoft.epichttps.ui.common.ViewHelper;
import de.epicsoft.epichttps.ui.le.LetsEncryptView;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@UIScope
@SpringComponent
@SpringView(name = OverviewView.NAME)
public class OverviewView extends VerticalLayout implements EpicView {

	private static final long serialVersionUID = 39322372625117582L;

	public static final String NAME = "overview";

	@Autowired
	private transient ApplicationContext ctx;
	@Autowired
	private transient DomainFacade domainFacade;
	@Autowired
	private transient LetsEncryptFacade letsEncryptFacade;
	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	private transient ViewHelper viewHelper;

	private final HorizontalLayout letsEncryptAuthWarning = new HorizontalLayout();
	private VerticalLayout layout;

	@PostConstruct
	public void init() {
		log.trace("OverviewView::init");

		this.layout = this.viewHelper.buildContent();
		this.layout.setHeightUndefined();
		this.layout.setWidth(100, Unit.PERCENTAGE);

		final Panel content = new Panel(this.layout);
		content.addStyleName(ValoTheme.PANEL_SCROLL_INDICATOR);
		content.setSizeFull();

		this.addComponent(this.viewHelper.buildHeader(this.msg.getMessage("overview.title")));
		this.addComponent(this.buildLetsEncryptAuthWarning());
		this.addComponent(content);

		this.setSpacing(true);
		this.setMargin(false);
	}

	@Override
	public void enter(final ViewChangeEvent event) {
		log.trace("OverviewView::enter");

		this.letsEncryptAuthWarning.setVisible(!this.letsEncryptFacade.isAccoutExists());

		this.layout.removeAllComponents();
		final Set<DomainInfo> allInfos = this.domainFacade.getAllDomainInfos();
		if (allInfos != null && allInfos.size() > 0) {
			/* @formatter:off */
			allInfos.stream()
			  .sorted((info1, info2) -> info1.getName().compareTo(info2.getName()))
			  .forEach(domainInfo -> {
				  final OverviewDomain od = this.ctx.getBean(OverviewDomain.class);
				  od.enter(domainInfo);
				  this.layout.addComponent(od);
			  });
			/* @formatter:on */
		} else {
			this.layout.addComponent(this.buildNoDomainFound());
		}
	}

	private Component buildNoDomainFound() {
		final Label notFount = new Label(this.msg.getMessage("overview.no.entry"));
		final HorizontalLayout od = new HorizontalLayout(notFount);
		return od;
	}

	private Component buildLetsEncryptAuthWarning() {
		final Label authWarning = new Label();
		authWarning.setValue(this.msg.getMessage("validator.le.account.not.exsits"));
		authWarning.addStyleName(ValoTheme.LABEL_FAILURE);

		final Button account = new Button();
		account.setCaption(this.msg.getMessage("overview.letsencrypt.create.account"));
		account.addClickListener(event -> UI.getCurrent().getNavigator().navigateTo(LetsEncryptView.NAME));
		account.addStyleName(ValoTheme.BUTTON_FRIENDLY);

		this.letsEncryptAuthWarning.addComponents(authWarning, account);
		this.letsEncryptAuthWarning.setVisible(false);
		return this.letsEncryptAuthWarning;
	}
}