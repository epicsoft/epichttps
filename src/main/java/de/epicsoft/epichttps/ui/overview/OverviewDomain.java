/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.overview;

import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.util.Assert;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.domain.Domain;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.facade.DomainFacade;
import de.epicsoft.epichttps.ui.common.ViewHelper;
import de.epicsoft.epichttps.ui.domain.DomainEditView;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OverviewDomain extends VerticalLayout {

	private static final long serialVersionUID = 4753340196151403507L;

	@Autowired
	private transient DomainFacade domainFacade;
	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	private transient DateTimeFormatter dateTimeFormatter;

	@Autowired
	private ViewHelper viewHelper;

	private final Panel main = new Panel();
	private final Label cert = new Label();
	private final Label certValid = new Label();
	private final Button privateKeys = new Button();
	private final Button backupKeys = new Button();
	private final Button edit = new Button();
	private final VerticalLayout warnings = new VerticalLayout();

	@PostConstruct
	public void init() {
		log.trace("OverviewDomain::init");

		this.main.setContent(new VerticalLayout(this.buildMain(), this.buildFooter()));

		this.addComponent(this.main);
		this.setSpacing(true);
		this.setMargin(false);
	}

	public void enter(final DomainInfo info) {
		log.trace("OverviewDomain::enter");
		Assert.notNull(info, "Domain Info must be not null");

		final Domain domain = this.domainFacade.getDomainByDomainInfo(info);
		this.main.setCaption(domain.getInfo().getName());
		this.privateKeys.setCaption(String.valueOf(domain.getPrivateKeys() != null ? domain.getPrivateKeys().size() : 0));
		this.backupKeys.setCaption(String.valueOf(domain.getBackupKeys() != null ? domain.getBackupKeys().size() : 0));
		this.edit.addClickListener(e -> UI.getCurrent().getNavigator().navigateTo(String.format("%s/%s", DomainEditView.NAME, domain.getInfo().getId())));
		if (domain.getCurrentCertificate() != null) {
			this.cert.setValue(domain.getCurrentCertificate().getIssuer());
			this.certValid.setValue(this.msg.getMessage("overview.domain.certificate.valid.until", domain.getCurrentCertificate().getValidUntil().format(this.dateTimeFormatter)));
		} else {
			this.cert.setVisible(false);
			this.certValid.setVisible(false);
		}

		this.updateWarnings(domain);
	}

	private void updateWarnings(final Domain domain) {
		final List<Component> warningComponents = this.viewHelper.buildDomainWarnings(domain);
		this.warnings.setVisible(warningComponents.size() > 0);
		this.warnings.addComponents(warningComponents.toArray(new Component[warningComponents.size()]));
	}

	private Component buildMain() {
		final VerticalLayout domainLayout = new VerticalLayout();
		domainLayout.setMargin(false);
		domainLayout.setSpacing(false);
		domainLayout.addComponents(this.cert, this.certValid);

		return domainLayout;
	}

	private Component buildFooter() {
		this.privateKeys.setIcon(VaadinIcons.KEY);
		this.privateKeys.setDescription(this.msg.getMessage("overview.domain.private.keys"));
		this.privateKeys.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		this.privateKeys.addStyleName(ValoTheme.BUTTON_QUIET);
		this.privateKeys.setEnabled(false);

		this.backupKeys.setIcon(VaadinIcons.SHIELD);
		this.backupKeys.setDescription(this.msg.getMessage("overview.domain.backup.keys"));
		this.backupKeys.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		this.backupKeys.addStyleName(ValoTheme.BUTTON_QUIET);
		this.backupKeys.setEnabled(false);

		this.edit.setIcon(VaadinIcons.PENCIL);
		this.edit.setCaption(this.msg.getMessage("overview.domain.edit"));
		this.edit.addStyleName(ValoTheme.BUTTON_TINY);

		this.warnings.setMargin(false);
		this.warnings.setSpacing(true);
		this.warnings.setVisible(false);

		final HorizontalLayout footer = new HorizontalLayout(this.privateKeys, this.backupKeys, this.edit);
		footer.setSpacing(true);
		footer.setMargin(false);
		footer.setComponentAlignment(this.edit, Alignment.MIDDLE_LEFT);

		final VerticalLayout wrapper = new VerticalLayout(footer, this.warnings);
		wrapper.setSpacing(false);
		wrapper.setMargin(false);
		return wrapper;
	}
}
