/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.util.StringUtils;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.common.event.AfterViewChangeEvent;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.facade.DomainFacade;
import de.epicsoft.epichttps.ui.about.AboutView;
import de.epicsoft.epichttps.ui.common.HorizontalLine;
import de.epicsoft.epichttps.ui.domain.DomainCreateView;
import de.epicsoft.epichttps.ui.domain.DomainEditView;
import de.epicsoft.epichttps.ui.overview.OverviewView;
import de.epicsoft.epichttps.ui.settings.SettingsView;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@UIScope
@SpringComponent
public class Navigation extends VerticalLayout {

	private static final long serialVersionUID = -6053750856937781090L;

	@Value("${epicsoft.navigation.domain.button.name.length}")
	private Integer domainButtonNameLength;

	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	private transient DomainFacade domainFacade;

	private final VerticalLayout layout = new VerticalLayout();
	private final List<Button> buttons = new ArrayList<>();
	private final List<Button> domainButtons = new ArrayList<>();

	@PostConstruct
	public void init() {
		final Button overview = this.buildOverviewButton();
		final Button settings = this.buildSettingsButton();
		final Button addDomain = this.buildAddDomainButton();
		final Button about = this.buildAboutButton();

		this.buttons.add(overview);
		this.buttons.add(settings);
		this.buttons.add(addDomain);
		this.buttons.add(about);

		this.layout.addComponent(this.buildTitleLabel());
		this.layout.addComponents(overview, settings, addDomain, about);
		this.layout.addComponent(new HorizontalLine());

		this.addComponent(this.layout);
		this.updateDomainButtons();
	}

	public void updateDomainButtons() {
		this.domainButtons.forEach(bt -> {
			this.layout.removeComponent(bt);
			this.buttons.remove(bt);
		});
		this.domainButtons.clear();
		/* @formatter:off */
		this.domainFacade.getAllDomainInfos().stream()
		  .sorted((bt1, bt2) -> bt1.getName().compareTo(bt2.getName()))
		  .forEach(this::addDomainButton);
		/* @formatter:on */
	}

	private Component buildTitleLabel() {
		final Label title = new Label(String.format("%s<br>%s", this.msg.getMessage("title.main"), this.msg.getMessage("title.sub")));
		title.setContentMode(ContentMode.HTML);
		title.addStyleName(ValoTheme.LABEL_H3);
		title.addStyleName(ValoTheme.LABEL_BOLD);

		final VerticalLayout titles = new VerticalLayout(title);
		titles.setMargin(false);
		titles.setSpacing(false);
		return titles;
	}

	private Button buildOverviewButton() {
		final Button overview = new Button(this.msg.getMessage("navigation.overview"));
		overview.setId(OverviewView.NAME);
		overview.setWidth(100, Unit.PERCENTAGE);
		overview.setStyleName(ValoTheme.BUTTON_TINY);
		overview.addClickListener(e -> UI.getCurrent().getNavigator().navigateTo(OverviewView.NAME));
		return overview;
	}

	private Button buildSettingsButton() {
		final Button settings = new Button(this.msg.getMessage("navigation.settings"));
		settings.setId(SettingsView.NAME);
		settings.setIcon(VaadinIcons.COGS);
		settings.setWidth(100, Unit.PERCENTAGE);
		settings.addStyleName(ValoTheme.BUTTON_TINY);
		settings.addClickListener(e -> UI.getCurrent().getNavigator().navigateTo(SettingsView.NAME));
		return settings;
	}

	private void addDomainButton(final DomainInfo domainInfo) {
		final Button button = this.buildDomainButton(domainInfo);
		this.layout.addComponent(button);
		this.buttons.add(button);
		this.domainButtons.add(button);
	}

	private Button buildDomainButton(final DomainInfo domainInfo) {
		final String name = domainInfo.getName();
		String btName = name;
		if (name.length() >= this.domainButtonNameLength) {
			btName = String.format("%s...", name.substring(0, this.domainButtonNameLength));
		}

		final Button domain = new Button(btName);
		domain.setDescription(name);
		domain.addStyleName(ValoTheme.BUTTON_TINY);
		domain.setWidth(100, Unit.PERCENTAGE);
		domain.setId(this.buildDomainButtonId(domainInfo.getId()));
		domain.addClickListener(e -> UI.getCurrent().getNavigator().navigateTo(e.getButton().getId()));
		return domain;
	}

	private String buildDomainButtonId(final String domainId) {
		return String.format("%s/%s", DomainEditView.NAME, domainId);
	}

	private Button buildAddDomainButton() {
		final Button addDomain = new Button(this.msg.getMessage("navigation.domain.new"));
		addDomain.setId(DomainCreateView.NAME);
		addDomain.setIcon(VaadinIcons.PLUS);
		addDomain.setWidth(100, Unit.PERCENTAGE);
		addDomain.addStyleName(ValoTheme.BUTTON_TINY);
		addDomain.addClickListener(e -> UI.getCurrent().getNavigator().navigateTo(DomainCreateView.NAME));
		return addDomain;
	}

	private Button buildAboutButton() {
		final Button about = new Button(this.msg.getMessage("navigation.about"));
		about.setId(AboutView.NAME);
		about.setIcon(VaadinIcons.INFO);
		about.setWidth(100, Unit.PERCENTAGE);
		about.setStyleName(ValoTheme.BUTTON_TINY);
		about.addClickListener(e -> UI.getCurrent().getNavigator().navigateTo(AboutView.NAME));
		return about;
	}

	@EventListener
	private void changeSelected(final AfterViewChangeEvent event) {
		final String viewName = event.getFullViewName();
		final String params = event.getParameters();

		this.buttons.forEach(button -> {
			button.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
			if (StringUtils.hasText(button.getId())) {
				if (StringUtils.hasText(params) && button.getId().equals(this.buildDomainButtonId(params)) || viewName.equalsIgnoreCase(button.getId())) {
					button.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				}
			} else {
				log.warn("Button has no ID::" + button.getCaption());
			}
		});
	}
}