/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.login;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.security.core.AuthenticationException;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.LoginForm;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import de.epicsoft.epichttps.Profiles;
import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.facade.LoginFacade;
import de.epicsoft.epichttps.ui.common.Resource;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@UIScope
@SpringComponent
@SpringView(name = LoginView.NAME)
public class LoginView extends CustomComponent implements View {

	private static final long serialVersionUID = 5214809936080862607L;

	public static final String NAME = ""; // default view

	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";

	@Value("${epicsoft.login.delay}")
	private Integer delay;

	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	private transient Environment env;
	@Autowired
	private transient LoginFacade loginFacade;

	@PostConstruct
	public void init() {
		log.trace("LoginView::init");

		final Label title = new Label(this.msg.getMessage("title.main"));
		title.addStyleName(ValoTheme.LABEL_H1);

		final Component login = this.buildLogin();

		final VerticalLayout titleLogin = new VerticalLayout(title, login);
		titleLogin.setSpacing(false);
		titleLogin.setMargin(false);
		titleLogin.setComponentAlignment(title, Alignment.TOP_CENTER);
		titleLogin.setComponentAlignment(login, Alignment.TOP_CENTER);

		final Label footer = new Label(String.format("%s %s", this.msg.getMessage("title.main"), this.msg.getMessage("title.sub")));
		footer.addStyleName(ValoTheme.LABEL_HUGE);
		footer.addStyleName(ValoTheme.LABEL_BOLD);

		final VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		layout.setMargin(true);
		layout.addComponent(titleLogin);
		layout.setComponentAlignment(titleLogin, Alignment.TOP_CENTER);
		layout.addComponent(footer);
		layout.setComponentAlignment(footer, Alignment.BOTTOM_RIGHT);

		this.setSizeFull();
		this.setCompositionRoot(layout);
	}

	@Override
	public void enter(final ViewChangeEvent event) {
		log.trace("LoginView::enter");
		if (this.env.acceptsProfiles(Profiles.DEV, Profiles.TEST) && this.env.getProperty("epicsoft.auth.autologin", Boolean.class)) {
			final String username = this.env.getProperty("epichttps.username");
			log.warn("DEBUG LOGIN with '{}'", username);
			this.login(username, this.env.getProperty("epichttps.password"));
		}
	}

	private LoginForm buildLogin() {
		final LoginForm login = new LoginForm();
		login.setUsernameCaption(this.msg.getMessage("login.username"));
		login.setPasswordCaption(this.msg.getMessage("login.password"));
		login.setLoginButtonCaption(this.msg.getMessage("login.signin"));
		login.addLoginListener(event -> {
			final String username = event.getLoginParameter(USERNAME);
			final String password = event.getLoginParameter(PASSWORD);

			this.delayLogin();
			this.login(username, password);
		});
		return login;
	}

	private void login(final String username, final String password) {
		try {
			this.loginFacade.tryLogin(username, password);
			VaadinService.reinitializeSession(VaadinService.getCurrentRequest());
			Page.getCurrent().setLocation(Resource.DASHBOARD);
		} catch (@SuppressWarnings("unused") final AuthenticationException e) {
			Notification.show(this.msg.getMessage("login.failed"), Notification.Type.ERROR_MESSAGE);
		}
	}

	private void delayLogin() {
		try {
			// delay login time
			Thread.sleep(this.delay);
		} catch (@SuppressWarnings("unused") final InterruptedException e) {
			// do nothing
		}
	}
}