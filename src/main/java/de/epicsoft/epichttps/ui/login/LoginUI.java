/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

import de.epicsoft.epichttps.ui.DashboardUI;
import de.epicsoft.epichttps.ui.common.AccessDeniedView;
import de.epicsoft.epichttps.ui.common.Resource;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@UIScope
@Title(DashboardUI.TITLE)
@Theme(ValoTheme.THEME_NAME)
@SpringUI(path = Resource.LOGIN)
public class LoginUI extends UI {

	private static final long serialVersionUID = 6127769498714906907L;

	@Autowired
	private transient ApplicationContext context;
	@Autowired
	private transient SpringViewProvider viewProvider;

	@Override
	protected void init(final VaadinRequest request) {
		log.debug("LoginUI::init");

		final Navigator nav = new Navigator(this, this);
		nav.addProvider(this.viewProvider);
		nav.addView(LoginView.NAME, this.context.getBean(LoginView.class));
		nav.setErrorView(AccessDeniedView.class);
		this.setNavigator(nav);

		UI.getCurrent().setErrorHandler(event -> {
			final Throwable e = event.getThrowable();
			log.error("UI::{}::{}::{}", this.getClass().getSimpleName(), e.getClass().getSimpleName(), e.getMessage(), e);
			Page.getCurrent().setLocation(Resource.LOGIN);
		});

		VaadinSession.getCurrent().setErrorHandler(event -> {
			final Throwable e = event.getThrowable();
			log.error("Session::{}::{}::{}", this.getClass().getSimpleName(), e.getClass().getSimpleName(), e.getMessage(), e);
			Page.getCurrent().setLocation(Resource.LOGIN);
		});
	}
}