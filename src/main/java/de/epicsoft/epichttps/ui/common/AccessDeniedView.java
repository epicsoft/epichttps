/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.common;

import org.springframework.stereotype.Component;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ExternalResource;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@UIScope
@Component
public class AccessDeniedView extends VerticalLayout implements View {

	private static final long serialVersionUID = 51600208523432773L;

	@Override
	public void enter(final ViewChangeListener.ViewChangeEvent event) {
		log.trace("AccessDeniedView::enter");

		final Label message = new Label("Access Denied");
		message.addStyleName(ValoTheme.LABEL_FAILURE);
		message.setSizeUndefined();

		final Link link = new Link("To Login", new ExternalResource(Resource.LOGIN));

		final VerticalLayout layout = new VerticalLayout(message, link);
		layout.setComponentAlignment(message, Alignment.MIDDLE_CENTER);
		layout.setComponentAlignment(link, Alignment.MIDDLE_CENTER);
		layout.setSpacing(true);

		final HorizontalLayout wrapper = new HorizontalLayout(layout);
		wrapper.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);
		wrapper.setSizeFull();

		this.setSizeFull();
		this.addComponent(wrapper);
	}
}