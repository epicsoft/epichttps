/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.common;

import java.time.format.DateTimeFormatter;
import java.util.Collection;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.terminal.Process;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Scope("prototype")
@SpringComponent
public class TerminalWindow extends Window {

	private static final long serialVersionUID = -8871826335415334937L;

	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	private transient DateTimeFormatter detailedDateTimeFormatter;

	protected final Button close = new Button();
	protected final CheckBox autoClose = new CheckBox();
	private final VerticalLayout list = new VerticalLayout();

	@PostConstruct
	public void init() {
		this.list.setWidth(100, Unit.PERCENTAGE);
		this.list.setSpacing(true);
		this.list.setMargin(true);

		this.close.setCaption(this.msg.getMessage("processes.close"));
		this.close.addClickListener(event -> this.close());

		this.autoClose.setCaption(this.msg.getMessage("processes.close.auto"));

		final HorizontalLayout buttons = new HorizontalLayout();
		buttons.addComponent(this.close);
		buttons.setWidth(100, Unit.PERCENTAGE);
		buttons.setComponentAlignment(this.close, Alignment.MIDDLE_RIGHT);

		final VerticalLayout layout = new VerticalLayout(this.list, buttons);
		layout.setExpandRatio(this.list, 1.0f);
		layout.setExpandRatio(buttons, 0.0f);
		layout.setSpacing(true);
		layout.setSizeFull();
		layout.setMargin(true);

		final Panel panel = new Panel(layout);
		panel.addStyleName(ValoTheme.PANEL_WELL);
		panel.setSizeFull();

		this.setCaption(this.msg.getMessage("processes.title"));
		this.setHeight(90, Unit.PERCENTAGE);
		this.setWidth(90, Unit.PERCENTAGE);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.addCloseShortcut(KeyCode.ESCAPE, null);
		this.setResizable(false);
		this.setContent(panel);
		this.addStyleName(ValoTheme.WINDOW_TOP_TOOLBAR);
	}

	public synchronized void setMessages(final Collection<Process> processes) {
		this.list.removeAllComponents();
		/* @formatter:off */
		processes.stream()
		  .sorted((p1, p2) -> p2.getStartTime().compareTo(p1.getStartTime()))
		  .forEach(process -> this.list.addComponent(this.buildMessage(process)));
		/* @formatter:on */
	}

	private Label buildMessage(final Process process) {
		final Label message = new Label();
		message.setId(process.getId().toString());
		message.setCaption(process.getMessage());
		message.setCaption(String.format("%s - %s", process.getStartTime().format(this.detailedDateTimeFormatter), message.getCaption()));
		message.setDescription(process.getOutput());

		switch (process.getStatus()) {
		case PREPARED:
		case RUNNING:
			message.setIcon(VaadinIcons.COG);
			break;
		case SUCCESS:
			message.setIcon(VaadinIcons.CHECK);
			break;
		case FAILED:
			message.setIcon(VaadinIcons.CLOSE);
			break;
		}

		return message;
	}
}