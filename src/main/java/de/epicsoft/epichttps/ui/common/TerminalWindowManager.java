/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.common;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.util.Assert;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.UI;

import de.epicsoft.epichttps.common.event.ProcessBeginEvent;
import de.epicsoft.epichttps.common.event.ProcessEndEvent;
import de.epicsoft.epichttps.terminal.Process;
import de.epicsoft.epichttps.terminal.ProcessManager;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@Slf4j
@SpringComponent
public class TerminalWindowManager {

	@Autowired
	private ApplicationContext context;
	@Autowired
	private ProcessManager processManager;

	private final Map<UUID, Process> processes = new ConcurrentHashMap<>();

	public void showTerminal(final UI ui) {
		if (ui != null) {
			final TerminalWindow terminal = this.context.getBean(TerminalWindow.class);
			terminal.setMessages(this.processes.values());
			this.showTerminal(ui, terminal);
		}
	}

	public void showTerminal(final UI ui, final TerminalWindow terminal) {
		if (ui != null) {
			log.debug("TerminalWindow attached to current UI");
			ui.getWindows().forEach(w -> w.close());
			ui.addWindow(terminal);
			ui.push();
		} else {
			log.warn("Current UI not found");
		}
	}

	@EventListener
	public synchronized void startProcess(final ProcessBeginEvent event) {
		Assert.notNull(event, "Event may be not null");

		final Process process = event.getProcess();
		this.processes.put(process.getId(), process);

		final TerminalWindow terminal = this.context.getBean(TerminalWindow.class);
		terminal.setClosable(false);
		terminal.setMessages(this.processes.values());

		this.showTerminal(UI.getCurrent(), terminal);
	}

	@EventListener
	public synchronized void endProcess(final ProcessEndEvent event) {
		Assert.notNull(event, "Event may be not null");

		final Process process = event.getProcess();
		this.processes.put(process.getId(), process);

		final UI ui = UI.getCurrent();
		if (ui != null) {
			/* @formatter:off */
			ui.getWindows().stream()
			  .filter(window -> window instanceof TerminalWindow)
			  .map(window -> (TerminalWindow) window)
			  .findFirst()
			  .ifPresent(terminal -> {
			  	terminal.setMessages(this.processes.values());

			  	if (!this.processManager.isAnyRunning()) {
			  		terminal.setClosable(true);
				    if (terminal.autoClose.getValue()) {
				    	terminal.close();
				    }
			  	}
			 });
			/* @formatter:on */
			ui.push();
		}
	}
}
