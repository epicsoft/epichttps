/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.common;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.ui.overview.OverviewView;
import lombok.Getter;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SaveCancelButtons extends HorizontalLayout {

	private static final long serialVersionUID = -9162965876620372467L;

	@Autowired
	private transient EpicMessageSource msg;

	@Getter
	private final Button save = new Button();
	@Getter
	private final Button cancel = new Button();

	@PostConstruct
	public void init() {
		this.save.setCaption(this.msg.getMessage("settings.button.save"));
		this.save.addStyleName(ValoTheme.BUTTON_TINY);

		this.cancel.setCaption(this.msg.getMessage("settings.button.cancel"));
		this.cancel.addStyleName(ValoTheme.BUTTON_TINY);
		this.cancel.addStyleName(ValoTheme.BUTTON_DANGER);
		this.cancel.addClickListener(e -> UI.getCurrent().getNavigator().navigateTo(OverviewView.NAME));

		this.setSpacing(true);
		this.addComponents(this.save, this.cancel);
	}

	public void setButtonsEnabled(final Boolean enabled) {
		this.save.setEnabled(enabled);
		this.cancel.setEnabled(enabled);
	}
}