/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.domain.Domain;
import de.epicsoft.epichttps.key.Key;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@UIScope
@SpringComponent
public class ViewHelper implements Serializable {

	private static final long serialVersionUID = 2029064491789172933L;

	@Autowired
	private transient EpicMessageSource msg;
	@Autowired
	private transient TerminalWindowManager terminalWindowManager;

	public HorizontalLayout buildHeader(final String caption) {
		Assert.hasText(caption, "Caption may be not empty");

		final Label headerText = new Label();
		headerText.setValue(caption);
		return this.buildHeader(headerText);
	}

	public HorizontalLayout buildHeader(final Label title) {
		Assert.notNull(title, "Title may be not null");

		title.addStyleName(ValoTheme.LABEL_H3);
		title.addStyleName(ValoTheme.LABEL_BOLD);

		final HorizontalLayout buttons = this.buildHeaderButtons();

		final HorizontalLayout header = new HorizontalLayout();
		header.addComponents(title, buttons);
		header.setComponentAlignment(title, Alignment.MIDDLE_LEFT);
		header.setComponentAlignment(buttons, Alignment.MIDDLE_RIGHT);
		header.addStyleName(ValoTheme.LAYOUT_CARD);
		header.setHeightUndefined();
		header.setWidth(100, Unit.PERCENTAGE);
		header.setMargin(new MarginInfo(false, true, false, true));
		return header;
	}

	public VerticalLayout buildContent() {
		final VerticalLayout content = new VerticalLayout();
		content.setSpacing(true);
		content.addStyleName(ValoTheme.LAYOUT_WELL);
		return content;
	}

	public List<Component> buildDomainWarnings(final Domain domain) {
		Assert.notNull(domain, "Domain may be not null");

		final List<Component> components = new ArrayList<>();
		/* @formatter:off */
		final boolean backupKeyExists = domain.getBackupKeys().stream()
			.filter(key -> !key.isExpire())
		  .filter(Key::getKeyFileExists)
		  .map(Key::getKeyFileExists)
		  .findAny()
		    .orElse(false);
    /* @formatter:on */
		if (backupKeyExists) {
			final Label backupKeyWarn = new Label(String.format("%s  %s", VaadinIcons.WARNING.getHtml(), this.msg.getMessage("domain.warn.backup.key.exists")), ContentMode.HTML);
			backupKeyWarn.addStyleName(ValoTheme.LABEL_BOLD);
			backupKeyWarn.addStyleName(ValoTheme.LABEL_TINY);
			components.add(backupKeyWarn);
		}
		return components;
	}

	private HorizontalLayout buildHeaderButtons() {
		final Button logout = new Button();
		logout.setDescription(this.msg.getMessage("general.logout"));
		logout.setIcon(VaadinIcons.SIGN_OUT);
		logout.addStyleName(ValoTheme.BUTTON_ICON_ONLY);
		logout.addStyleName(ValoTheme.BUTTON_DANGER);
		logout.addClickListener(event -> {
			UI.getCurrent().getSession().close();
			UI.getCurrent().getPage().setLocation(Resource.LOGOUT);
		});

		final Button terminal = new Button();
		terminal.setDescription(this.msg.getMessage("general.terminal.show"));
		terminal.setIcon(VaadinIcons.TERMINAL);
		terminal.addStyleName(ValoTheme.BUTTON_ICON_ONLY);
		terminal.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		// terminal.addClickListener(event -> UI.getCurrent().addWindow(this.terminalWindow));
		// terminal.addClickListener(event -> this.terminalWindow.showWindow(UI.getCurrent(), this.terminalWindow));
		terminal.addClickListener(event -> this.terminalWindowManager.showTerminal(UI.getCurrent()));

		final HorizontalLayout buttons = new HorizontalLayout(terminal, logout);
		buttons.setSpacing(true);
		buttons.setMargin(false);
		return buttons;
	}
}