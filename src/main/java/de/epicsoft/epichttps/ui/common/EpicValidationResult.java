/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.common;

import java.util.Optional;

import org.springframework.util.Assert;

import com.vaadin.data.ValidationResult;
import com.vaadin.shared.ui.ErrorLevel;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public class EpicValidationResult implements ValidationResult {

  private static final long serialVersionUID = 1395230762872497579L;

  private final Boolean error;

  private final String message;

  /**
   * Konstruktor darf nur ueber die Methoden {@link EpicValidationResult#ok()} und {@link EpicValidationResult#error(String)} aufgerufen werden.
   */
  private EpicValidationResult() {
    this.error = false;
    this.message = null;
  }

  /**
   * Konstruktor darf nur ueber die Methoden {@link EpicValidationResult#ok()} und {@link EpicValidationResult#error(String)} aufgerufen werden.
   */
  private EpicValidationResult(final String errorMessage) {
    this.error = true;
    this.message = errorMessage;
  }

  /**
   * Liefert ein gueltiges Ergebnis.
   *
   * @return {@link EpicValidationResult}
   */
  public static EpicValidationResult ok() {
    return new EpicValidationResult();
  }

  /**
   * Liefert eini fehlerhaftes Ergebnis mkit der uebergebenen Naschricht.
   *
   * @param errorMessage
   *          {@link String} - Fehlernachricht
   * @return {@link EpicValidationResult}
   */
  public static EpicValidationResult error(final String errorMessage) {
    Assert.hasText(errorMessage, "Error message may be not empty");

    return new EpicValidationResult(errorMessage);
  }

  @Override
  public String getErrorMessage() {
    return this.message;
  }

  @Override
  public boolean isError() {
    return this.error;
  }

  @Override
  public Optional<ErrorLevel> getErrorLevel() {
    return Optional.of(ErrorLevel.ERROR);
  }
}