/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.ui.common;

import java.util.function.Predicate;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.vaadin.navigator.View;
import com.vaadin.spring.access.ViewAccessControl;
import com.vaadin.ui.UI;

import de.epicsoft.epichttps.configuration.SecurityConfig;
import de.epicsoft.epichttps.ui.DashboardUI;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public interface EpicView extends View, ViewAccessControl {

	final String ROLE_ADMIN = SecurityConfig.ROLE_PREFIX + SecurityConfig.ROLE_ADMIN;

	@Override
	public default boolean isAccessGranted(final UI ui, final String beanName) {
		final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		return ui instanceof DashboardUI && auth != null && !SecurityConfig.ANONYMOUS_USER.equals(auth.getName()) && auth.isAuthenticated()
				&& auth.getAuthorities().stream().map(GrantedAuthority::getAuthority).anyMatch(Predicate.isEqual(ROLE_ADMIN));
	}
}