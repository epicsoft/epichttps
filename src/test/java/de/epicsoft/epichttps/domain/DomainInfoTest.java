/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.FileSystem;
import java.nio.file.Path;

import org.junit.Before;
import org.junit.Test;

import com.github.marschall.memoryfilesystem.MemoryFileSystemFactoryBean;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public class DomainInfoTest {

	private FileSystem fs;

	@Before
	public void before() {
		final MemoryFileSystemFactoryBean factory = new MemoryFileSystemFactoryBean();
		factory.setType(MemoryFileSystemFactoryBean.LINUX);
		this.fs = factory.getObject();
	}

	@Test
	public void build_withAllParameters_buildDomainInfo() {
		// GIVEN
		final String id = "example_com";
		final String name = "example.com";
		final Path dir = this.fs.getPath("/etc/letsencrypt/custom/example.com");
		// WHEN
		final DomainInfo info = DomainInfo.builder().id(id).name(name).directory(dir).build();
		// THEN
		assertThat(info).isNotNull();
		assertThat(info.getId()).isEqualTo(id);
		assertThat(info.getName()).isEqualTo(name);
		assertThat(info.getDirectory()).isEqualTo(dir);
	}

	@Test(expected = NullPointerException.class)
	public void build_withoutId_exception() {
		// GIVEN
		final String name = "example.com";
		final Path dir = this.fs.getPath("/etc/letsencrypt/custom/example.com");
		// WHEN
		DomainInfo.builder().name(name).directory(dir).build();
	}

	@Test(expected = NullPointerException.class)
	public void build_withoutName_exception() {
		// GIVEN
		final String id = "example_com";
		final Path dir = this.fs.getPath("/etc/letsencrypt/custom/example.com");
		// WHEN
		DomainInfo.builder().id(id).directory(dir).build();
	}

	@Test(expected = NullPointerException.class)
	public void build_withoutDirectory_exception() {
		// GIVEN
		final String id = "example_com";
		final String name = "example.com";
		// WHEN
		DomainInfo.builder().id(id).name(name).build();
	}

	@Test
	public void getPublicSuffix_fromExampleDomain_returnOnlyPublicSuffix() {
		// GIVEN
		final Path dir = this.fs.getPath("/etc/letsencrypt/custom/sub.example.com");
		final DomainInfo info = DomainInfo.builder().id("sub_example_com").name("sub.example.com").directory(dir).build();
		// WHEN
		final String publicSuffix = info.getPublicSuffix();
		// THEN
		assertThat(publicSuffix).isNotNull();
		assertThat(publicSuffix).isEqualTo("com");
	}

	@Test
	public void getPublicSuffix_fromCoUkDomain_returnOnlyPublicSuffix() {
		// GIVEN
		final Path dir = this.fs.getPath("/etc/letsencrypt/custom/sub.example.co.ukk");
		final DomainInfo info = DomainInfo.builder().id("sub_example_co_uk").name("sub.example.co.uk").directory(dir).build();
		// WHEN
		final String publicSuffix = info.getPublicSuffix();
		// THEN
		assertThat(publicSuffix).isNotNull();
		assertThat(publicSuffix).isEqualTo("co.uk");
	}

	@Test
	public void getPrivateDomain_fromExampleDomain_returnOnlygetPrivateDomain() {
		// GIVEN
		final Path dir = this.fs.getPath("/etc/letsencrypt/custom/sub.example.com");
		final DomainInfo info = DomainInfo.builder().id("sub_example_com").name("sub.example.com").directory(dir).build();
		// WHEN
		final String privateDomain = info.getPrivateDomain();
		// THEN
		assertThat(privateDomain).isNotNull();
		assertThat(privateDomain).isEqualTo("example.com");
	}

	@Test
	public void getPrivateDomain_fromCoUkDomain_returnOnlygetPrivateDomain() {
		// GIVEN
		final Path dir = this.fs.getPath("/etc/letsencrypt/custom/sub.example.co.uk");
		final DomainInfo info = DomainInfo.builder().id("sub_example_co_uk").name("sub.example.co.uk").directory(dir).build();
		// WHEN
		final String privateDomain = info.getPrivateDomain();
		// THEN
		assertThat(privateDomain).isNotNull();
		assertThat(privateDomain).isEqualTo("example.co.uk");
	}
}
