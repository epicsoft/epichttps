/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Path;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.env.MockEnvironment;

import com.github.marschall.memoryfilesystem.MemoryFileSystemFactoryBean;

import de.epicsoft.epichttps.Profiles;
import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.common.exception.DomainInfoNotFoundException;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public class DomainInfoServiceTest {

	private DomainInfoService domainInfoService;
	private SettingsService settingsService;
	private FileSystemAPI fs;

	@Before
	public void before() {
		final MemoryFileSystemFactoryBean factory = new MemoryFileSystemFactoryBean();
		factory.setType(MemoryFileSystemFactoryBean.LINUX);
		this.fs = new FileSystemAPI(factory.getObject());

		final MockEnvironment env = new MockEnvironment();
		env.setDefaultProfiles(Profiles.TEST);
		env.setProperty(SettingsKey.CONFIG_FILE_NAME.getName(), "config.properties");
		env.setProperty(SettingsKey.LE_ROOT_DIR.getName(), "/etc/letsencrypt");
		env.setProperty(SettingsKey.CUSTOM_SUBDIR.getName(), "custom");
		env.setProperty(SettingsKey.LE_GENERAL_SUBDIR.getName(), "globals");
		env.setProperty(SettingsKey.DOMAIN_KEY_SUBDIR.getName(), "keys");
		env.setProperty(SettingsKey.DOMAIN_CERT_SUBDIR.getName(), "certs");

		this.settingsService = new SettingsService(env, this.fs, env);

		this.domainInfoService = new DomainInfoService(this.settingsService, this.fs);
	}

	@Test
	public void create_withExampleDomain_newDomainInfo() {
		// GIVEN
		final String name = "example.com";
		// WHEN
		final DomainInfo info = this.domainInfoService.create(name);
		// THEN
		assertThat(info).isNotNull();
		assertThat(info.getId()).isEqualTo("example_com");
		assertThat(info.getName()).isEqualTo(name);
		// AND
		final Path dir = info.getDirectory();
		assertThat(dir).isNotNull();
		assertThat(dir.toString()).isEqualTo("/etc/letsencrypt/custom/example.com");
	}

	@Test(expected = IllegalArgumentException.class)
	public void create_withNull_exception() {
		// GIVEN
		final String name = null;
		// WHEN
		this.domainInfoService.create(name);
	}

	@Test(expected = IllegalArgumentException.class)
	public void create_withInvalidDomain_exception() {
		// GIVEN
		final String name = "notDomain";
		// WHEN
		this.domainInfoService.create(name);
	}

	@Test
	public void getAll_fromFilledFileSystem_returnDomainInfos() {
		// GIVEN
		this.fs.createDirectory(this.fs.getPath("/etc/letsencrypt", "custom", "global"));
		this.fs.createDirectory(this.fs.getPath("/etc/letsencrypt", "custom", "example.com"));
		this.fs.createDirectory(this.fs.getPath("/etc/letsencrypt", "custom", "epicsoft.de"));
		// WHEN
		final Set<DomainInfo> infos = this.domainInfoService.getAll();
		// THEN
		assertThat(infos).isNotNull();
		assertThat(infos.size()).isEqualTo(2);
		// AND
		for (final DomainInfo info : infos) {
			final String id = info.getId();
			assertThat(id).isNotNull();
			assertThat(id.equals("example_com") || id.equals("epicsoft_de")).isTrue();
		}
	}

	@Test
	public void getAll_withoutDomains_returnDomainInfos() {
		// GIVEN
		this.fs.createDirectory(this.fs.getPath("/etc/letsencrypt", "custom", "global"));
		// WHEN
		final Set<DomainInfo> infos = this.domainInfoService.getAll();
		// THEN
		assertThat(infos).isNotNull();
		assertThat(infos.size()).isEqualTo(0);
	}

	@Test
	public void getAll_fromEmptyFileSystem_returnEmptyDomainInfos() {
		// WHEN
		final Set<DomainInfo> infos = this.domainInfoService.getAll();
		// THEN
		assertThat(infos).isNotNull();
		assertThat(infos.size()).isEqualTo(0);
	}

	@Test
	public void getbyId_fromExistingDomain_returnDomainInfo() {
		// GIVEN
		final String id = "example_com";
		this.fs.createDirectory(this.fs.getPath("/etc/letsencrypt", "custom", "global"));
		this.fs.createDirectory(this.fs.getPath("/etc/letsencrypt", "custom", "example.com"));
		// WHEN
		final DomainInfo info = this.domainInfoService.getById(id);
		// THEN
		assertThat(info).isNotNull();
		assertThat(info.getId()).isEqualTo(id);
		assertThat(info.getName()).isEqualTo("example.com");
		assertThat(info.getDirectory().toString()).isEqualTo("/etc/letsencrypt/custom/example.com");
	}

	@Test(expected = DomainInfoNotFoundException.class)
	public void getById_fromNotExistingDomain_exception() {
		// GIVEN
		final String id = "epicsoft_de";
		this.fs.createDirectory(this.fs.getPath("/etc/letsencrypt", "custom", "global"));
		this.fs.createDirectory(this.fs.getPath("/etc/letsencrypt", "custom", "example.com"));
		// WHEN
		this.domainInfoService.getById(id);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getById_withNullId_exception() {
		// GIVEN
		final String id = null;
		// WHEN
		this.domainInfoService.getById(id);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getById_withEmptyId_exception() {
		// GIVEN
		final String id = "";
		// WHEN
		this.domainInfoService.getById(id);
	}
}
