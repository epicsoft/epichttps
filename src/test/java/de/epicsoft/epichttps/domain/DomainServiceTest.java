/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.nio.file.Path;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.mock.env.MockEnvironment;

import com.github.marschall.memoryfilesystem.MemoryFileSystemFactoryBean;

import de.epicsoft.epichttps.Profiles;
import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.common.exception.DomainAlreadyExistsException;
import de.epicsoft.epichttps.common.exception.DomainNotFoundException;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public class DomainServiceTest {

	private DomainService domainService;
	private SettingsService settingsService;
	private FileSystemAPI fs;
	private ApplicationEventPublisher publisher;

	@Before
	public void before() {
		final EpicMessageSource msg = mock(EpicMessageSource.class);
		when(msg.getMessage(anyString(), anyString())).thenReturn("test message");

		final MemoryFileSystemFactoryBean factory = new MemoryFileSystemFactoryBean();
		factory.setType(MemoryFileSystemFactoryBean.LINUX);
		this.fs = new FileSystemAPI(factory.getObject());

		final MockEnvironment env = new MockEnvironment();
		env.setDefaultProfiles(Profiles.TEST);
		env.setProperty(SettingsKey.CONFIG_FILE_NAME.getName(), "config.properties");
		env.setProperty(SettingsKey.LE_ROOT_DIR.getName(), "/etc/letsencrypt");
		env.setProperty(SettingsKey.CUSTOM_SUBDIR.getName(), "custom");
		env.setProperty(SettingsKey.LE_GENERAL_SUBDIR.getName(), "globals");
		env.setProperty(SettingsKey.DOMAIN_KEY_SUBDIR.getName(), "keys");
		env.setProperty(SettingsKey.DOMAIN_CERT_SUBDIR.getName(), "certs");

		this.settingsService = new SettingsService(env, this.fs, env);

		this.publisher = mock(ApplicationEventPublisher.class);

		this.domainService = new DomainService(this.settingsService, this.fs, this.publisher);
	}

	@Test
	public void create_withMinValues_newDomain() {
		// GIVEN
		final Path dir = this.fs.getPath("/etc/letsencrypt", "custom", "example.com");
		final DomainInfo info = DomainInfo.builder().id("example_com").name("example.com").directory(dir).build();
		// WHEN
		final Domain result = this.domainService.create(info);
		// THEN
		assertThat(result).isNotNull();
		assertThat(result.getInfo()).isNotNull();
		assertThat(result.getInfo().getName()).isEqualTo("example.com");
		// AND
		assertThat(this.fs.isDirectory(dir)).isTrue();
	}

	@Test(expected = IllegalArgumentException.class)
	public void create_withNullDomainInfo_exception() {
		// GIVEN
		final DomainInfo info = null;
		// WHEN
		this.domainService.create(info);
	}

	@Test(expected = DomainAlreadyExistsException.class)
	public void create_withExistsDirectory_exception() {
		// GIVEN
		final Path dir = this.fs.getPath("/etc/letsencrypt", "custom", "example.com");
		final DomainInfo info = DomainInfo.builder().id("example_com").name("example.com").directory(dir).build();
		this.fs.createDirectory(dir);
		// WHEN
		this.domainService.create(info);
	}

	@Test
	public void get_byDomainInfo_returnExistsDmain() {
		// GIVEN
		final String id = "example_com";
		final String name = "example.com";
		final Path dir = this.fs.getPath("/etc/letsencrypt/custom/example.com");
		final DomainInfo info = DomainInfo.builder().id(id).name(name).directory(dir).build();
		this.fs.createDirectory(this.fs.getPath("/etc/letsencrypt", "custom", name));
		// WHEN
		final Domain domain = this.domainService.get(info);
		// THEN
		assertThat(domain).isNotNull();
		assertThat(domain.getInfo()).isEqualTo(info);
	}

	@Test(expected = IllegalArgumentException.class)
	public void get_withNullValue_exception() {
		// GIVEN
		final DomainInfo info = null;
		// WHEN
		this.domainService.get(info);
	}

	@Test(expected = DomainNotFoundException.class)
	public void get_notExistingDomain_exception() {
		// GIVEN
		final String id = "example_com";
		final String name = "example.com";
		final Path dir = this.fs.getPath("/etc/letsencrypt/custom/example.com");
		final DomainInfo info = DomainInfo.builder().id(id).name(name).directory(dir).build();
		this.fs.removeDirectory(this.fs.getPath("/etc/letsencrypt", "custom", name));
		// WHEN
		this.domainService.get(info);
	}
}