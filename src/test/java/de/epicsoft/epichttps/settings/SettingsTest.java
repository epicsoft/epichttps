/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.settings;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.github.marschall.memoryfilesystem.MemoryFileSystemFactoryBean;

import de.epicsoft.epichttps.domain.DomainInfo;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public class SettingsTest {

	private Settings envSettings;
	private Settings globalSettings;
	private Settings domainSettings;

	@SuppressWarnings("resource")
	@Before
	public void before() {
		final Properties env = new Properties();
		env.setProperty(SettingsKey.TEST.getName(), "envSettings");
		this.envSettings = new Settings(env);

		final Properties global = new Properties();
		global.setProperty(SettingsKey.TEST.getName(), "globalSettings");
		global.setProperty(SettingsKey.AUTH_DOMAIN.getName(), "TestValue");
		global.setProperty(SettingsKey.AUTH_PORT.getName(), "80");
		global.setProperty(SettingsKey.CSR_COUNTRY.getName(), "${epicsoft.auth.domain} at the beginning");
		global.setProperty(SettingsKey.CSR_LOCATION.getName(), "at the end ${epicsoft.auth.domain}");
		global.setProperty(SettingsKey.CSR_STATE.getName(), "middle ${epicsoft.auth.domain} text");
		global.setProperty(SettingsKey.CSR_STREET.getName(), "two ${epicsoft.auth.domain} in the ${epicsoft.auth.port} text");
		global.setProperty(SettingsKey.CSR_ORGANIZATION.getName(), "not ${placeholder.not.exists} exists");
		global.setProperty(SettingsKey.HPKP_ADD_PINS.getName(), "value1,value2,value3,value4,value5");
		global.setProperty(SettingsKey.HPKP_INCLUDE_SUBDOMAINS.getName(), "value1");
		// HPKP_REPORT_ENFORCE = null
		this.globalSettings = new Settings(env, global);

		final MemoryFileSystemFactoryBean factory = new MemoryFileSystemFactoryBean();
		factory.setType(MemoryFileSystemFactoryBean.LINUX);
		final FileSystem fs = factory.getObject();

		final Path path = fs.getPath("/etc/letsencrypt", "custom", "testdomain");
		final DomainInfo domainInfo = DomainInfo.builder().id("testdomain").name("TestDomain").directory(path).build();

		final Properties domain = new Properties();
		domain.setProperty(SettingsKey.TEST.getName(), "domainSettings");
		this.domainSettings = new Settings(env, global, domain, domainInfo);
	}

	@Test
	public void getProperties_fromEnvSettings_returnEnvProperties() {
		// WHEN
		final Properties properties = this.envSettings.getProperties();
		// THEN
		assertThat(properties).isNotNull();
		assertThat(properties.getProperty(SettingsKey.TEST.getName())).isNotNull();
		assertThat(properties.getProperty(SettingsKey.TEST.getName())).isEqualTo("envSettings");
	}

	@Test
	public void getProperties_fromGenenalSettings_returnGlobalProperties() {
		// WHEN
		final Properties properties = this.globalSettings.getProperties();
		// THEN
		assertThat(properties).isNotNull();
		assertThat(properties.getProperty(SettingsKey.TEST.getName())).isNotNull();
		assertThat(properties.getProperty(SettingsKey.TEST.getName())).isEqualTo("globalSettings");
	}

	@Test
	public void getProperties_fromDomainSettings_returnDomainProperties() {
		// WHEN
		final Properties properties = this.domainSettings.getProperties();
		// THEN
		assertThat(properties).isNotNull();
		assertThat(properties.getProperty(SettingsKey.TEST.getName())).isNotNull();
		assertThat(properties.getProperty(SettingsKey.TEST.getName())).isEqualTo("domainSettings");
	}

	@Test
	public void getDomainInfo_fromGlobalSettings_returnNull() {
		// WHEN
		final DomainInfo domainInfo = this.globalSettings.getDomainInfo();
		// THEN
		assertThat(domainInfo).isNull();
	}

	@Test
	public void getDomainInfo_fromDomainSettings_returnDomainInfo() {
		// WHEN
		final DomainInfo domainInfo = this.domainSettings.getDomainInfo();
		// THEN
		assertThat(domainInfo).isNotNull();
		assertThat(domainInfo.getId()).isEqualTo("testdomain");
	}

	@Test
	public void isEnv_fromAllSettings_returnTrueOnlyAtEnfSettings() {
		// WHEN
		final Boolean env = this.envSettings.isEnv();
		final Boolean global = this.globalSettings.isEnv();
		final Boolean domain = this.domainSettings.isEnv();
		// THEN
		assertThat(env).isTrue();
		assertThat(global).isFalse();
		assertThat(domain).isFalse();
	}

	@Test
	public void isGlobal_fromAllSettings_returnTrueOnlyAtGlobalSettings() {
		// WHEN
		final Boolean env = this.envSettings.isGlobal();
		final Boolean global = this.globalSettings.isGlobal();
		final Boolean domain = this.domainSettings.isGlobal();
		// THEN
		assertThat(env).isFalse();
		assertThat(global).isTrue();
		assertThat(domain).isFalse();
	}

	@Test
	public void isDomain_fromAllSettings_returnTrueOnlyAtDomainSettings() {
		// WHEN
		final Boolean env = this.envSettings.isDomain();
		final Boolean global = this.globalSettings.isDomain();
		final Boolean domain = this.domainSettings.isDomain();
		// THEN
		assertThat(env).isFalse();
		assertThat(global).isFalse();
		assertThat(domain).isTrue();
	}

	@Test
	public void get_fromEnvSettings_returnEnvValue() {
		// GIVEN
		final SettingsKey key = SettingsKey.TEST;
		// WHEN
		final String value = this.envSettings.get(key);
		// THEN
		assertThat(value).isNotNull();
		assertThat(value).isEqualTo("envSettings");
	}

	@Test
	public void get_fromGlobalSettings_returnGlobalValue() {
		// GIVEN
		final SettingsKey key = SettingsKey.TEST;
		// WHEN
		final String value = this.globalSettings.get(key);
		// THEN
		assertThat(value).isNotNull();
		assertThat(value).isEqualTo("globalSettings");
	}

	@Test
	public void get_fromDomainSettings_returnDomainValue() {
		// GIVEN
		final SettingsKey key = SettingsKey.TEST;
		// WHEN
		final String value = this.domainSettings.get(key);
		// THEN
		assertThat(value).isNotNull();
		assertThat(value).isEqualTo("domainSettings");
	}

	@Test(expected = IllegalArgumentException.class)
	public void get_withNullKey_exception() {
		// GIVEN
		final SettingsKey key = null;
		// WHEN
		this.globalSettings.get(key);
	}

	@Test
	public void getInt_fromGlobalSettings_returnIntegerValue() {
		// GIVEN
		final SettingsKey key = SettingsKey.TEST;
		final Integer value = 1234;
		this.globalSettings.set(key, value);
		// WHEN
		final Integer newValue = this.globalSettings.getInt(key);
		// THEN
		assertThat(newValue).isNotNull();
		assertThat(newValue).isEqualTo(value);
	}

	@Test
	public void getBool_fromGlobalSettings_returnBooleanValue() {
		// GIVEN
		final SettingsKey key = SettingsKey.TEST;
		final Boolean value = Boolean.TRUE;
		this.globalSettings.set(key, value);
		// WHEN
		final Boolean newValue = this.globalSettings.getBool(key);
		// THEN
		assertThat(newValue).isNotNull();
		assertThat(newValue).isEqualTo(value);
	}

	@Test
	public void getSet_fromCommaSeparatedList_returnSetWithSeveralValues() {
		// GIVEN
		final SettingsKey key = SettingsKey.HPKP_ADD_PINS;
		// WHEN
		final Set<String> set = this.globalSettings.getSet(key);
		// THEN
		assertThat(set).isNotNull();
		assertThat(set.size()).isEqualTo(5);
	}

	@Test
	public void getSet_fromOneValueList_returnSetWithOneValue() {
		// GIVEN
		final SettingsKey key = SettingsKey.HPKP_INCLUDE_SUBDOMAINS;
		// WHEN
		final Set<String> set = this.globalSettings.getSet(key);
		// THEN
		assertThat(set).isNotNull();
		assertThat(set.size()).isEqualTo(1);
	}

	@Test
	public void getSet_fromNullValueList_returnEmptySet() {
		// GIVEN
		final SettingsKey key = SettingsKey.HPKP_REPORT_ENFORCE;
		// WHEN
		final Set<String> set = this.globalSettings.getSet(key);
		// THEN
		assertThat(set).isNotNull();
		assertThat(set.size()).isEqualTo(0);
	}

	@Test(expected = RuntimeException.class)
	public void set_string_onEnvSettings_exception() {
		// GIVEN
		final SettingsKey key = SettingsKey.CONFIG_FILE_NAME;
		final String value = "test_config.properties";
		// WHEN
		this.envSettings.set(key, value);
	}

	@Test
	public void set_string_onGlobalSetting_setNewValue() {
		// GIVEN
		final SettingsKey key = SettingsKey.CONFIG_FILE_NAME;
		final String value = "global_test_config.properties";
		// WHEN
		this.globalSettings.set(key, value);
		// THEN
		final String newValue = this.globalSettings.get(key);
		assertThat(newValue).isNotNull();
		assertThat(newValue).isEqualTo(value);
	}

	@Test
	public void set_string_onDomainSetting_setNewValue() {
		// GIVEN
		final SettingsKey key = SettingsKey.CONFIG_FILE_NAME;
		final String value = "domain_test_config.properties";
		// WHEN
		this.domainSettings.set(key, value);
		// THEN
		final String newValue = this.domainSettings.get(key);
		assertThat(newValue).isNotNull();
		assertThat(newValue).isEqualTo(value);
	}

	@Test
	public void set_integer_onGlobalSettings_setNewIntegerValue() {
		// GIVEN
		final SettingsKey key = SettingsKey.TEST;
		final Integer value = 1234;
		// WHEN
		this.globalSettings.set(key, value);
		// THEN
		final Integer newValue = this.globalSettings.getInt(key);
		assertThat(newValue).isNotNull();
		assertThat(newValue).isEqualTo(value);
	}

	@Test
	public void set_boolean_onGlobalSettings_setNewBooleanValue() {
		// GIVEN
		final SettingsKey key = SettingsKey.TEST;
		final Boolean value = Boolean.TRUE;
		// WHEN
		this.globalSettings.set(key, value);
		// THEN
		final Boolean newValue = this.globalSettings.getBool(key);
		assertThat(newValue).isNotNull();
		assertThat(newValue).isEqualTo(value);
	}

	@Test
	public void set_ZoneDateTime_setValueSuccess() {
		// GIVEN
		final ZonedDateTime value = ZonedDateTime.now(ZoneOffset.UTC);
		final SettingsKey key = SettingsKey.KEY_EXPIRE_DATE;
		// WHEN
		this.globalSettings.set(key, value);
		// THEN
		final String time = this.globalSettings.get(key);
		assertThat(time).isNotNull();
		assertThat(time).isEqualTo(value.toString());
	}

	@Test
	public void set_nullZoneDateTime_setEmptyValue() {
		// GIVEN
		final ZonedDateTime value = null;
		final SettingsKey key = SettingsKey.KEY_EXPIRE_DATE;
		// WHEN
		this.globalSettings.set(key, value);
		// THEN
		final String time = this.globalSettings.get(key);
		assertThat(time).isNotNull();
		assertThat(time).isEmpty();
	}

	@Test
	public void set_set_withSeveralValues_setSetSuccess() {
		// GIVEN
		final SettingsKey key = SettingsKey.HPKP_ADD_PINS;
		final Set<String> values = new HashSet<>(Arrays.asList("v1,v2,v3,v4"));
		// WHEN
		this.globalSettings.set(key, values);
		// THEN
		final Set<String> newValues = this.globalSettings.getSet(key);
		assertThat(newValues).isNotNull();
		assertThat(newValues.size()).isEqualTo(4);
	}

	@Test
	public void set_set_withEmptyValues_setSetSuccess() {
		// GIVEN
		final SettingsKey key = SettingsKey.HPKP_ADD_PINS;
		final Set<String> values = new HashSet<>();
		// WHEN
		this.globalSettings.set(key, values);
		// THEN
		final Set<String> newValues = this.globalSettings.getSet(key);
		assertThat(newValues).isNotNull();
		assertThat(newValues.size()).isEqualTo(0);
	}

	@Test
	public void set_set_withNullValues_setSetSuccess() {
		// GIVEN
		final SettingsKey key = SettingsKey.HPKP_ADD_PINS;
		final Set<String> values = null;
		// WHEN
		this.globalSettings.set(key, values);
		// THEN
		final Set<String> newValues = this.globalSettings.getSet(key);
		assertThat(newValues).isNotNull();
		assertThat(newValues.size()).isEqualTo(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void set_set_withNullKey_exception() {
		// GIVEN
		final SettingsKey key = null;
		final Set<String> values = new HashSet<>(Arrays.asList("v1,v2,v3,v4"));
		// WHEN
		this.globalSettings.set(key, values);
	}

	@Test
	public void replaceVariables_onePlaceholderAtBeginning_replaceWithOtherValue() {
		// GIVEN
		final SettingsKey key = SettingsKey.CSR_COUNTRY;
		// WHEN
		final String value = this.globalSettings.get(key);
		// THEN
		assertThat(value).isNotNull();
		assertThat(value).isEqualTo("TestValue at the beginning");
	}

	@Test
	public void replaceVariables_onePlaceholderAtEnd_replaceWithOtherValue() {
		// GIVEN
		final SettingsKey key = SettingsKey.CSR_LOCATION;
		// WHEN
		final String value = this.globalSettings.get(key);
		// THEN
		assertThat(value).isNotNull();
		assertThat(value).isEqualTo("at the end TestValue");
	}

	@Test
	public void replaceVariables_onePlaceholderInMiddle_replaceWithOtherValue() {
		// GIVEN
		final SettingsKey key = SettingsKey.CSR_STATE;
		// WHEN
		final String value = this.globalSettings.get(key);
		// THEN
		assertThat(value).isNotNull();
		assertThat(value).isEqualTo("middle TestValue text");
	}

	@Test
	public void replaceVariables_twoPlaceholdersInMiddle_replaceWithOtherValues() {
		// GIVEN
		final SettingsKey key = SettingsKey.CSR_STREET;
		// WHEN
		final String value = this.globalSettings.get(key);
		// THEN
		assertThat(value).isNotNull();
		assertThat(value).isEqualTo("two TestValue in the 80 text");
	}

	@Test
	public void replaceVariables_withNotExistingPlaceholder_() {
		// GIVEN
		final SettingsKey key = SettingsKey.CSR_ORGANIZATION;
		// WHEN
		final String value = this.globalSettings.get(key);
		// THEN
		assertThat(value).isNotNull();
		assertThat(value).isEqualTo("not  exists");
	}
}
