/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.settings;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.Path;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.env.MockEnvironment;

import com.github.marschall.memoryfilesystem.MemoryFileSystemFactoryBean;

import de.epicsoft.epichttps.Profiles;
import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.domain.DomainInfo;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public class SettingsServiceTest {

	private SettingsService settingsService;
	private FileSystemAPI fs;

	@Before
	public void before() {
		final MemoryFileSystemFactoryBean factory = new MemoryFileSystemFactoryBean();
		factory.setType(MemoryFileSystemFactoryBean.LINUX);
		this.fs = new FileSystemAPI(factory.getObject());

		final MockEnvironment env = new MockEnvironment();
		env.setDefaultProfiles(Profiles.TEST);
		env.setProperty(SettingsKey.CONFIG_FILE_NAME.getName(), "config.properties");
		env.setProperty(SettingsKey.LE_ROOT_DIR.getName(), "/etc/letsencrypt");
		env.setProperty(SettingsKey.CUSTOM_SUBDIR.getName(), "custom");
		env.setProperty(SettingsKey.LE_GENERAL_SUBDIR.getName(), "globals");

		this.settingsService = new SettingsService(env, this.fs, env);
	}

	@Test
	public void getEnv_returnEnvironmentSettings() {
		// WHEN
		final Settings env = this.settingsService.getEnv();
		// THEN
		assertThat(env).isNotNull();
		assertThat(env.isEnv()).isTrue();
	}

	@Test
	public void getGlobal_withExistsConfigFile_returnGlobalSettingsWithValues() {
		// GIVEN
		final Settings settings = this.settingsService.getGlobal();
		settings.set(SettingsKey.TEST, "TestValue");
		this.settingsService.save(settings);
		// WHEN
		final Settings globalSettings = this.settingsService.getGlobal();
		// THEN
		assertThat(globalSettings).isNotNull();
		assertThat(globalSettings.getProperties()).isNotNull();
		assertThat(globalSettings.getProperties().isEmpty()).isFalse();
		// AND
		assertThat(globalSettings.get(SettingsKey.TEST)).isNotNull();
		assertThat(globalSettings.get(SettingsKey.TEST)).isEqualTo("TestValue");
		// AND
		assertThat(globalSettings.isGlobal()).isTrue();
	}

	@Test
	public void getGlobal_withNotExistsConfigFile_returnEmptyGlobalSettings() throws IOException {
		// WHEN
		final Settings globalSettings = this.settingsService.getGlobal();
		// THEN
		assertThat(globalSettings).isNotNull();
		assertThat(globalSettings.getProperties()).isNotNull();
		assertThat(globalSettings.getProperties().isEmpty()).isTrue();
		// AND
		assertThat(globalSettings.isGlobal()).isTrue();
	}

	@Test
	public void getDomain_withValidDomainInfo_returnDomainSettings() throws IOException {
		// GIVEN
		final Path path = this.fs.getPath("/etc/letsencrypt", "custom", "testdomain");
		final DomainInfo domainInfo = DomainInfo.builder().id("testdomain").name("TestDomain").directory(path).build();
		// WHEN
		final Settings domainSettings = this.settingsService.getDomain(domainInfo);
		// THEN
		assertThat(domainSettings).isNotNull();
		assertThat(domainSettings.isDomain()).isTrue();
	}

	@Test(expected = IllegalArgumentException.class)
	public void getDomain_withNullDomainInfo_exception() {
		// GIVEN
		final DomainInfo domainInfo = null;
		// WHEN
		this.settingsService.getDomain(domainInfo);
	}

	@Test
	public void save_domainSettings() {
		// GIVEN
		final Path path = this.fs.getPath("/etc/letsencrypt", "custom", "testdomain");
		final DomainInfo domainInfo = DomainInfo.builder().id("testdomain").name("TestDomain").directory(path).build();
		final Settings domainSettings = this.settingsService.getDomain(domainInfo);
		domainSettings.set(SettingsKey.TEST, "TestDomainSettingsSave");
		// WHEN
		this.settingsService.save(domainSettings);
		// THEN
		final Settings loadedDomainSettings = this.settingsService.getDomain(domainInfo);
		assertThat(loadedDomainSettings).isNotNull();
		// AND
		assertThat(loadedDomainSettings.get(SettingsKey.TEST)).isNotNull();
		assertThat(loadedDomainSettings.get(SettingsKey.TEST)).isEqualTo("TestDomainSettingsSave");
		// AND
		assertThat(loadedDomainSettings.isDomain()).isTrue();
	}

	@Test
	public void save_globalSettings() {
		// GIVEN
		final Settings globalSettings = this.settingsService.getGlobal();
		globalSettings.set(SettingsKey.TEST, "TestGlobalSettingsSave");
		// WHEN
		this.settingsService.save(globalSettings);
		// THEN
		final Settings loadedGlobalSettings = this.settingsService.getGlobal();
		assertThat(loadedGlobalSettings).isNotNull();
		// AND
		assertThat(loadedGlobalSettings.get(SettingsKey.TEST)).isNotNull();
		assertThat(loadedGlobalSettings.get(SettingsKey.TEST)).isEqualTo("TestGlobalSettingsSave");
		// AND
		assertThat(loadedGlobalSettings.isGlobal()).isTrue();
	}

	@Test(expected = IllegalArgumentException.class)
	public void save_envSettings_exception() {
		// GIVEN
		final Settings env = this.settingsService.getEnv();
		// WHEN
		this.settingsService.save(env);
	}
}
