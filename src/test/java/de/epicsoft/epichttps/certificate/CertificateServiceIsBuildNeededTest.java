/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.certificate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.nio.file.Path;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.env.MockEnvironment;

import com.github.marschall.memoryfilesystem.MemoryFileSystemFactoryBean;
import com.google.common.collect.ImmutableSet;

import de.epicsoft.epichttps.Profiles;
import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.common.OpenSslAPI;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.key.Key;
import de.epicsoft.epichttps.key.KeyService;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public class CertificateServiceIsBuildNeededTest {

	public static final String PIN = "C2PmBYkr6RsDT25toxzzJK0LDWjorZzO1dymKmbBaW8=";

	private FileSystemAPI fs;
	private SettingsService settingsService;
	private LetsEncryptAPI le;
	private CertificateService certificateService;
	private OpenSslAPI openSsl;
	private KeyService keyService;

	private DomainInfo info;
	private Key privateKey;

	@Before
	public void before() {
		final MemoryFileSystemFactoryBean factory = new MemoryFileSystemFactoryBean();
		factory.setType(MemoryFileSystemFactoryBean.LINUX);
		this.fs = new FileSystemAPI(factory.getObject());
		this.fs.createDirectory(this.fs.getPath("/etc/letsencrypt", "custom", "example.com", "certs"));

		final MockEnvironment env = new MockEnvironment();
		env.setDefaultProfiles(Profiles.TEST);
		env.setProperty(SettingsKey.CONFIG_FILE_NAME.getName(), "config.properties");
		env.setProperty(SettingsKey.LE_ROOT_DIR.getName(), "/etc/letsencrypt");
		env.setProperty(SettingsKey.CUSTOM_SUBDIR.getName(), "custom");
		env.setProperty(SettingsKey.LE_GENERAL_SUBDIR.getName(), "globals");
		env.setProperty(SettingsKey.DOMAIN_CERT_SUBDIR.getName(), "certs");
		env.setProperty(SettingsKey.CERT_RENEW_BEFORE_EXPIRY_DAYS.getName(), "10");
		env.setProperty(SettingsKey.DOMAIN_ALTERNATIVE_NAMES.getName(), "example.com,sub.example.com");
		env.setProperty(SettingsKey.CERT_RSA_KEY_SIZE.getName(), "4096");
		env.setProperty(SettingsKey.LE_DRY_MODE.getName(), "true");

		this.settingsService = new SettingsService(env, this.fs, env);

		this.le = mock(LetsEncryptAPI.class);
		this.openSsl = mock(OpenSslAPI.class);

		this.privateKey = mock(Key.class);
		when(this.privateKey.getPin()).thenReturn(PIN);

		this.keyService = mock(KeyService.class);
		when(this.keyService.getValidPrivateKeys(any())).thenReturn(ImmutableSet.of(this.privateKey));

		this.certificateService = new CertificateService(this.fs, this.settingsService, this.le, this.openSsl, this.keyService);

		final Path dir = this.fs.getPath("/etc/letsencrypt", "custom", "example.com");
		this.info = DomainInfo.builder().id("example_com").name("example.com").directory(dir).build();
	}

	@Test
	public void isBuildNeeded_validDateAndSameAltNames_buildNotNeeded() {
		// GIVEN
		final Certificate current = this.buildCertificate(null, null, null);
		// WHEN
		final Boolean result = this.certificateService.isBuildNeeded(this.info, current);
		// THEN
		assertThat(result).isFalse();
	}

	@Test
	public void isBuildNeeded_validDateAndAltNamesLittle_buildNeeded() {
		// GIVEN
		final Certificate current = this.buildCertificate(null, null, ImmutableSet.of("example.com"));
		// WHEN
		final Boolean result = this.certificateService.isBuildNeeded(this.info, current);
		// THEN
		assertThat(result).isTrue();
	}

	@Test
	public void isBuildNeeded_validDateAndAltNamesMuch_buildNeeded() {
		// GIVEN
		final Certificate current = this.buildCertificate(null, null, ImmutableSet.of("example.com", "sub.example.com", "much.example.com"));
		// WHEN
		final Boolean result = this.certificateService.isBuildNeeded(this.info, current);
		// THEN
		assertThat(result).isTrue();
	}

	@Test
	public void isBuildNeeded_validDateAndAltNamesOther_buildNeeded() {
		// GIVEN
		final Certificate current = this.buildCertificate(null, null, ImmutableSet.of("example.com", "other.example.com"));
		// WHEN
		final Boolean result = this.certificateService.isBuildNeeded(this.info, current);
		// THEN
		assertThat(result).isTrue();
	}

	@Test
	public void isBuildNeeded_earlyDateAndSameAltNames_buildNeeded() {
		// GIVEN
		final ZonedDateTime from = ZonedDateTime.now(ZoneOffset.UTC).plusDays(10);
		final ZonedDateTime until = ZonedDateTime.now(ZoneOffset.UTC).plusDays(100);
		final Certificate current = this.buildCertificate(from, until, null);
		// WHEN
		final Boolean result = this.certificateService.isBuildNeeded(this.info, current);
		// THEN
		assertThat(result).isTrue();
	}

	@Test
	public void isBuildNeeded_expiredDateAndSameAltNames_buildNeeded() {
		// GIVEN
		final ZonedDateTime from = ZonedDateTime.now(ZoneOffset.UTC).minusDays(100);
		final ZonedDateTime until = ZonedDateTime.now(ZoneOffset.UTC).minusDays(10);
		final Certificate current = this.buildCertificate(from, until, null);
		// WHEN
		final Boolean result = this.certificateService.isBuildNeeded(this.info, current);
		// THEN
		assertThat(result).isTrue();
	}

	@Test
	public void isBuildNeeded_renewBeforeExpiryOneDayAndSameAltNames_buildNeeded() {
		// GIVEN
		final ZonedDateTime from = ZonedDateTime.now(ZoneOffset.UTC).minusDays(81);
		final ZonedDateTime until = ZonedDateTime.now(ZoneOffset.UTC).plusDays(9);
		final Certificate current = this.buildCertificate(from, until, null);
		// WHEN
		final Boolean result = this.certificateService.isBuildNeeded(this.info, current);
		// THEN
		assertThat(result).isTrue();
	}

	@Test
	public void isBuildNeeded_renewBeforeExpirySameDayAndSameAltNames_buildNeeded() {
		// GIVEN
		final ZonedDateTime from = ZonedDateTime.now(ZoneOffset.UTC).minusDays(80);
		final ZonedDateTime until = ZonedDateTime.now(ZoneOffset.UTC).plusDays(10);
		final Certificate current = this.buildCertificate(from, until, null);
		// WHEN
		final Boolean result = this.certificateService.isBuildNeeded(this.info, current);
		// THEN
		assertThat(result).isTrue();
	}

	@Test
	public void isBuildNeeded_renewBeforeExpiryEarlyDayAndSameAltNames_buildNotNeeded() {
		// GIVEN
		final ZonedDateTime from = ZonedDateTime.now(ZoneOffset.UTC).minusDays(79);
		final ZonedDateTime until = ZonedDateTime.now(ZoneOffset.UTC).plusDays(11);
		final Certificate current = this.buildCertificate(from, until, null);
		// WHEN
		final Boolean result = this.certificateService.isBuildNeeded(this.info, current);
		// THEN
		assertThat(result).isFalse();
	}

	@Test
	public void isBuildNeeded_withWrongPrivateKeyPin_buildNeeded() {
		// GIVEN
		when(this.privateKey.getPin()).thenReturn("cUPcTAZWKaASuYWhhneDttWpY3oBAkE3h2+soZS7sWs=");
		final Certificate current = this.buildCertificate(null, null, null);
		// WHEN
		final Boolean result = this.certificateService.isBuildNeeded(this.info, current);
		// THEN
		assertThat(result).isTrue();
	}

	private Certificate buildCertificate(final ZonedDateTime from, final ZonedDateTime until, final ImmutableSet<String> alternativeNames) {
		/* @formatter:off */
		return Certificate.builder()
				.file(this.fs.getPath("/etc/letsencrypt", "custom", "example.com", "certs", "1234567890_fullchain.pem"))
				.name("example.com")
				.validFrom(from != null ? from : ZonedDateTime.now(ZoneOffset.UTC).minusDays(20))
				.validUntil(until != null ? until : ZonedDateTime.now(ZoneOffset.UTC).plusDays(70))
				.issuer("CN=example.com, O=example, C=US")
				.keyAlgorithm("RSA")
				.signatureAlgorithm("SHA256withRSA")
				.alternativeNames(alternativeNames != null ? alternativeNames : ImmutableSet.of("example.com", "sub.example.com"))
				.pin(PIN)
				.build();
		/* @formatter:on */
	}
}
