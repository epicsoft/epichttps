/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.certificate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import com.github.marschall.memoryfilesystem.MemoryFileSystemFactoryBean;

import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.settings.Settings;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import de.epicsoft.epichttps.terminal.Terminal;
import de.epicsoft.epichttps.testhelper.MockSettings;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public class LetsEncryptAPITest {

  private LetsEncryptAPI letsEncryptAPI;

  private Terminal terminal;

  private FileSystemAPI fs;

  @Before
  public void before() {
    final EpicMessageSource msg = mock(EpicMessageSource.class);
    when(msg.getMessage(anyString())).thenReturn("test message");
    when(msg.getMessage(anyString(), anyString())).thenReturn("test message");

    this.terminal = mock(Terminal.class);
    when(this.terminal.build(any())).thenReturn("test command");

    final Settings settings = new MockSettings();
    settings.set(SettingsKey.LE_EXCECUTION_FILE, "/opt/letsencrypt/letsencrypt-auto");
    final SettingsService settingsService = mock(SettingsService.class);
    when(settingsService.getGlobal()).thenReturn(settings);

    final MemoryFileSystemFactoryBean factory = new MemoryFileSystemFactoryBean();
    factory.setType(MemoryFileSystemFactoryBean.LINUX);
    this.fs = new FileSystemAPI(factory.getObject());

    this.letsEncryptAPI = new LetsEncryptAPI(this.terminal, settingsService, msg, this.fs);
  }

  @Test
  public void register_withValidEmailAndAgreeTos_excecuteCommand() {
    // GIVEN
    final String email = "test@example.com";
    final Boolean agreeTos = true;
    // WHEN
    this.letsEncryptAPI.register(email, agreeTos);
    // THEN
    verify(this.terminal, times(1)).execute(any());
  }

  @Test(expected = IllegalArgumentException.class)
  public void register_withEmptyEmail_exception() {
    // GIVEN
    final String email = "";
    final Boolean agreeTos = true;
    // WHEN
    this.letsEncryptAPI.register(email, agreeTos);
  }

  @Test(expected = IllegalArgumentException.class)
  public void register_withNullEmail_exception() {
    // GIVEN
    final String email = null;
    final Boolean agreeTos = true;
    // WHEN
    this.letsEncryptAPI.register(email, agreeTos);
  }

  @Test(expected = IllegalArgumentException.class)
  public void register_withInvalidEmail_exception() {
    // GIVEN
    final String email = "testexample.com";
    final Boolean agreeTos = true;
    // WHEN
    this.letsEncryptAPI.register(email, agreeTos);
  }

  @Test(expected = IllegalArgumentException.class)
  public void register_withInvalidEmail2_exception() {
    // GIVEN
    final String email = "test@examplecom";
    final Boolean agreeTos = true;
    // WHEN
    this.letsEncryptAPI.register(email, agreeTos);
  }

  @Test(expected = IllegalArgumentException.class)
  public void register_withRejectTos_exception() {
    // GIVEN
    final String email = "test@example.com";
    final Boolean agreeTos = false;
    // WHEN
    this.letsEncryptAPI.register(email, agreeTos);
  }

  @Test
  public void registerWithoutEmail_agreeTos_excecuteCommand() {
    // GIVEN
    final Boolean agreeTos = true;
    // WHEN
    this.letsEncryptAPI.registerWithoutEmail(agreeTos);
    // THEN
    verify(this.terminal, times(1)).execute(any());
  }

  @Test(expected = IllegalArgumentException.class)
  public void registerWithoutEmail_rejectTos_exception() {
    // GIVEN
    final Boolean agreeTos = false;
    // WHEN
    this.letsEncryptAPI.registerWithoutEmail(agreeTos);
  }
}