/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.common;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public class OpenSslAPITest {

	// private OpenSslAPI openSslAPI;
	// private FileSystemAPI fs;
	// private SettingsService settingsService;
	// private Terminal terminal;
	// private ProcessManager processManager;
	//
	// @Before
	// public void before() {
	// final MemoryFileSystemFactoryBean factory = new MemoryFileSystemFactoryBean();
	// factory.setType(MemoryFileSystemFactoryBean.LINUX);
	// this.fs = new FileSystemAPI(factory.getObject());
	//
	// final MockEnvironment env = new MockEnvironment();
	// env.setDefaultProfiles(Profiles.TEST);
	// env.setProperty(SettingsKey.CONFIG_FILE_NAME.getName(), "config.properties");
	// env.setProperty(SettingsKey.LE_ROOT_DIR.getName(), "/etc/letsencrypt");
	// env.setProperty(SettingsKey.CUSTOM_SUBDIR.getName(), "custom");
	// env.setProperty(SettingsKey.LE_GENERAL_SUBDIR.getName(), "globals");
	//
	// this.terminal = mock(Terminal.class);
	// when(this.terminal.build(any())).thenReturn("test command");
	//
	// this.processManager = mock(ProcessManager.class);
	//
	// this.openSslAPI = new OpenSslAPI(this.settingsService, this.terminal, this.fs, this.processManager);
	// }
}
