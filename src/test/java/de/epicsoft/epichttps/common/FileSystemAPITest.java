/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.common;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.List;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

import com.github.marschall.memoryfilesystem.MemoryFileSystemFactoryBean;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public class FileSystemAPITest {

	private FileSystemAPI fs;

	@Before
	public void before() {
		final MemoryFileSystemFactoryBean factory = new MemoryFileSystemFactoryBean();
		factory.setType(MemoryFileSystemFactoryBean.LINUX);
		this.fs = new FileSystemAPI(factory.getObject());
	}

	@Test
	public void getPath_forFile_createNewPathObject() {
		// GIVEN
		final String file = "/etc/letsencrypt/custom/global/config.properties";
		// WHEN
		final Path path = this.fs.getPath(file);
		// THEN
		assertThat(path).isNotNull();
		assertThat(path.toString()).isEqualTo(file);
	}

	@Test
	public void getPath_forDirectoryAndFile_createNewPathObject() {
		// GIVEN
		final String directory = "/etc/letsencrypt/custom/global";
		final String file = "config.properties";
		// WHEN
		final Path path = this.fs.getPath(directory, file);
		// THEN
		assertThat(path).isNotNull();
		assertThat(path.toString()).isEqualTo("/etc/letsencrypt/custom/global/config.properties");
	}

	@Test
	public void getPath_forDirectorySubdirectoryAndFile_createNewPathObject() {
		// GIVEN
		final String directory = "/etc/letsencrypt";
		final String sub1 = "custom";
		final String sub2 = "global";
		final String file = "config.properties";
		// WHEN
		final Path path = this.fs.getPath(directory, sub1, sub2, file);
		// THEN
		assertThat(path).isNotNull();
		assertThat(path.toString()).isEqualTo("/etc/letsencrypt/custom/global/config.properties");
	}

	@Test(expected = IllegalArgumentException.class)
	public void getPath_withNullValue_exception() {
		// GIVEN
		final String file = null;
		// WHEN
		this.fs.getPath(file);
	}

	@Test(expected = IllegalArgumentException.class)
	public void list_withNullDirectory_exception() {
		// GIVEN
		final Path directory = null;
		// WHEN
		this.fs.list(directory);
	}

	@Test
	public void list_withNotExistsDirectory_emptyResult() {
		// GIVEN
		final Path directory = this.fs.getPath("/etc/letsencrypt");
		// WHEN
		final List<Path> result = this.fs.list(directory);
		// THEN
		assertThat(result).isNotNull();
		assertThat(result.size()).isEqualTo(0);
	}

	@Test
	public void list_filesAndFoldersFromDirectory_returnContentFromDirectory() {
		// GIVEN
		final String directory = "/etc/letsencrypt/custom";
		final Path path = this.fs.getPath(directory);
		this.fs.createDirectory(this.fs.getPath(directory, "sub1"));
		this.fs.createDirectory(this.fs.getPath(directory, "sub2"));
		// WHEN
		final List<Path> content = this.fs.list(path);
		// THEN
		assertThat(content).isNotNull();
		assertThat(content.size()).isEqualTo(2);
		assertThat(content.toString()).contains("sub1");
		assertThat(content.toString()).contains("sub2");
	}

	@Test(expected = IllegalArgumentException.class)
	public void isDirectory_withNullPath_exception() {
		// GIVEN
		final Path path = null;
		// WHEN
		this.fs.isDirectory(path);
	}

	@Test
	public void isDirectory_withNotExistingPath_returnFalse() {
		// GIVEN
		final String directory = "/etc/letsencrypt/custom";
		this.fs.createDirectory(this.fs.getPath(directory, "sub1"));
		final Path notExistsPath = this.fs.getPath(directory, "sub2");
		// WHEN
		final Boolean exists = this.fs.isDirectory(notExistsPath);
		// THEN
		assertThat(exists).isNotNull();
		assertThat(exists).isEqualTo(Boolean.FALSE);
	}

	@Test
	public void isDirectory_withExistingPath_returnTrue() {
		// GIVEN
		final String directory = "/etc/letsencrypt/custom";
		final Path existsPath = this.fs.getPath(directory, "sub1");
		this.fs.createDirectory(existsPath);
		// WHEN
		final Boolean exists = this.fs.isDirectory(existsPath);
		// THEN
		assertThat(exists).isNotNull();
		assertThat(exists).isEqualTo(Boolean.TRUE);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createDirectory_withNullPath_exception() {
		// GIVEN
		final Path path = null;
		// WHEN
		this.fs.createDirectory(path);
	}

	@Test
	public void createDirectory_withValidPath_createNewDirectory() {
		// GIVEN
		final String directory = "/etc/letsencrypt/custom";
		final Path path = this.fs.getPath(directory);
		// AND
		assertThat(this.fs.isDirectory(path)).isFalse();
		// WHEN
		this.fs.createDirectory(path);
		// THEN
		assertThat(this.fs.isDirectory(path)).isTrue();
	}

	@Test
	public void createDirectory_allredyExists_noAlert() {
		// GIVEN
		final String directory = "/etc/letsencrypt/custom";
		final Path path = this.fs.getPath(directory);
		// AND
		assertThat(this.fs.isDirectory(path)).isFalse();
		// WHEN
		this.fs.createDirectory(path);
		this.fs.createDirectory(path);
		// THEN
		assertThat(this.fs.isDirectory(path)).isTrue();
	}

	@Test
	public void removeDirectory_withExistingDirectory_removeDirectory() {
		// GIVEN
		final String directory = "/etc/letsencrypt/custom";
		final Path path = this.fs.getPath(directory);
		this.fs.createDirectory(path);
		// AND
		assertThat(this.fs.isDirectory(path)).isTrue();
		// WHEN
		this.fs.removeDirectory(path);
		// THEN
		assertThat(this.fs.isDirectory(path)).isFalse();
	}

	@Test
	public void removeDirectory_withNotExistingDirectory_noAlert() {
		// GIVEN
		final String directory = "/etc/letsencrypt/custom";
		final Path path = this.fs.getPath(directory);
		// AND
		assertThat(this.fs.isDirectory(path)).isFalse();
		// WHEN
		this.fs.removeDirectory(path);
		// THEN
		assertThat(this.fs.isDirectory(path)).isFalse();
	}

	@Test(expected = IllegalArgumentException.class)
	public void removeDirectory_withNullPath_exception() {
		// GIVEN
		final Path path = null;
		// WHEN
		this.fs.removeDirectory(path);
	}

	@Test
	public void saveProperties_withValidValues_saveProperties() throws IOException {
		// GIVEN
		final Properties props = new Properties();
		props.setProperty("testKey", "testValue");
		final String directory = "/etc/letsencrypt/custom/global";
		final String file = "config.properties";
		final Path path = this.fs.getPath(directory, file);
		// WHEN
		this.fs.saveProperties(props, path);
		// THEN
		final Properties loadedProps = this.fs.loadProperties(path);
		assertThat(loadedProps).isNotNull();
		// AND
		assertThat(loadedProps.get("testKey")).isEqualTo("testValue");
	}

	@Test(expected = IllegalArgumentException.class)
	public void saveProperties_withNullProperties_exception() throws IOException {
		// GIVEN
		final Properties props = null;
		final String directory = "/etc/letsencrypt/custom/global";
		final String file = "config.properties";
		final Path path = this.fs.getPath(directory, file);
		// WHEN
		this.fs.saveProperties(props, path);
	}

	@Test(expected = IllegalArgumentException.class)
	public void saveProperties_withNullPath_exception() throws IOException {
		// GIVEN
		final Properties props = new Properties();
		props.setProperty("testKey", "testValue");
		final Path path = null;
		// WHEN
		this.fs.saveProperties(props, path);
	}

	@Test
	public void loadProperties_existsProperties_returnProperties() throws IOException {
		// GIVEN
		final Properties props = new Properties();
		props.setProperty("testKey", "testValue");
		final String directory = "/etc/letsencrypt/custom/global";
		final String file = "config.properties";
		final Path path = this.fs.getPath(directory, file);
		this.fs.saveProperties(props, path);
		// WHEN
		final Properties loadedProperties = this.fs.loadProperties(path);
		// THEN
		assertThat(loadedProperties).isNotNull();
		assertThat(loadedProperties.get("testKey")).isEqualTo("testValue");
	}

	@Test(expected = NoSuchFileException.class)
	public void loadProperties_notExistsProperties_exception() throws IOException {
		// GIVEN
		final String directory = "/etc/letsencrypt/custom/global";
		final String file = "config.properties";
		final Path path = this.fs.getPath(directory, file);
		// WHEN
		this.fs.loadProperties(path);
	}

	@Test(expected = IllegalArgumentException.class)
	public void loadProperties_withNullPath_exception() throws IOException {
		// GIVEN
		final Path path = null;
		// WHEN
		this.fs.loadProperties(path);
	}

	@Test
	public void saveFile_withNotExistsFile_createNewFileAndWriteContent() {
		// GIVEN
		final Path file = this.fs.getPath("/etc/letsencrypt/new/directory/test.file");
		final String content = "newTestContent";
		// WHEN
		this.fs.saveFile(file, content);
		// THEN
		assertThat(this.fs.isFile(file)).isTrue();
		// AND
		final String loadedContent = this.fs.getContent(file);
		assertThat(loadedContent).isNotNull();
		assertThat(loadedContent).isEqualTo(content);
	}

	@Test
	public void saveFile_withExistingFile_overrideContent() {
		// GIVEN
		final Path file = this.fs.getPath("/etc/letsencrypt/new/directory/test.file");
		final String contentOld = "oldLongerTestContent";
		final String contentNew = "newTestContent";
		// WHEN
		this.fs.saveFile(file, contentOld);
		this.fs.saveFile(file, contentNew);
		// THEN
		assertThat(this.fs.isFile(file)).isTrue();
		// AND
		final String loadedContent = this.fs.getContent(file);
		assertThat(loadedContent).isNotNull();
		assertThat(loadedContent).isEqualTo(contentNew);
	}

	@Test(expected = IllegalArgumentException.class)
	public void saveFile_withNullFile_exception() {
		// GIVEN
		final Path file = null;
		final String content = "TestContent";
		// WHEN
		this.fs.saveFile(file, content);
	}

	@Test(expected = IllegalArgumentException.class)
	public void saveFile_withNullContent_exception() {
		// GIVEN
		final Path file = this.fs.getPath("/etc/letsencrypt/new/directory/test.file");
		final String content = null;
		// WHEN
		this.fs.saveFile(file, content);
	}

	@Test
	public void isFile_withNotExistsFile_returnFalse() {
		// GIVEN
		final Path file = this.fs.getPath("/etc/letsencrypt/new/directory/not.exists");
		// WHEN
		final Boolean result = this.fs.isFile(file);
		// THEN
		assertThat(result).isFalse();
	}

	@Test
	public void isFile_withExistingFile_returnTrue() {
		// GIVEN
		final Path file = this.fs.getPath("/etc/letsencrypt/new/directory/file.exists");
		this.fs.saveFile(file, "");
		// WHEN
		final Boolean result = this.fs.isFile(file);
		// THEN
		assertThat(result).isTrue();
	}

	@Test(expected = IllegalArgumentException.class)
	public void isFile_withNullFile_exception() {
		// GIVEN
		final Path file = null;
		// WHEN
		this.fs.isFile(file);
	}

	@Test
	public void getContent_fromExistsFile_returnContent() {
		// GIVEN
		final Path file = this.fs.getPath("/etc/letsencrypt/new/directory/file.exists");
		final String content = "myTestContent";
		this.fs.saveFile(file, content);
		// WHEN
		final String readContent = this.fs.getContent(file);
		// THEN
		assertThat(readContent).isNotNull();
		assertThat(readContent).isEqualTo(content);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getContent_fromNotExistingFile_exception() {
		// GIVEN
		final Path file = this.fs.getPath("/etc/letsencrypt/new/directory/not.exists");
		// WHEN
		this.fs.getContent(file);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getContent_withNullFile_exception() {
		// GIVEN
		final Path file = null;
		// WHEN
		this.fs.getContent(file);
	}
}