/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.key;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.nio.file.Path;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.env.MockEnvironment;

import com.github.marschall.memoryfilesystem.MemoryFileSystemFactoryBean;
import com.google.common.collect.ImmutableSet;

import de.epicsoft.epichttps.Profiles;
import de.epicsoft.epichttps.common.EpicDateTime;
import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.common.OpenSslAPI;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import de.epicsoft.epichttps.terminal.ProcessManager;
import de.epicsoft.epichttps.terminal.Terminal;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public class KeyServiceCreateOrUpdateTest {

	private static final String DOMAIN_ID = "example_de";
	private static final String DOMAIN_NAME = "example.de";
	private static final String DOMAIN_PATH = "/etc/letencrypt/custom/example.com";

	private KeyService keyService;
	private SettingsService settingsService;
	private FileSystemAPI fs;
	private OpenSslAPI openSslApi;

	private DomainInfo info;
	private MockEnvironment env;

	@Before
	public void before() {
		final EpicMessageSource msg = mock(EpicMessageSource.class);
		when(msg.getMessage(anyString(), anyString())).thenReturn("test message");

		final MemoryFileSystemFactoryBean factory = new MemoryFileSystemFactoryBean();
		factory.setType(MemoryFileSystemFactoryBean.LINUX);
		this.fs = new FileSystemAPI(factory.getObject());
		this.fs.createDirectory(this.fs.getPath(DOMAIN_PATH, "keys"));

		this.env = new MockEnvironment();
		this.env.setDefaultProfiles(Profiles.TEST);
		this.env.setProperty(SettingsKey.LE_ROOT_DIR.getName(), "/etc/letsencrypt");
		this.env.setProperty(SettingsKey.CUSTOM_SUBDIR.getName(), "custom");
		this.env.setProperty(SettingsKey.LE_GENERAL_SUBDIR.getName(), "globals");
		this.env.setProperty(SettingsKey.CONFIG_FILE_NAME.getName(), "config.properties");
		this.env.setProperty(SettingsKey.DH_PARAM_BIT.getName(), "4096");
		this.env.setProperty(SettingsKey.DOMAIN_KEY_SUBDIR.getName(), "keys");
		this.env.setProperty(SettingsKey.PRIVATE_KEY_QUANTITY.getName(), "3");
		this.env.setProperty(SettingsKey.PRIVATE_KEY_PERIOD_VALIDITY_DAYS.getName(), "90");
		this.env.setProperty(SettingsKey.PRIVATE_KEY_TOLERANCE_DAYS.getName(), "15");
		this.env.setProperty(SettingsKey.BACKUP_KEY_ENABLE.getName(), "false");
		this.env.setProperty(SettingsKey.BACKUP_KEY_QUANTITY.getName(), "3");
		this.env.setProperty(SettingsKey.KEYS_SIZE.getName(), "1024");
		this.env.setProperty(SettingsKey.KEYS_ALGORITHM.getName(), "RSA");
		this.env.setProperty(SettingsKey.MESSAGE_DIGEST_ALGORITHM.getName(), "SHA-256");
		this.settingsService = new SettingsService(this.env, this.fs, this.env);

		this.info = DomainInfo.builder().id(DOMAIN_ID).name(DOMAIN_NAME).directory(this.fs.getPath(DOMAIN_PATH)).build();

		this.openSslApi = new OpenSslAPI(this.settingsService, mock(Terminal.class), this.fs, mock(ProcessManager.class));
		this.keyService = new KeyService(this.settingsService, this.openSslApi, this.fs, msg);
	}

	@After
	public void after() {
		EpicDateTime.reset();
	}

	@Test
	public void createOrUpdate_withValidDomainInfo_createPrivateKey() {
		// GIVEN before
		// WHEN
		this.keyService.createOrUpdate(this.info);
		// THEN
		final List<Path> files = this.fs.list(this.fs.getPath(DOMAIN_PATH, "keys"));
		assertThat(files.size()).isEqualTo(2);
		// AND
		final Map<String, Integer> count = this.countKeyFiles(files);
		assertThat(count.get("private_key")).isEqualTo(1);
		assertThat(count.get("private_properties")).isEqualTo(1);
		assertThat(count.get("backup_key")).isEqualTo(0);
		assertThat(count.get("backup_properties")).isEqualTo(0);
	}

	@Test
	public void createOrUpdate_createPrivateAndBackupKeys_createPrivateAndBackupFiles() {
		// GIVEN
		this.env.setProperty(SettingsKey.PRIVATE_KEY_ROTATION_ENABLE.getName(), "true");
		this.env.setProperty(SettingsKey.BACKUP_KEY_ENABLE.getName(), "true");
		// WHEN
		this.keyService.createOrUpdate(this.info);
		// THEN
		final List<Path> files = this.fs.list(this.fs.getPath(DOMAIN_PATH, "keys"));
		assertThat(files.size()).isEqualTo(12);
		// AND
		final Map<String, Integer> count = this.countKeyFiles(files);
		assertThat(count.get("private_key")).isEqualTo(3);
		assertThat(count.get("private_properties")).isEqualTo(3);
		assertThat(count.get("backup_key")).isEqualTo(3);
		assertThat(count.get("backup_properties")).isEqualTo(3);
		// AND
		/* @formatter:off */
		final List<Key> keys = this.keyService.getValidPrivateKeys(this.info).stream()
		  .sorted((key1, key2) -> key1.getExpireDate().compareTo(key2.getExpireDate()))
		  .collect(Collectors.toList());
		/* @formatter:on */
		ZonedDateTime nextExpire = EpicDateTime.now().plusDays(90);
		for (final Key key : keys) {
			assertThat(nextExpire.getYear()).isEqualTo(key.getExpireDate().getYear());
			assertThat(nextExpire.getDayOfYear()).isEqualTo(key.getExpireDate().getDayOfYear());
			nextExpire = nextExpire.plusDays(90);
		}
	}

	@Test
	public void createOrUpdate_privateKeyRotation_rotate() {
		// GIVEN
		this.env.setProperty(SettingsKey.PRIVATE_KEY_ROTATION_ENABLE.getName(), "true");
		this.env.setProperty(SettingsKey.BACKUP_KEY_ENABLE.getName(), "true");
		// WHEN
		this.keyService.createOrUpdate(this.info);
		EpicDateTime.setClock(EpicDateTime.now().plusDays(75)); // -15 days tolerance
		this.keyService.createOrUpdate(this.info);
		// THEN
		final List<Path> files = this.fs.list(this.fs.getPath(DOMAIN_PATH, "keys"));
		assertThat(files.size()).isEqualTo(14);
		// AND
		final Map<String, Integer> count = this.countKeyFiles(files);
		assertThat(count.get("private_key")).isEqualTo(4);
		assertThat(count.get("private_properties")).isEqualTo(4);
		assertThat(count.get("backup_key")).isEqualTo(3);
		assertThat(count.get("backup_properties")).isEqualTo(3);
		// AND
		final ImmutableSet<Key> privateKeys = this.keyService.getValidPrivateKeys(this.info);
		assertThat(privateKeys.size()).isEqualTo(3);
		// AND
		final ImmutableSet<Key> backupKeys = this.keyService.getValidBackupKeys(this.info);
		assertThat(backupKeys.size()).isEqualTo(3);
	}

	@Test
	public void createOrUpdate_privateKeyRotation_rotate2() {
		// GIVEN
		this.env.setProperty(SettingsKey.PRIVATE_KEY_ROTATION_ENABLE.getName(), "true");
		this.env.setProperty(SettingsKey.BACKUP_KEY_ENABLE.getName(), "true");
		// WHEN
		this.keyService.createOrUpdate(this.info);
		EpicDateTime.setClock(EpicDateTime.now().plusDays(75)); // -15 days tolerance
		this.keyService.createOrUpdate(this.info);
		EpicDateTime.setClock(EpicDateTime.now().plusDays(90)); // +1 extra
		this.keyService.createOrUpdate(this.info);
		// THEN
		final List<Path> files = this.fs.list(this.fs.getPath(DOMAIN_PATH, "keys"));
		assertThat(files.size()).isEqualTo(16);
		// AND
		final Map<String, Integer> count = this.countKeyFiles(files);
		assertThat(count.get("private_key")).isEqualTo(5);
		assertThat(count.get("private_properties")).isEqualTo(5);
		assertThat(count.get("backup_key")).isEqualTo(3);
		assertThat(count.get("backup_properties")).isEqualTo(3);
		// AND
		final ImmutableSet<Key> privateKeys = this.keyService.getValidPrivateKeys(this.info);
		assertThat(privateKeys.size()).isEqualTo(3);
		// AND
		final ImmutableSet<Key> backupKeys = this.keyService.getValidBackupKeys(this.info);
		assertThat(backupKeys.size()).isEqualTo(3);
	}

	private Map<String, Integer> countKeyFiles(final Collection<Path> files) {
		final Map<String, Integer> count = new HashMap<>();
		count.put("private_key", 0);
		count.put("private_properties", 0);
		count.put("backup_key", 0);
		count.put("backup_properties", 0);
		files.stream().forEach(file -> {
			if (file.getFileName().toString().matches("^private_key_[a-z0-9]{32}\\.pem$")) {
				count.put("private_key", count.get("private_key") + 1);
			}
			if (file.getFileName().toString().matches("^private_key_[a-z0-9]{32}\\.properties$")) {
				count.put("private_properties", count.get("private_properties") + 1);
			}
			if (file.getFileName().toString().matches("^backup_key_[a-z0-9]{32}\\.pem$")) {
				count.put("backup_key", count.get("backup_key") + 1);
			}
			if (file.getFileName().toString().matches("^backup_key_[a-z0-9]{32}\\.properties$")) {
				count.put("backup_properties", count.get("backup_properties") + 1);
			}
		});
		return count;
	}
}
