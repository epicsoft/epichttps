/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.key;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mock.env.MockEnvironment;

import com.github.marschall.memoryfilesystem.MemoryFileSystemFactoryBean;
import com.google.common.collect.ImmutableSet;

import de.epicsoft.epichttps.Profiles;
import de.epicsoft.epichttps.common.EpicDateTime;
import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.common.OpenSslAPI;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;
import de.epicsoft.epichttps.terminal.ProcessManager;
import de.epicsoft.epichttps.terminal.Terminal;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */

public class KeyServiceGetPrivateKeysTest {

	private static final Logger log = LoggerFactory.getLogger(KeyServiceGetPrivateKeysTest.class);

	private static final String DOMAIN_ID = "example_de";
	private static final String DOMAIN_NAME = "example.de";
	private static final String DOMAIN_PATH = "/etc/letencrypt/custom/example.com";

	private KeyService keyService;
	private SettingsService settingsService;
	private FileSystemAPI fs;
	private OpenSslAPI openSslApi;

	private DomainInfo info;
	private MockEnvironment env;
	private List<Key> keys;
	private Key key1;
	private Key key2;
	private Key key3;
	private Key key4;
	private Key key5;
	private Key key6;

	@Before
	public void before() {
		final EpicMessageSource msg = mock(EpicMessageSource.class);
		when(msg.getMessage(anyString(), anyString())).thenReturn("test message");

		final MemoryFileSystemFactoryBean factory = new MemoryFileSystemFactoryBean();
		factory.setType(MemoryFileSystemFactoryBean.LINUX);
		this.fs = new FileSystemAPI(factory.getObject());
		this.fs.createDirectory(this.fs.getPath(DOMAIN_PATH, "keys"));

		this.env = new MockEnvironment();
		this.env.setDefaultProfiles(Profiles.TEST);
		this.env.setProperty(SettingsKey.LE_ROOT_DIR.getName(), "/etc/letsencrypt");
		this.env.setProperty(SettingsKey.CUSTOM_SUBDIR.getName(), "custom");
		this.env.setProperty(SettingsKey.LE_GENERAL_SUBDIR.getName(), "globals");
		this.env.setProperty(SettingsKey.CONFIG_FILE_NAME.getName(), "config.properties");
		this.env.setProperty(SettingsKey.DOMAIN_KEY_SUBDIR.getName(), "keys");
		this.env.setProperty(SettingsKey.PRIVATE_KEY_QUANTITY.getName(), "3");
		this.env.setProperty(SettingsKey.PRIVATE_KEY_PERIOD_VALIDITY_DAYS.getName(), "90");
		this.env.setProperty(SettingsKey.PRIVATE_KEY_TOLERANCE_DAYS.getName(), "15");
		this.env.setProperty(SettingsKey.BACKUP_KEY_ENABLE.getName(), "false");
		this.env.setProperty(SettingsKey.BACKUP_KEY_QUANTITY.getName(), "3");
		this.env.setProperty(SettingsKey.KEYS_SIZE.getName(), "1024");
		this.env.setProperty(SettingsKey.KEYS_ALGORITHM.getName(), "RSA");
		this.env.setProperty(SettingsKey.MESSAGE_DIGEST_ALGORITHM.getName(), "SHA-256");
		this.settingsService = new SettingsService(this.env, this.fs, this.env);

		this.info = DomainInfo.builder().id(DOMAIN_ID).name(DOMAIN_NAME).directory(this.fs.getPath(DOMAIN_PATH)).build();

		this.openSslApi = new OpenSslAPI(this.settingsService, mock(Terminal.class), this.fs, mock(ProcessManager.class));
		this.keyService = new KeyService(this.settingsService, this.openSslApi, this.fs, msg);

		this.preparePrivateKeyForTest();
	}

	/**
	 * key1 = key.expire=true / key.expire.date=now + validity days <br>
	 * key2 = key.expire=false / key.expire.date=past (expire) <br>
	 * key3 = key.expire=false / key.expire.date=now + validity days <br>
	 * key4 = key.expire=false / key.expire.date=now + 2 * validity days <br>
	 * key5 = key.expire=false / key.expire.date=now + 3 * validity days <br>
	 * key6 = key.expire=true / key.expire.date=now + 3 * validity days
	 */
	private void preparePrivateKeyForTest() {
		this.key1 = this.keyService.buildKey(KeyType.PRIVATE, this.info);
		this.keyService.save(this.key1);
		this.key1.getSettings().set(SettingsKey.KEY_EXPIRE, true);
		this.key1.getSettings().set(SettingsKey.KEY_EXPIRE_DATE, EpicDateTime.now().plusDays(90));
		this.settingsService.save(this.key1.getSettings());

		this.key2 = this.keyService.buildKey(KeyType.PRIVATE, this.info);
		this.keyService.save(this.key2);
		this.key2.getSettings().set(SettingsKey.KEY_EXPIRE_DATE, EpicDateTime.now().minusDays(20));
		this.settingsService.save(this.key2.getSettings());

		this.key3 = this.keyService.buildKey(KeyType.PRIVATE, this.info);
		this.keyService.save(this.key3);
		this.key3.getSettings().set(SettingsKey.KEY_EXPIRE_DATE, EpicDateTime.now().plusDays(90));
		this.settingsService.save(this.key3.getSettings());

		this.key4 = this.keyService.buildKey(KeyType.PRIVATE, this.info);
		this.keyService.save(this.key4);
		this.key4.getSettings().set(SettingsKey.KEY_EXPIRE_DATE, EpicDateTime.now().plusDays(2 * 90));
		this.settingsService.save(this.key4.getSettings());

		this.key5 = this.keyService.buildKey(KeyType.PRIVATE, this.info);
		this.keyService.save(this.key5);
		this.key5.getSettings().set(SettingsKey.KEY_EXPIRE_DATE, EpicDateTime.now().plusDays(3 * 90));
		this.settingsService.save(this.key5.getSettings());

		this.key6 = this.keyService.buildKey(KeyType.PRIVATE, this.info);
		this.keyService.save(this.key6);
		this.key6.getSettings().set(SettingsKey.KEY_EXPIRE, true);
		this.key6.getSettings().set(SettingsKey.KEY_EXPIRE_DATE, EpicDateTime.now().plusDays(3 * 90));
		this.settingsService.save(this.key6.getSettings());

		this.keys = Arrays.asList(this.key1, this.key2, this.key3, this.key4, this.key5, this.key6);
		for (int i = 0; i < this.keys.size(); i++) {
			log.debug("key{} '{}'", i + 1, this.keys.get(i).getPin());
		}
	}

	@Test
	public void getCurrentPrivateKey_returnsFirstValidKey() {
		// GIVEN before
		// WHEN
		final Key key = this.keyService.getCurrentPrivateKey(this.info);
		// THEN
		assertThat(key.getPin()).isEqualTo(this.key3.getPin());
	}

	@Test
	public void getLastExpirePrivateKey_returnsLastValidKey() {
		// GIVEN before
		// WHEN
		final Key key = this.keyService.getLastExpirePrivateKey(this.info);
		// THEN
		assertThat(key.getPin()).isEqualTo(this.key5.getPin());
	}

	@Test
	public void getAllPrivateKeys_returnsAll() {
		// GIVEN before
		// WHEN
		final ImmutableSet<Key> all = this.keyService.getAllPrivateKeys(this.info);
		// THEN
		assertThat(all.size()).isEqualTo(this.keys.size());
		/* @formatter:off */
		assertThat(this.keys.stream()
					.map(Key::getPin)
					.collect(Collectors.toSet())
					.containsAll(all.stream()
						.map(Key::getPin)
						.collect(Collectors.toSet()))
				).isTrue();
		/* @formatter:on */
	}

	@Test
	public void getValidPrivateKeys_returnsAllValidKeys() {
		// GIVEN before
		final List<Key> validKeys = Arrays.asList(this.key3, this.key4, this.key5);
		// WHEN
		final ImmutableSet<Key> valid = this.keyService.getValidPrivateKeys(this.info);
		// THEN
		assertThat(valid.size()).isEqualTo(validKeys.size());
		/* @formatter:off */
		assertThat(validKeys.stream()
					.map(Key::getPin)
					.collect(Collectors.toSet())
					.containsAll(valid.stream()
						.map(Key::getPin)
						.collect(Collectors.toSet()))
				).isTrue();
		/* @formatter:on */
	}
}
