/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.key;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.env.MockEnvironment;

import com.github.marschall.memoryfilesystem.MemoryFileSystemFactoryBean;

import de.epicsoft.epichttps.Profiles;
import de.epicsoft.epichttps.common.EpicMessageSource;
import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.common.OpenSslAPI;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.settings.SettingsKey;
import de.epicsoft.epichttps.settings.SettingsService;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public class DhParamServiceTest {

  private static final String DOMAIN_ID = "example_de";

  private static final String DOMAIN_NAME = "example.de";

  private static final String DOMAIN_PATH = "/etc/letencrypt/custom/example.com";

  private DhParamService dhParamService;

  private SettingsService settingsService;

  private FileSystemAPI fs;

  private OpenSslAPI openSsl;

  private EpicMessageSource msg;

  private DhParam dummy;

  @Before
  public void before() {
    this.msg = mock(EpicMessageSource.class);
    when(this.msg.getMessage(anyString(), anyString())).thenReturn("test message");
    when(this.msg.getMessage(anyString(), anyString(), anyString())).thenReturn("test message");

    final MemoryFileSystemFactoryBean factory = new MemoryFileSystemFactoryBean();
    factory.setType(MemoryFileSystemFactoryBean.LINUX);
    this.fs = new FileSystemAPI(factory.getObject());
    this.fs.createDirectory(this.fs.getPath(DOMAIN_PATH, "keys"));

    final MockEnvironment env = new MockEnvironment();
    env.setDefaultProfiles(Profiles.TEST);
    env.setProperty(SettingsKey.LE_ROOT_DIR.getName(), "/etc/letsencrypt");
    env.setProperty(SettingsKey.CUSTOM_SUBDIR.getName(), "custom");
    env.setProperty(SettingsKey.LE_GENERAL_SUBDIR.getName(), "globals");
    env.setProperty(SettingsKey.CONFIG_FILE_NAME.getName(), "config.properties");
    env.setProperty(SettingsKey.DH_PARAM_BIT.getName(), "4096");
    env.setProperty(SettingsKey.DOMAIN_KEY_SUBDIR.getName(), "keys");
    this.settingsService = new SettingsService(env, this.fs, env);

    final String dhParamName = String.format(DhParamService.DH_PARAM_TPL, 4096);
    this.dummy = new DhParam(this.fs.getPath(DOMAIN_PATH, "keys", dhParamName));

    this.openSsl = mock(OpenSslAPI.class);
    when(this.openSsl.createDhParam(any(), anyString())).thenReturn(this.dummy);

    this.dhParamService = new DhParamService(this.settingsService, this.fs, this.openSsl, this.msg);
  }

  @Test
  public void createDhParam_withNotExistingDhParam_createNewDhParam() {
    // GIVEN
    final DomainInfo info = DomainInfo.builder().id(DOMAIN_ID).name(DOMAIN_NAME).directory(this.fs.getPath(DOMAIN_PATH)).build();
    // WHEN
    final DhParam dhParam = this.dhParamService.create(info);
    // THEN
    assertThat(dhParam).isNotNull();
    assertThat(dhParam).isEqualTo(this.dummy);
    // AND
    verify(this.openSsl, times(1)).createDhParam(any(), anyString());
  }

  @Test
  public void createDhParam_withExistsDhParam_returnExistsDhParam() {
    // GIVEN
    final DomainInfo info = DomainInfo.builder().id(DOMAIN_ID).name(DOMAIN_NAME).directory(this.fs.getPath(DOMAIN_PATH)).build();
    this.fs.saveFile(this.dummy.getPath(), "someContent");
    // WHEN
    final DhParam dhParam = this.dhParamService.create(info);
    // THEN
    assertThat(dhParam).isNotNull();
    assertThat(dhParam).isEqualTo(this.dummy);
    // AND
    verify(this.openSsl, times(0)).createDhParam(any(), anyString());
  }

  @Test(expected = IllegalArgumentException.class)
  public void createDhParam_withNullDomainInfo_exception() {
    // GIVEN
    final DomainInfo info = null;
    // WHEN
    this.dhParamService.create(info);
  }
}
