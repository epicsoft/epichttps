/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.key;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.github.marschall.memoryfilesystem.MemoryFileSystemFactoryBean;

import de.epicsoft.epichttps.common.FileSystemAPI;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.settings.KeySettings;
import de.epicsoft.epichttps.settings.SettingsKey;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
public class KeyCreateDateComperatorTest {

	private static final String DOMAIN_ID = "example_de";
	private static final String DOMAIN_NAME = "example.de";
	private static final String DOMAIN_PATH = "/etc/letencrypt/custom/example.com";

	private DomainInfo info;
	private FileSystemAPI fs;
	private KeyCreateDateComperator comperator;

	@Before
	public void before() {
		final MemoryFileSystemFactoryBean factory = new MemoryFileSystemFactoryBean();
		factory.setType(MemoryFileSystemFactoryBean.LINUX);
		this.fs = new FileSystemAPI(factory.getObject());
		this.fs.createDirectory(this.fs.getPath(DOMAIN_PATH, "keys"));

		this.info = DomainInfo.builder().id(DOMAIN_ID).name(DOMAIN_NAME).directory(this.fs.getPath(DOMAIN_PATH)).build();

		this.comperator = new KeyCreateDateComperator();
	}

	@Test
	public void compare_withTwoKeys_firstOlder() {
		// GIVEN
		final Key first = this.generate(ZonedDateTime.now(ZoneOffset.UTC).minusDays(20));
		final Key second = this.generate(ZonedDateTime.now(ZoneOffset.UTC).minusDays(10));
		// WHEN
		final List<Key> list = Arrays.asList(first, second);
		Collections.sort(list, this.comperator);
		// THEN
		assertThat(list.get(0)).isEqualTo(first);
		assertThat(list.get(1)).isEqualTo(second);
	}

	@Test
	public void compare_withTwoKeysReverse_firstOlder() {
		// GIVEN
		final Key first = this.generate(ZonedDateTime.now(ZoneOffset.UTC).minusDays(20));
		final Key second = this.generate(ZonedDateTime.now(ZoneOffset.UTC).minusDays(10));
		// WHEN
		final List<Key> list = Arrays.asList(second, first);
		Collections.sort(list, this.comperator);
		// THEN
		assertThat(list.get(0)).isEqualTo(first);
		assertThat(list.get(1)).isEqualTo(second);
	}

	@Test
	public void compare_withFirstIsNull_keyForward() {
		// GIVEN
		final Key first = null;
		final Key second = this.generate(ZonedDateTime.now(ZoneOffset.UTC).minusDays(10));
		// WHEN
		final List<Key> list = Arrays.asList(first, second);
		Collections.sort(list, this.comperator);
		// THEN
		assertThat(list.get(0)).isEqualTo(second);
		assertThat(list.get(1)).isNull();
	}

	@Test
	public void compare_withSecondIsNull_keyForward() {
		// GIVEN
		final Key first = this.generate(ZonedDateTime.now(ZoneOffset.UTC).minusDays(20));
		final Key second = null;
		// WHEN
		final List<Key> list = Arrays.asList(first, second);
		Collections.sort(list, this.comperator);
		// THEN
		assertThat(list.get(0)).isEqualTo(first);
		assertThat(list.get(1)).isNull();
	}

	@Test
	public void compare_withBothAreNull_keyForward() {
		// GIVEN
		final Key first = null;
		final Key second = null;
		// WHEN
		final List<Key> list = Arrays.asList(first, second);
		Collections.sort(list, this.comperator);
		// THEN
		assertThat(list.get(0)).isNull();
		assertThat(list.get(1)).isNull();
	}

	private Key generate(final ZonedDateTime create) {
		final KeySettings keySettings = mock(KeySettings.class);
		when(keySettings.get(SettingsKey.KEY_CREATE_DATE)).thenReturn(create.toString());

		/* @formatter:off */
		return Key.builder()
				.type(KeyType.PRIVATE)
				.info(this.info)
				.keyProperty(this.fs.getPath(DOMAIN_PATH, "keys", "privatekey.property"))
				.settings(keySettings)
				.keyFileExists(true)
				.build();
		/* @formatter:on */
	}
}
