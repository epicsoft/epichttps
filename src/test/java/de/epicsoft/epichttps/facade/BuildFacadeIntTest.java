/**
 * Copyright 2017 epicsoft.de / Alexander Schwarz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.epicsoft.epichttps.facade;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import de.epicsoft.epichttps.Profiles;
import de.epicsoft.epichttps.domain.DomainInfo;
import de.epicsoft.epichttps.domain.DomainInfoService;

/**
 * Copyright epicsoft.de @author Alexander Schwarz <br>
 */
@ActiveProfiles({ Profiles.TEST, Profiles.NGINX })
@SpringBootTest
@RunWith(SpringRunner.class)
public class BuildFacadeIntTest {

	@Autowired
	private BuildFacade buildFacade;
	@Autowired
	private DomainInfoService domainInfoService;

	@Test
	public void isBuildNeeded_withEmptyDomain_buildNeeded() {
		// GIVEN
		final DomainInfo info = this.domainInfoService.create("example.com");
		// WHEN
		final BuildInfo result = this.buildFacade.isBuildNeeded(info);
		// THEN
		assertThat(result).isNotNull();
		assertThat(result.isBuildNeeded()).isTrue();
		assertThat(result.isKeyOrCertificateBuildNeeded()).isTrue();
		// AND
		assertThat(result.isCertificate()).isTrue();
		assertThat(result.isCsr()).isTrue();
		assertThat(result.isDhParam()).isTrue();
		assertThat(result.isKey()).isTrue();
		assertThat(result.isWebserver()).isTrue();
	}

	@Test(expected = IllegalArgumentException.class)
	public void isBuildNeeded_withNull_exception() {
		// GIVEN
		final DomainInfo info = null;
		// WHEN
		this.buildFacade.isBuildNeeded(info);
		// THEN exception
	}
}
