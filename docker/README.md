## epicHTTPS ##

epicHTTPS is a graphical interface for the administration and creation of Let's Encrypt certificates as well as web server configuration files.

epicHTTPS supports rotating keys, HTTP Strict Transport Security (HSTS), HTTP Public Key Pinning (HPKP), and other features.

## Environments ##

Available environments with default values, these can be overwritten as required.

```
SPRING_PROFILES_ACTIVE "prod,nginx"
EPICHTTPS_USERNAME "admin"
EPICHTTPS_PASSWORD ""
EPICHTTPS_HOSTPATH "/etc/letsencrypt"
EPICHTTPS_DOMAIN "epichttps.example.com"
```

## Example 'docker-compose.yml' ##

```yaml
version: '2'
services:
  epichttps:
    container_name: epichttps
    image: epicsoft/epichttps
    ports: 
      - "8080:8080"
    volumes:
      - /etc/letsencrypt:/etc/letsencrypt
    environment:
      EPICHTTPS_USERNAME: "admin"
      # create your own password eg: docker run epicsoft/bcrypt hash changeMe 16
      # you may need to escape the $ characters with $$
      # no default password is stored, at the start a random one is generated, see log output
      EPICHTTPS_PASSWORD: ""
      # the same as volumes mount host directory
      EPICHTTPS_HOSTPATH: "/etc/letsencrypt"
      # change to your public (sub-)domain for epicHTTPS
      EPICHTTPS_DOMAIN: "epichttps.example.com"
```

## Links ##
- https://bitbucket.org/epicsoft/epichttps
- https://bitbucket.org/epicsoft/epichttps_docker

## License ##

epicHTTPS is under the MIT license.

Please note the licenses of third parties and libraries, these may differ.
